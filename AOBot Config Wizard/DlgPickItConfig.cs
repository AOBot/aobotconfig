﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AOBot_Config_Wizard
{
    public partial class DlgPickItConfig : Form
    {
        string pickItSettings = "";
        public DlgPickItConfig(string p_pickItSettings)
        {
            InitializeComponent();
            pickItSettings = p_pickItSettings;
        }

        private void ClassicClick(object sender, EventArgs e)
        {
            DataTypes.gameVersion = "classic";
            DlgClassic classic = new DlgClassic(pickItSettings);
            classic.ShowDialog();
            this.Close();
        }

    }
}
