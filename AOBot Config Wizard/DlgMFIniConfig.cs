﻿using System;
using System.IO;
using System.Windows.Forms;

namespace AOBot_Config_Wizard
{
    public partial class DlgMFIniConfig : Form
    {
        public DlgMFIniConfig()
        {
            InitializeComponent();
        }

        private void DlgMFIniConfig_Load(object sender, EventArgs e)
        {
            DeserializeMFIni();
        }

        private void ShopClick(object sender, EventArgs e)
        {
            addItem("Shop", 60);
        }

        private void Delete(object sender, EventArgs e)
        {
            try
            {
                Object selection = MFIniFile.SelectedItem;
                int selectionIndex = MFIniFile.SelectedIndex;
                MFIniFile.Items.Remove(selection);
                MFIniFile.SetSelected(selectionIndex, true);
            }
            catch (ArgumentOutOfRangeException) { }
        }

        private void MoveUp(object sender, EventArgs e)
        {
            try
            {
                int selection = MFIniFile.SelectedIndex;
                MFIniFile.Items.Insert(selection - 1, MFIniFile.SelectedItem);
                MFIniFile.Items.RemoveAt(selection + 1);
                MFIniFile.SetSelected(selection - 1, true);
            }
            catch (ArgumentOutOfRangeException) { }
        }

        private void MoveDown(object sender, EventArgs e)
        {
            try
            {
                int selection = MFIniFile.SelectedIndex;
                if (MFIniFile.SelectedItem != null)
                {
                    MFIniFile.Items.Insert(selection + 2, MFIniFile.SelectedItem);
                    MFIniFile.Items.RemoveAt(selection);
                    MFIniFile.SetSelected(selection + 1, true);
                }
            }
            catch (ArgumentOutOfRangeException) { }
        }

        private void Sell(object sender, EventArgs e)
        {
            addItem("Sell", 60);
        }

        private void Heal(object sender, EventArgs e)
        {
            addItem("Heal", 60);
        }

        private void Resurrect(object sender, EventArgs e)
        {
            addItem("Resurrect", 60);
        }

        private void Repair(object sender, EventArgs e)
        {
            addItem("Repair", 60);
        }

        private void Stash(object sender, EventArgs e)
        {
            addItem("Stash", 60);
        }

        private void Gamble(object sender, EventArgs e)
        {
            addItem("Gamble", 600);
        }

        private void addBotAction(object sender, EventArgs e)
        {
            string selection = comboBox1.SelectedItem.ToString();
            switch (selection)
            {
                case "Countess":        { addItem("Countess", 300); break; }
                case "The Pit":         { addItem("The Pit", 400); break; }
                case "Andariel":        { addItem("Andariel", 300); break; }
                case "Ancient tunnels": { addItem("Ancient tunnels", 400); break; }
                case "Summoner":        { addItem("Summoner", 300); break; }
                case "KurastChests":    { addItem("KurastChests", 180); break; }
                case "Travincal":       { addItem("Travincal", 400); break; }
                case "Mephisto":        { addItem("Mephisto", 300); break; }
                case "Chaos":           { addItem("Chaos", 300); break; }
                case "Shenk":           { addItem("Shenk", 180); break; }
                case "Eldritch":        { addItem("Eldritch", 180); break; }
                case "Pindleskin":      { addItem("Pindleskin", 180); break; }
                case "Nihlathak":       { addItem("Nihlathak", 300); break; }
                case "Baal":            { addItem("Baal", 400); break; }
            }
        }

        private void addItem(string action, int timeout)
        {
            try
            {
                MFIniFile.Items.Insert(MFIniFile.SelectedIndex, action + ", " + timeout.ToString());
            }
            catch (Exception)
            {
                MFIniFile.Items.Add(action + ", " + timeout.ToString());
            }
        }

        private void ActChange(object sender, EventArgs e)
        {
            if (new DlgActChange().ShowDialog() == DialogResult.OK)
            {
                try
                {
                    MFIniFile.Items.Insert(MFIniFile.SelectedIndex, DataTypes.globalActChange);
                }
                catch (Exception)
                {
                    MFIniFile.Items.Add(DataTypes.globalActChange);
                }
            }
        }

        private void Sleep(object sender, EventArgs e)
        {
            if (new DlgSleep().ShowDialog() == DialogResult.OK)
            {
                try
                {
                    MFIniFile.Items.Insert(MFIniFile.SelectedIndex, "Sleep, " + DataTypes.globalSleepMS);
                }
                catch (Exception)
                {
                    MFIniFile.Items.Add("Sleep, " + DataTypes.globalSleepMS);
                }
            }
        }

        private void acceptMF(object sender, EventArgs e)
        {
            StreamWriter stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\MF.ini");
            for (int i = 1; i <= MFIniFile.Items.Count; ++i)
            {
                MFIniFile.SetSelected(i - 1, true);
                if (MFIniFile.SelectedItem.ToString() == "The Pit, 400")
                {
                    stw.WriteLine("Teleport 12, 100");
                    stw.WriteLine("Set OpenChests 1");
                    stw.WriteLine("ClearArea, 400");
                    stw.WriteLine("Teleport 16, 100");
                    stw.WriteLine("ClearArea, 400");
                    stw.WriteLine("Set OpenChests 0");
                }
                else if (MFIniFile.SelectedItem.ToString() == "Ancient tunnels, 400")
                {
                    stw.WriteLine("Teleport 65, 100");
                    stw.WriteLine("Set OpenChests 1");
                    stw.WriteLine("ClearArea, 400");
                    stw.WriteLine("Set OpenChests 0");
                }
                else if (MFIniFile.SelectedItem.ToString() == "Pindleskin, 180")
                {
                    stw.WriteLine("Teleport 121, 60");
                    stw.WriteLine("ClearHeroes, 180");
                }
                else { stw.WriteLine(MFIniFile.SelectedItem); }
            }
            stw.Close();
            this.Close();
        }

        private void DeserializeMFIni()
        {
            string[] Lines = File.ReadAllLines(DataTypes.mainConfig.AOPath + "config\\ini\\MF.ini");
            foreach (string Line in Lines)
            {
                MFIniFile.Items.Add(Line);
            }
            GetTeleportNames();
        }

        private void GetTeleportNames() 
        {
            for (int i = 0; i != MFIniFile.Items.Count; ++i)
            {
                MFIniFile.SetSelected(i, true);
                if (MFIniFile.SelectedItem.ToString() == "Teleport 12, 100")
                {
                    MFIniFile.Items.RemoveAt(i + 5);
                    MFIniFile.Items.RemoveAt(i + 4);
                    MFIniFile.Items.RemoveAt(i + 3);
                    MFIniFile.Items.RemoveAt(i + 2);
                    MFIniFile.Items.RemoveAt(i + 1);
                    MFIniFile.Items.RemoveAt(i);
                    MFIniFile.Items.Insert(i, "The Pit, 400");
                    i = 0;
                }
                else if (MFIniFile.SelectedItem.ToString() == "Teleport 65, 100")
                {
                    MFIniFile.Items.RemoveAt(i + 3);
                    MFIniFile.Items.RemoveAt(i + 2);
                    MFIniFile.Items.RemoveAt(i + 1);
                    MFIniFile.Items.RemoveAt(i);
                    MFIniFile.Items.Insert(i, "Ancient tunnels, 400");
                    i = 0;
                }
                else if (MFIniFile.SelectedItem.ToString() == "Teleport 121, 60")
                {
                    MFIniFile.Items.RemoveAt(i + 1);
                    MFIniFile.Items.RemoveAt(i);
                    MFIniFile.Items.Insert(i, "Pindleskin, 180");
                    i = 0;
                }
            }
        }
    }
}
