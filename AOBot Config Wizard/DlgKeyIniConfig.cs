﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AOBot_Config_Wizard
{
    public partial class DlgKeyIniConfig : Form
    {
        public DlgKeyIniConfig()
        {
            InitializeComponent();
            DeserializeKeyIni();
        }
        private void ShopClick(object sender, EventArgs e)
        {
            try
            {
                KeyIniFile.Items.Insert(KeyIniFile.SelectedIndex, "Shop, 60");
            }
            catch (Exception)
            {
                KeyIniFile.Items.Add("Shop, 60");
            }
        }

        private void Delete(object sender, EventArgs e)
        {
            try
            {
                Object selection = KeyIniFile.SelectedItem;
                int selectionIndex = KeyIniFile.SelectedIndex;
                KeyIniFile.Items.Remove(selection);
                KeyIniFile.SetSelected(selectionIndex, true);
            }
            catch (ArgumentOutOfRangeException)
            { }
        }

        private void MoveUp(object sender, EventArgs e)
        {
            try
            {

                int selection = KeyIniFile.SelectedIndex;
                KeyIniFile.Items.Insert(selection - 1, KeyIniFile.SelectedItem);
                KeyIniFile.Items.RemoveAt(selection + 1);
                KeyIniFile.SetSelected(selection - 1, true);

            }
            catch (ArgumentOutOfRangeException)
            { }

        }

        private void MoveDown(object sender, EventArgs e)
        {
            try
            {
                int selection = KeyIniFile.SelectedIndex;
                Object selectedItem = KeyIniFile.SelectedItem;
                if (selectedItem != null)
                {

                    KeyIniFile.Items.Insert(selection + 2, KeyIniFile.SelectedItem);
                    KeyIniFile.Items.RemoveAt(selection);
                    KeyIniFile.SetSelected(selection + 1, true);
                }

            }
            catch (ArgumentOutOfRangeException)
            { }
        }

        private void Sell(object sender, EventArgs e)
        {
            try
            {
                KeyIniFile.Items.Insert(KeyIniFile.SelectedIndex, "Sell, 60");
            }
            catch (Exception)
            {
                KeyIniFile.Items.Add("Sell, 60");
            }
        }

        private void Heal(object sender, EventArgs e)
        {
            try
            {
                KeyIniFile.Items.Insert(KeyIniFile.SelectedIndex, "Heal, 60");
            }
            catch (Exception)
            {
                KeyIniFile.Items.Add("Heal, 60");
            }
        }

        private void Resurrect(object sender, EventArgs e)
        {
            try
            {
                KeyIniFile.Items.Insert(KeyIniFile.SelectedIndex, "Resurrect, 60");
            }
            catch (Exception)
            {
                KeyIniFile.Items.Add("Resurrect, 60");
            }
        }

        private void Repair(object sender, EventArgs e)
        {
            try
            {
                KeyIniFile.Items.Insert(KeyIniFile.SelectedIndex, "Repair, 60");
            }
            catch (Exception)
            {
                KeyIniFile.Items.Add("Repair, 60");
            }
        }

        private void Stash(object sender, EventArgs e)
        {
            try
            {
                KeyIniFile.Items.Insert(KeyIniFile.SelectedIndex, "Stash, 60");
            }
            catch (Exception)
            {
                KeyIniFile.Items.Add("Stash, 60");
            }
        }

        private void Gamble(object sender, EventArgs e)
        {
            try
            {
                KeyIniFile.Items.Insert(KeyIniFile.SelectedIndex, "Gamble, 180");
            }
            catch (Exception)
            {
                KeyIniFile.Items.Add("Gamble, 180");
            }
        }

        private void AddBossClick(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString() == "Countess")
                try
                {
                    KeyIniFile.Items.Insert(KeyIniFile.SelectedIndex, "Countess, 300");
                }
                catch (Exception)
                {
                    KeyIniFile.Items.Add("Countess, 300");
                }
            else
                if (comboBox1.SelectedItem.ToString() == "Summoner")
                    try
                    {
                        KeyIniFile.Items.Insert(KeyIniFile.SelectedIndex, "Summoner, 300");
                    }
                    catch (Exception)
                    {
                        KeyIniFile.Items.Add("Summoner, 300");
                    }
                else
                    if (comboBox1.SelectedItem.ToString() == "Nihlathak")
                        try
                        {
                            KeyIniFile.Items.Insert(KeyIniFile.SelectedIndex, "Nihlathak, 300");
                        }
                        catch (Exception)
                        {
                            KeyIniFile.Items.Add("Nihlathak, 300");
                        }
                    else
                    { }
        }

        private void ActChange(object sender, EventArgs e)
        {
            DlgActChange act = new DlgActChange();
            if (act.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    KeyIniFile.Items.Insert(KeyIniFile.SelectedIndex, DataTypes.globalActChange);
                }
                catch (Exception)
                {
                    KeyIniFile.Items.Add(DataTypes.globalActChange);
                }
            }
        }

        private void Sleep(object sender, EventArgs e)
        {
            DlgSleep sleep = new DlgSleep();
            if (sleep.ShowDialog() == DialogResult.OK)
                try
                {
                    KeyIniFile.Items.Insert(KeyIniFile.SelectedIndex, "Sleep, " + DataTypes.globalSleepMS);
                }
                catch (Exception)
                {
                    KeyIniFile.Items.Add("Sleep, " + DataTypes.globalSleepMS);
                }
        }

        private void acceptKey(object sender, EventArgs e)
        {
            StreamWriter stw;
            stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\Key.ini");
            for (int i = 1; i <= KeyIniFile.Items.Count; ++i)
            {
                KeyIniFile.SetSelected(i - 1, true);
                stw.WriteLine(KeyIniFile.SelectedItem);
            }
            stw.Close();
            this.Close();
        }
        private void DeserializeKeyIni()
        {
            StreamReader str;
            str = File.OpenText(DataTypes.mainConfig.AOPath + "config\\ini\\Key.ini");
            while (!str.EndOfStream)
                KeyIniFile.Items.Add(str.ReadLine());
        }

        private void validation(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}
