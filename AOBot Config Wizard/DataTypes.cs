﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace AOBot_Config_Wizard
{
    class DataTypes
    {
        public struct AwesomOini
        {
            public string AOPath;
            public string GamePath;
            public string GameExe;
            public string OwnerName;
            public string WindowName;
            public string BotFile;
        }
        public struct BotIni
        {
            public string AccountName;
            public string Password;
            public string CharName;
            public string GambleFrom;
            public string GambleTo;
            public string ActiveRun;
            public string InventoryRow1;
            public string InventoryRow2;
            public string InventoryRow3;
            public string InventoryRow4;
            public string Difficulty;
            public string GameName;
            public string GamePass;
            public string PublicChat;
            public string PickItSetting;
            public string RunnerAccount;
            public string RunnerCharName;
            public string RunnerGamePass;
            public bool ConfigDone;
            public string CharacterBuild;
        }

        public static bool globalconfigDone;
        public static AwesomOini mainConfig = new AwesomOini();
        public static BotIni botIni0 = new BotIni();
        public static BotIni botIni1 = new BotIni();
        public static BotIni botIni2 = new BotIni();
        public static BotIni botIni3 = new BotIni();
        public static string globalSleepMS = "500";
        public static string globalActChange = "Act5, 30";
        public static bool globalNewVersion;
        public static string gameVersion = "";
        public static BotIni currentBotIni = new BotIni();
    }
}
