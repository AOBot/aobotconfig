﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AOBot_Config_Wizard
{
    public partial class DlgLeechBaalInfo : Form
    {
        public DlgLeechBaalInfo()
        {
            InitializeComponent();
        }

        private void SaveLeechTempInfo(object sender, EventArgs e)
        {
            DataTypes.leechInfo.RunnerAccount = textBoxRunnerAccount.Text;
            DataTypes.leechInfo.RunnerCharName = textBoxRunnerCharName.Text;
            DataTypes.leechInfo.RunnerGamePass = textBoxRunnerPass.Text;
            Close();
        }
        private void NoSpacesValidation(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
            if (Convert.ToInt32(e.KeyChar) == 32)
                e.Handled = true;
        }
    }
}
