﻿using System;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace AOBot_Config_Wizard
{
    public partial class DlgLoading : Form
    {
        private string configDone = "";

        public DlgLoading()
        {
            InitializeComponent();
        }

        private void DlgLoading_Load(object sender, EventArgs e)
        {
            DetectUpdateAvailable();
            label1.Text = "Getting previous settings...";
            this.Text = "Getting previous settings...";
            DeserializeMainInfo();
            progressBar1.PerformStep();
            label1.Text = "Done!";
            button1.Visible = true;
        }

        private void DetectUpdateAvailable()
        {
            try
            {
                progressBar1.PerformStep();
                string Data = new WebClient().DownloadString("http://www.aobot.org/forum/viewtopic.php?f=22&t=13877");
                progressBar1.PerformStep();
                progressBar1.PerformStep();
                if (!Data.Contains("<title>AoBot.org &bull; View topic - AOBot Config Wizard - V 2.6.2</title>"))
                    DataTypes.globalNewVersion = true;
                progressBar1.PerformStep();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error checking for update. Exception: " + ex.Message);
                progressBar1.PerformStep();
                progressBar1.PerformStep();
                progressBar1.PerformStep();
            }
        }

        private void DeserializeMainInfo()
        {
            try
            {
                StreamReader str;
                str = File.OpenText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\AOBot Config\\mainInfo.txt");
                configDone = str.ReadLine();
                DataTypes.mainConfig.AOPath = str.ReadLine();
                DataTypes.mainConfig.GameExe = str.ReadLine();
                DataTypes.mainConfig.GamePath = str.ReadLine();
                DataTypes.mainConfig.BotFile = str.ReadLine();
                DataTypes.mainConfig.OwnerName = str.ReadLine();
                str.Close();
            }
            catch (FileNotFoundException)
            {
                File.Create(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\AOBot Config\\mainInfo.txt");
            }
            catch (DirectoryNotFoundException)
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\AOBot Config\\");
            }
            try
            {
                if (configDone == "True")
                {
                    string contentBot;
                    try
                    {
                        contentBot = File.ReadAllText(DataTypes.mainConfig.AOPath + "config\\ini\\bot0.ini");
                        if ((DataTypes.botIni0.AccountName = ReadLine(contentBot, "Username=")) != "Account" && DataTypes.botIni0.AccountName != "")
                        {
                            DataTypes.botIni0.ConfigDone = true;
                            DataTypes.botIni0.CharName = ReadLine(contentBot, "Charname=");
                            DataTypes.botIni0.GambleFrom = ReadLine(contentBot, "GambleFrom=");
                            DataTypes.botIni0.GambleTo = ReadLine(contentBot, "GambleTo=");
                            DataTypes.botIni0.ActiveRun = ReadLine(contentBot, "BotFile=");
                            DataTypes.botIni0.InventoryRow1 = ReadLine(contentBot, "Inventory[0]=");
                            DataTypes.botIni0.InventoryRow2 = ReadLine(contentBot, "Inventory[1]=");
                            DataTypes.botIni0.InventoryRow3 = ReadLine(contentBot, "Inventory[2]=");
                            DataTypes.botIni0.InventoryRow4 = ReadLine(contentBot, "Inventory[3]=");
                            DataTypes.botIni0.Difficulty = ReadLine(contentBot, "Difficulty=");
                            DataTypes.botIni0.GameName = ReadLine(contentBot, "GameName=");
                            DataTypes.botIni0.GamePass = ReadLine(contentBot, "GamePass=");
                            DataTypes.botIni0.PublicChat = ReadLine(contentBot, "PublicChat=");
                            DataTypes.botIni0.PickItSetting = ReadLine(contentBot, "PickLua=");
                            DataTypes.botIni0.PickItSetting = DataTypes.botIni0.PickItSetting.Remove(DataTypes.botIni0.PickItSetting.IndexOf("\\pickItem.lua"));
                            DataTypes.botIni0.CharacterBuild = ReadLine(contentBot, "AttackFile=\"");
                            DataTypes.botIni0.CharacterBuild = DataTypes.botIni0.CharacterBuild.Remove(DataTypes.botIni0.CharacterBuild.IndexOf("\\Attack.ini"));
                        }
                        else { DataTypes.botIni0.ConfigDone = false; }
                    }
                    catch (FileNotFoundException)
                    {
                        File.WriteAllText(DataTypes.mainConfig.AOPath + "config\\ini\\bot1.ini", File.ReadAllText(DataTypes.mainConfig.AOPath + "config\\ini\\bot0.ini"));
                    }
                    try
                    {
                        contentBot = File.ReadAllText(DataTypes.mainConfig.AOPath + "config\\ini\\bot1.ini");
                        if ((DataTypes.botIni1.AccountName = ReadLine(contentBot, "Username=")) != "Account" && DataTypes.botIni1.AccountName != "")
                        {
                            DataTypes.botIni1.ConfigDone = true;
                            DataTypes.botIni1.CharName = ReadLine(contentBot, "Charname=");
                            DataTypes.botIni1.GambleFrom = ReadLine(contentBot, "GambleFrom=");
                            DataTypes.botIni1.GambleTo = ReadLine(contentBot, "GambleTo=");
                            DataTypes.botIni1.ActiveRun = ReadLine(contentBot, "BotFile=");
                            DataTypes.botIni1.InventoryRow1 = ReadLine(contentBot, "Inventory[0]=");
                            DataTypes.botIni1.InventoryRow2 = ReadLine(contentBot, "Inventory[1]=");
                            DataTypes.botIni1.InventoryRow3 = ReadLine(contentBot, "Inventory[2]=");
                            DataTypes.botIni1.InventoryRow4 = ReadLine(contentBot, "Inventory[3]=");
                            DataTypes.botIni1.Difficulty = ReadLine(contentBot, "Difficulty=");
                            DataTypes.botIni1.GameName = ReadLine(contentBot, "GameName=");
                            DataTypes.botIni1.GamePass = ReadLine(contentBot, "GamePass=");
                            DataTypes.botIni1.PublicChat = ReadLine(contentBot, "PublicChat=");
                            DataTypes.botIni1.PickItSetting = ReadLine(contentBot, "PickLua=");
                            DataTypes.botIni1.PickItSetting = DataTypes.botIni1.PickItSetting.Remove(DataTypes.botIni1.PickItSetting.IndexOf("\\pickItem.lua"));
                            DataTypes.botIni1.CharacterBuild = ReadLine(contentBot, "AttackFile=\"");
                            DataTypes.botIni1.CharacterBuild = DataTypes.botIni1.CharacterBuild.Remove(DataTypes.botIni1.CharacterBuild.IndexOf("\\Attack.ini"));

                        }
                        else { DataTypes.botIni1.ConfigDone = false; }
                    }
                    catch (FileNotFoundException)
                    {
                        File.WriteAllText(DataTypes.mainConfig.AOPath + "config\\ini\\bot1.ini", File.ReadAllText(DataTypes.mainConfig.AOPath + "config\\ini\\bot0.ini"));
                    }
                    try
                    {
                        contentBot = File.ReadAllText(DataTypes.mainConfig.AOPath + "config\\ini\\bot2.ini");
                        if ((DataTypes.botIni2.AccountName = ReadLine(contentBot, "Username=")) != "Account" && DataTypes.botIni2.AccountName != "")
                        {
                            DataTypes.botIni2.ConfigDone = true;
                            DataTypes.botIni2.CharName = ReadLine(contentBot, "Charname=");
                            DataTypes.botIni2.GambleFrom = ReadLine(contentBot, "GambleFrom=");
                            DataTypes.botIni2.GambleTo = ReadLine(contentBot, "GambleTo=");
                            DataTypes.botIni2.ActiveRun = ReadLine(contentBot, "BotFile=");
                            DataTypes.botIni2.InventoryRow1 = ReadLine(contentBot, "Inventory[0]=");
                            DataTypes.botIni2.InventoryRow2 = ReadLine(contentBot, "Inventory[1]=");
                            DataTypes.botIni2.InventoryRow3 = ReadLine(contentBot, "Inventory[2]=");
                            DataTypes.botIni2.InventoryRow4 = ReadLine(contentBot, "Inventory[3]=");
                            DataTypes.botIni2.Difficulty = ReadLine(contentBot, "Difficulty=");
                            DataTypes.botIni2.GameName = ReadLine(contentBot, "GameName=");
                            DataTypes.botIni2.GamePass = ReadLine(contentBot, "GamePass=");
                            DataTypes.botIni2.PublicChat = ReadLine(contentBot, "PublicChat=");
                            DataTypes.botIni2.PickItSetting = ReadLine(contentBot, "PickLua=");
                            DataTypes.botIni2.PickItSetting = DataTypes.botIni2.PickItSetting.Remove(DataTypes.botIni2.PickItSetting.IndexOf("\\pickItem.lua"));
                            DataTypes.botIni2.CharacterBuild = ReadLine(contentBot, "AttackFile=\"");
                            DataTypes.botIni2.CharacterBuild = DataTypes.botIni2.CharacterBuild.Remove(DataTypes.botIni2.CharacterBuild.IndexOf("\\Attack.ini"));

                        }
                        else { DataTypes.botIni2.ConfigDone = false; }
                    }
                    catch (FileNotFoundException)
                    {
                        File.WriteAllText(DataTypes.mainConfig.AOPath + "config\\ini\\bot2.ini", File.ReadAllText(DataTypes.mainConfig.AOPath + "config\\ini\\bot0.ini"));
                    }
                    try
                    {
                        contentBot = File.ReadAllText(DataTypes.mainConfig.AOPath + "config\\ini\\bot3.ini");
                        if ((DataTypes.botIni3.AccountName = ReadLine(contentBot, "Username=")) != "Account" && DataTypes.botIni3.AccountName != "")
                        {
                            DataTypes.botIni3.ConfigDone = true;
                            DataTypes.botIni3.CharName = ReadLine(contentBot, "Charname=");
                            DataTypes.botIni3.GambleFrom = ReadLine(contentBot, "GambleFrom=");
                            DataTypes.botIni3.GambleTo = ReadLine(contentBot, "GambleTo=");
                            DataTypes.botIni3.ActiveRun = ReadLine(contentBot, "BotFile=");
                            DataTypes.botIni3.InventoryRow1 = ReadLine(contentBot, "Inventory[0]=");
                            DataTypes.botIni3.InventoryRow2 = ReadLine(contentBot, "Inventory[1]=");
                            DataTypes.botIni3.InventoryRow3 = ReadLine(contentBot, "Inventory[2]=");
                            DataTypes.botIni3.InventoryRow4 = ReadLine(contentBot, "Inventory[3]=");
                            DataTypes.botIni3.Difficulty = ReadLine(contentBot, "Difficulty=");
                            DataTypes.botIni3.GameName = ReadLine(contentBot, "GameName=");
                            DataTypes.botIni3.GamePass = ReadLine(contentBot, "GamePass=");
                            DataTypes.botIni3.PublicChat = ReadLine(contentBot, "PublicChat=");
                            DataTypes.botIni3.PickItSetting = ReadLine(contentBot, "PickLua=");
                            DataTypes.botIni3.PickItSetting = DataTypes.botIni3.PickItSetting.Remove(DataTypes.botIni3.PickItSetting.IndexOf("\\pickItem.lua"));
                            DataTypes.botIni3.CharacterBuild = ReadLine(contentBot, "AttackFile=\"");
                            DataTypes.botIni3.CharacterBuild = DataTypes.botIni3.CharacterBuild.Remove(DataTypes.botIni3.CharacterBuild.IndexOf("\\Attack.ini"));

                        }
                        else { DataTypes.botIni3.ConfigDone = false; }
                    }
                    catch (FileNotFoundException)
                    {
                        File.WriteAllText(DataTypes.mainConfig.AOPath + "config\\ini\\bot3.ini", File.ReadAllText(DataTypes.mainConfig.AOPath + "config\\ini\\bot0.ini"));
                    }
                }
                if (configDone == "True")
                    DataTypes.globalconfigDone = true;
                else
                    DataTypes.globalconfigDone = false;
            }
            catch (DirectoryNotFoundException)
            {
                File.Create(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\AOBot Config\\mainInfo.txt");
                MessageBox.Show("Deleted old mainInfo.txt file, continuing to program...", "Deleted old mainInfo.txt", MessageBoxButtons.OK);
                DeserializeMainInfo();
            }
        }

        private string ReadLine(string p_content, string p_textToSearch)
        {
            int start = p_content.IndexOf(p_textToSearch) + p_textToSearch.Length;
            int end = p_content.IndexOf('\r', start);
            return p_content.Substring(start, (end - start)).TrimStart('\"').TrimEnd('\"');
        }

        private void clickOk(object sender, EventArgs e)
        {
            this.Hide();
            new DlgMainProgram().ShowDialog();
            this.Close();
        }
    }
}
