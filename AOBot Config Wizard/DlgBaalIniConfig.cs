﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AOBot_Config_Wizard
{
    public partial class DlgBaalIniConfig : Form
    {
        public DlgBaalIniConfig()
        {
            InitializeComponent();
            DeserializeBaalIni();
        }

        private void ShopClick(object sender, EventArgs e)
        {
            try
            {
                BaalIniFile.Items.Insert(BaalIniFile.SelectedIndex, "Shop, 60");
            }
            catch (Exception)
            {
                BaalIniFile.Items.Add("Shop, 60");
            }
        }

        private void Delete(object sender, EventArgs e)
        {
            try
            {
                Object selection = BaalIniFile.SelectedItem;
                int selectionIndex = BaalIniFile.SelectedIndex;
                BaalIniFile.Items.Remove(selection);
                BaalIniFile.SetSelected(selectionIndex, true);
            }
            catch (ArgumentOutOfRangeException)
            { }
        }

        private void MoveUp(object sender, EventArgs e)
        {
            try
            {

                int selection = BaalIniFile.SelectedIndex;
                    BaalIniFile.Items.Insert(selection - 1, BaalIniFile.SelectedItem);
                    BaalIniFile.Items.RemoveAt(selection + 1);
                    BaalIniFile.SetSelected(selection - 1, true);

            }
            catch (ArgumentOutOfRangeException)
            { }
 
        }

        private void MoveDown(object sender, EventArgs e)
        {
            try
            {
                int selection = BaalIniFile.SelectedIndex;
                Object selectedItem = BaalIniFile.SelectedItem;
                if (selectedItem != null)
                {

                    BaalIniFile.Items.Insert(selection + 2, BaalIniFile.SelectedItem);
                    BaalIniFile.Items.RemoveAt(selection);
                    BaalIniFile.SetSelected(selection + 1, true);
                }

            }
            catch (ArgumentOutOfRangeException)
            { }
        }

        private void Sell(object sender, EventArgs e)
        {
            try
            {
                BaalIniFile.Items.Insert(BaalIniFile.SelectedIndex, "Sell, 60");
            }
            catch (Exception)
            {
                BaalIniFile.Items.Add("Sell, 60");
            }
        }

        private void Heal(object sender, EventArgs e)
        {
            try
            {
                BaalIniFile.Items.Insert(BaalIniFile.SelectedIndex, "Heal, 60");
            }
            catch (Exception)
            {
                BaalIniFile.Items.Add("Heal, 60");
            }
        }

        private void Resurrect(object sender, EventArgs e)
        {
            try
            {
                BaalIniFile.Items.Insert(BaalIniFile.SelectedIndex, "Resurrect, 60");
            }
            catch (Exception)
            {
                BaalIniFile.Items.Add("Resurrect, 60");
            }
        }

        private void Repair(object sender, EventArgs e)
        {
            try
            {
                BaalIniFile.Items.Insert(BaalIniFile.SelectedIndex, "Repair, 60");
            }
            catch (Exception)
            {
                BaalIniFile.Items.Add("Repair, 60");
            }
        }

        private void Stash(object sender, EventArgs e)
        {
            try
            {
                BaalIniFile.Items.Insert(BaalIniFile.SelectedIndex, "Stash, 60");
            }
            catch (Exception)
            {
                BaalIniFile.Items.Add("Stash, 60");
            }
        }

        private void Gamble(object sender, EventArgs e)
        {
            try
            {
                BaalIniFile.Items.Insert(BaalIniFile.SelectedIndex, "Gamble, 180");
            }
            catch (Exception)
            {
                BaalIniFile.Items.Add("Gamble, 180");
            }
        }

        private void Baal(object sender, EventArgs e)
        {
            try
            {
                BaalIniFile.Items.Insert(BaalIniFile.SelectedIndex, "Baal, 400");
            }
            catch (Exception)
            {
                BaalIniFile.Items.Add("Baal, 400");
            }
        }

        private void ActChange(object sender, EventArgs e)
        {
            DlgActChange act = new DlgActChange();
            if (act.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    BaalIniFile.Items.Insert(BaalIniFile.SelectedIndex, DataTypes.globalActChange);
                }
                catch (Exception)
                {
                    BaalIniFile.Items.Add(DataTypes.globalActChange);
                }
            }
        }

        private void Sleep(object sender, EventArgs e)
        {
            DlgSleep sleep = new DlgSleep();
            if (sleep.ShowDialog() == DialogResult.OK)
                try
                {
                    BaalIniFile.Items.Insert(BaalIniFile.SelectedIndex, "Sleep, " + DataTypes.globalSleepMS);
                }
                catch (Exception)
                {
                    BaalIniFile.Items.Add("Sleep, " + DataTypes.globalSleepMS);
                }
        }

        private void acceptBaal(object sender, EventArgs e)
        {
            StreamWriter stw;
            stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\Baal.ini");
            for (int i = 1; i <= BaalIniFile.Items.Count; ++i)
            {
                BaalIniFile.SetSelected(i - 1, true);
                stw.WriteLine(BaalIniFile.SelectedItem);
            }
            stw.Close();
            this.Close();
        }
        private void DeserializeBaalIni()
        {
            StreamReader str;
            str = File.OpenText(DataTypes.mainConfig.AOPath + "config\\ini\\Baal.ini");
            while (!str.EndOfStream)
                BaalIniFile.Items.Add(str.ReadLine());
        }
    }
}
