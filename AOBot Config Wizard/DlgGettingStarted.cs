﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AOBot_Config_Wizard
{
    public partial class DlgGettingStarted : Form
    {
        public DlgGettingStarted()
        {
            InitializeComponent();
        }

        private void HelpChosen(object sender, TreeViewEventArgs e)
        {
          TreeNode clickedNode = treeView1.SelectedNode;
          string nodeName = clickedNode.Name;

          if (nodeName == "Beginners")
              texte.Text = "In this section, you will see how to install Awesom-O bot,\nconfigure it, and then run it. And you have the choice \nto install auto-awesom. Which is a really useful program. \nI highly recommend it.";
          else
              if (nodeName == "installAO")
              {
                  texte.Text = "First, you must download Awesom-O from the link at the bottom of the page.\n\nIt's a .rar file so you will need to install WinRar, which could be found in the \nsecond link below.\n\nWhen you're finished installing WinRar, Open Awesom-O.rar and extract the files\n to a folder of your choice, but NOT DIABLO II FOLDER. It could get you banned.\n\n";
                  texteLink.Text = "Awesom-O bot can be downloaded here";
                  linkWinrar.Text = "Winrar can be downloaded here";
              }
              else
                  if (nodeName == "configureAO")
                  {
                      texte.Text = "Now that it's installed, we need to configure it. With this program its pretty easy.\nBut remember, you must have CTA on the second weapons slot and have at least\n 1 point in Redemption skill.\n\nFirst you got to config Awesom-O itself, to tell it where your game is and stuff like that.\nYou just have to click on Main Config, and on New Main Config. Follow instructions.\nWhen you're done, you can click on Main Config, See current config and look if \neverything is ok. If not, click New config again.\n\nNow that Awesom-O is configured, we have to configure the bot, to let him know what\nyou want him to do and how. \nClick on Bot Config and on Bot0.ini (its the active config by default).\nFollow instructions. When you're done, your config should appear on the first page, \nunder Current Configuration.\nIf you configured another bot file than Bot0.ini, make sure you use the \nSelect Active Bot Config to select your config.\n\nYou are now ready to bot, or if you want, you can install Auto-Awesom, \na very useful tool, which I highly recommend.";
                  }
                  else
                      if (nodeName == "startingAO")
                          texte.Text = "To run the bot, you just have to click on Awesom-O.exe (the red portal).\nThe first time you will run it, it will ask for some realm information.\nFollow instructions and create a realm with the name of your choice.\n\nWhen it will be done, Awesom-O interface will appear.\nThen, you have to click on Start really fast. Before Diablo II opens.\nIt starts redvex proxy, if you dont do it, the bot won't be able \nto connect to battle.net.\n\nNow watch it go :)\n\n\nIf you don't want to click Start each time you start the bot, \nsee instructions for installing Auto-Awesom.";
                      else
                          if (nodeName == "installAutoAwesom")
                          {
                              texte.Text = "To install Auto-Awesom, simply download Auto-Awesom from the link below.\nRun it and follow instructions. It should ask for your char name and realm \nand stuff like that. If not, when program is started, click on Settings, \nConfiguration. Set it all correctly.";
                              texteLink.Text = "Auto-Awesom can be found here";
                          }
                          else
                              if (nodeName == "utiliseAutoAwesom")
                                  texte.Text = "To run it, simply double click on Auto-Awesom.exe, then on File, Start.\n\nOr if you checked Auto-Start in the settings, it will start the bot automatically.\n\nOther useful thing is Auto-Minize, check it in the settings and diablo II will \nauto-minimize, which reduce cpu and memory usage.";
                              else
                                  if (nodeName == "modifyingInis")
                                      texte.Text = "If you guys need to modify the ini files by yourself a bit, make sure you run this program \nBEFORE making your adjustments, or it will overwrite them.\nAlso, the changes won't appear on the Main page.";
                                  else
                                      if (nodeName == "runFiles")
                                          texte.Text = "Don't really need explanations, everything works correctly and is self-explanatory.\n\nNote that in The Pit and Ancient Tunnels, bot will open chests.\n\nTechnical details: \nFor users who knows that pindleskin, the pit, and ancient tunnels are buggy.\nDon't worry, it's written in the file as Teleport xx, xx.\nIt even opens chests :)\n\nFor the Select Active Config, it changes the active BotX.ini file runFile (MF.ini, etc...).\n\nExemple: Bot0.ini is currently active (Dark Orange), then the Bot0.ini runFile will \nbe changed.\n\nP.s.: I suggest that you delete everything inside the box before making a new run.\n\nImportant: You must have needed wps for the actions you put in your config.\nEx.: For KurastChests, you need Lower Kurast wp.";
                                      
                                      else
                                          texte.Text = "In this section, you will see how to install Awesom-O bot,\nconfigure it, and then run it. And you have the choice \nto install auto-awesom. Which is a really useful program. \nI highly recommend it.";
        }

        private void linkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (treeView1.SelectedNode.Name == "installAO")
                try
                {
                    System.Diagnostics.Process.Start("http://rapidshare.com/files/259048043/Awesom-O.rar");
                }
                catch (Exception)
                {
                    MessageBox.Show("Link error... to see website, go on www.aobot.org and click on download at the top of the page", "Error");
                }
            else
                if (treeView1.SelectedNode.Name == "installAutoAwesom")
                    try
                    {
                        System.Diagnostics.Process.Start("http://www.aobot.org/rogue/Auto-Awesom.exe");
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Link error... to see website, go on www.aobot.org and click on AOHelpers", "Error");
                    }
        }

        private void winrarClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.rarlab.com/rar/wrar380.exe");
            }
            catch (Exception)
            {
                MessageBox.Show("Link error... to see website, go on www.aobot.org and click on download at the top of the page", "Error");
            }
        }
       
    }
}
