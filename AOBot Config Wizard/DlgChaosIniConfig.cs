﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AOBot_Config_Wizard
{
    public partial class DlgChaosIniConfig : Form
    {
        public DlgChaosIniConfig()
        {
            InitializeComponent();
            DeserializeChaosIni();
        }
        private void ShopClick(object sender, EventArgs e)
        {
            try
            {
                ChaosIniFile.Items.Insert(ChaosIniFile.SelectedIndex, "Shop, 60");
            }
            catch (Exception)
            {
                ChaosIniFile.Items.Add("Shop, 60");
            }
        }

        private void Delete(object sender, EventArgs e)
        {
            try
            {
                Object selection = ChaosIniFile.SelectedItem;
                int selectionIndex = ChaosIniFile.SelectedIndex;
                ChaosIniFile.Items.Remove(selection);
                ChaosIniFile.SetSelected(selectionIndex, true);
            }
            catch (ArgumentOutOfRangeException)
            { }
        }

        private void MoveUp(object sender, EventArgs e)
        {
            try
            {

                int selection = ChaosIniFile.SelectedIndex;
                ChaosIniFile.Items.Insert(selection - 1, ChaosIniFile.SelectedItem);
                ChaosIniFile.Items.RemoveAt(selection + 1);
                ChaosIniFile.SetSelected(selection - 1, true);

            }
            catch (ArgumentOutOfRangeException)
            { }

        }

        private void MoveDown(object sender, EventArgs e)
        {
            try
            {
                int selection = ChaosIniFile.SelectedIndex;
                Object selectedItem = ChaosIniFile.SelectedItem;
                if (selectedItem != null)
                {

                    ChaosIniFile.Items.Insert(selection + 2, ChaosIniFile.SelectedItem);
                    ChaosIniFile.Items.RemoveAt(selection);
                    ChaosIniFile.SetSelected(selection + 1, true);
                }

            }
            catch (ArgumentOutOfRangeException)
            { }
        }

        private void Sell(object sender, EventArgs e)
        {
            try
            {
                ChaosIniFile.Items.Insert(ChaosIniFile.SelectedIndex, "Sell, 60");
            }
            catch (Exception)
            {
                ChaosIniFile.Items.Add("Sell, 60");
            }
        }

        private void Heal(object sender, EventArgs e)
        {
            try
            {
                ChaosIniFile.Items.Insert(ChaosIniFile.SelectedIndex, "Heal, 60");
            }
            catch (Exception)
            {
                ChaosIniFile.Items.Add("Heal, 60");
            }
        }

        private void Resurrect(object sender, EventArgs e)
        {
            try
            {
                ChaosIniFile.Items.Insert(ChaosIniFile.SelectedIndex, "Resurrect, 60");
            }
            catch (Exception)
            {
                ChaosIniFile.Items.Add("Resurrect, 60");
            }
        }

        private void Repair(object sender, EventArgs e)
        {
            try
            {
                ChaosIniFile.Items.Insert(ChaosIniFile.SelectedIndex, "Repair, 60");
            }
            catch (Exception)
            {
                ChaosIniFile.Items.Add("Repair, 60");
            }
        }

        private void Stash(object sender, EventArgs e)
        {
            try
            {
                ChaosIniFile.Items.Insert(ChaosIniFile.SelectedIndex, "Stash, 60");
            }
            catch (Exception)
            {
                ChaosIniFile.Items.Add("Stash, 60");
            }
        }

        private void Gamble(object sender, EventArgs e)
        {
            try
            {
                ChaosIniFile.Items.Insert(ChaosIniFile.SelectedIndex, "Gamble, 180");
            }
            catch (Exception)
            {
                ChaosIniFile.Items.Add("Gamble, 180");
            }
        }

        private void Chaos(object sender, EventArgs e)
        {
            try
            {
                ChaosIniFile.Items.Insert(ChaosIniFile.SelectedIndex, "Chaos, 400");
            }
            catch (Exception)
            {
                ChaosIniFile.Items.Add("Chaos, 400");
            }
        }

        private void ActChange(object sender, EventArgs e)
        {
            DlgActChange act = new DlgActChange();
            if (act.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    ChaosIniFile.Items.Insert(ChaosIniFile.SelectedIndex, DataTypes.globalActChange);
                }
                catch (Exception)
                {
                    ChaosIniFile.Items.Add(DataTypes.globalActChange);
                }
            }
        }

        private void Sleep(object sender, EventArgs e)
        {
            DlgSleep sleep = new DlgSleep();
            if (sleep.ShowDialog() == DialogResult.OK)
                try
                {
                    ChaosIniFile.Items.Insert(ChaosIniFile.SelectedIndex, "Sleep, " + DataTypes.globalSleepMS);
                }
                catch (Exception)
                {
                    ChaosIniFile.Items.Add("Sleep, " + DataTypes.globalSleepMS);
                }
        }

        private void acceptChaos(object sender, EventArgs e)
        {
            StreamWriter stw;
            stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\Chaos.ini");
            for (int i = 1; i <= ChaosIniFile.Items.Count; ++i)
            {
                ChaosIniFile.SetSelected(i - 1, true);
                stw.WriteLine(ChaosIniFile.SelectedItem);
            }
            stw.Close();
            this.Close();
        }
        private void DeserializeChaosIni()
        {
            StreamReader str;
            str = File.OpenText(DataTypes.mainConfig.AOPath + "config\\ini\\Chaos.ini");
            while (!str.EndOfStream)
                ChaosIniFile.Items.Add(str.ReadLine());
        }
    }
}
