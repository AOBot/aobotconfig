﻿namespace AOBot_Config_Wizard
{
    partial class DlgBot0LeechBaalInfo
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgBot0LeechBaalInfo));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxRunnerAccount = new System.Windows.Forms.TextBox();
            this.textBoxRunnerCharName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxRunnerPass = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(275, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "To leech a friend\'s baal games, we need some more info.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(259, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "And don\'t forget, the baal runner you\'re leeching from \r\nmust add you in his frie" +
                "ndlist.\r\n";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Baal runner\'s account:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Baal runner char name:";
            // 
            // textBoxRunnerAccount
            // 
            this.textBoxRunnerAccount.Location = new System.Drawing.Point(131, 72);
            this.textBoxRunnerAccount.Name = "textBoxRunnerAccount";
            this.textBoxRunnerAccount.Size = new System.Drawing.Size(100, 20);
            this.textBoxRunnerAccount.TabIndex = 4;
            this.textBoxRunnerAccount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoSpacesValidation);
            // 
            // textBoxRunnerCharName
            // 
            this.textBoxRunnerCharName.Location = new System.Drawing.Point(131, 98);
            this.textBoxRunnerCharName.Name = "textBoxRunnerCharName";
            this.textBoxRunnerCharName.Size = new System.Drawing.Size(100, 20);
            this.textBoxRunnerCharName.TabIndex = 5;
            this.textBoxRunnerCharName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoSpacesValidation);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Leech game password  :";
            // 
            // textBoxRunnerPass
            // 
            this.textBoxRunnerPass.Location = new System.Drawing.Point(131, 124);
            this.textBoxRunnerPass.Name = "textBoxRunnerPass";
            this.textBoxRunnerPass.Size = new System.Drawing.Size(100, 20);
            this.textBoxRunnerPass.TabIndex = 7;
            this.textBoxRunnerPass.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoSpacesValidation);
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(293, 41);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(293, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "Ok";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.SaveLeechTempInfo);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2, 147);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(262, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "(If baal game has a password, the bot will try this pass)";
            // 
            // DlgBot0LeechBaalInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(374, 173);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxRunnerPass);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxRunnerCharName);
            this.Controls.Add(this.textBoxRunnerAccount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(390, 209);
            this.MinimumSize = new System.Drawing.Size(390, 209);
            this.Name = "DlgBot0LeechBaalInfo";
            this.Text = "Leech Baal Config";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxRunnerAccount;
        private System.Windows.Forms.TextBox textBoxRunnerCharName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxRunnerPass;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label6;
    }
}