﻿namespace AOBot_Config_Wizard
{
    partial class DlgMainProgram
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgMainProgram));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancedSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NewConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seeCurrentConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.botConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bot0ConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bot1ConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bot2ConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bot3ConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.selectActiveBotConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.characterBuildsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.botRunConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.baaliniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chaosiniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.coBaaliniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.keyiniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leechBaaliniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mFiniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.selectActiveConfigFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pickItConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strictConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moderateConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greedyConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.selectActiveConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.gettingStartedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contactMeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.LabelBot1PublicChat = new System.Windows.Forms.Label();
            this.LabelBot1GamePass = new System.Windows.Forms.Label();
            this.LabelBot1GameName = new System.Windows.Forms.Label();
            this.LabelBot1ActiveRun = new System.Windows.Forms.Label();
            this.LabelBot1CharName = new System.Windows.Forms.Label();
            this.LabelBot1AccountName = new System.Windows.Forms.Label();
            this.LabelBot1PickIt = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.LabelBot2PublicChat = new System.Windows.Forms.Label();
            this.LabelBot2GamePass = new System.Windows.Forms.Label();
            this.LabelBot2GameName = new System.Windows.Forms.Label();
            this.LabelBot2ActiveRun = new System.Windows.Forms.Label();
            this.LabelBot2CharName = new System.Windows.Forms.Label();
            this.LabelBot2AccountName = new System.Windows.Forms.Label();
            this.LabelBot2PickIt = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.LabelBot3PublicChat = new System.Windows.Forms.Label();
            this.LabelBot3GamePass = new System.Windows.Forms.Label();
            this.LabelBot3GameName = new System.Windows.Forms.Label();
            this.LabelBot3ActiveRun = new System.Windows.Forms.Label();
            this.LabelBot3CharName = new System.Windows.Forms.Label();
            this.LabelBot3AccountName = new System.Windows.Forms.Label();
            this.LabelBot3PickIt = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelBot0PickitStat = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LabelBot0PublicChat = new System.Windows.Forms.Label();
            this.LabelBot0GamePassword = new System.Windows.Forms.Label();
            this.LabelBot0GameName = new System.Windows.Forms.Label();
            this.LabelBot0ActiveRunFile = new System.Windows.Forms.Label();
            this.LabelBot0CharName = new System.Windows.Forms.Label();
            this.LabelBot0AccountName = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.MainConfigToolStripMenuItem,
            this.botConfigToolStripMenuItem,
            this.botRunConfigToolStripMenuItem,
            this.pickItConfigToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(607, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Click += new System.EventHandler(this.ActivateMenuItems);
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExitToolStripMenuItem,
            this.advancedSettingsToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fichierToolStripMenuItem.Text = "&File";
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.ExitToolStripMenuItem.Text = "E&xit";
            this.ExitToolStripMenuItem.Click += new System.EventHandler(this.CloseForm);
            // 
            // advancedSettingsToolStripMenuItem
            // 
            this.advancedSettingsToolStripMenuItem.CheckOnClick = true;
            this.advancedSettingsToolStripMenuItem.Name = "advancedSettingsToolStripMenuItem";
            this.advancedSettingsToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.advancedSettingsToolStripMenuItem.Text = "Ad&vanced Settings";
            this.advancedSettingsToolStripMenuItem.Click += new System.EventHandler(this.AdvancedClick);
            // 
            // MainConfigToolStripMenuItem
            // 
            this.MainConfigToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewConfigToolStripMenuItem,
            this.seeCurrentConfigToolStripMenuItem});
            this.MainConfigToolStripMenuItem.Name = "MainConfigToolStripMenuItem";
            this.MainConfigToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.MainConfigToolStripMenuItem.Text = "&Main Config";
            // 
            // NewConfigToolStripMenuItem
            // 
            this.NewConfigToolStripMenuItem.Name = "NewConfigToolStripMenuItem";
            this.NewConfigToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.NewConfigToolStripMenuItem.Text = "&New Config";
            this.NewConfigToolStripMenuItem.Click += new System.EventHandler(this.NewMainConfig);
            // 
            // seeCurrentConfigToolStripMenuItem
            // 
            this.seeCurrentConfigToolStripMenuItem.Enabled = false;
            this.seeCurrentConfigToolStripMenuItem.Name = "seeCurrentConfigToolStripMenuItem";
            this.seeCurrentConfigToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.seeCurrentConfigToolStripMenuItem.Text = "&See Current Config";
            this.seeCurrentConfigToolStripMenuItem.Click += new System.EventHandler(this.SeeCurrentConfigClick);
            // 
            // botConfigToolStripMenuItem
            // 
            this.botConfigToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bot0ConfigToolStripMenuItem,
            this.bot1ConfigToolStripMenuItem,
            this.bot2ConfigToolStripMenuItem,
            this.bot3ConfigToolStripMenuItem,
            this.toolStripSeparator12,
            this.selectActiveBotConfigToolStripMenuItem,
            this.toolStripSeparator1,
            this.characterBuildsToolStripMenuItem});
            this.botConfigToolStripMenuItem.Enabled = false;
            this.botConfigToolStripMenuItem.Name = "botConfigToolStripMenuItem";
            this.botConfigToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.botConfigToolStripMenuItem.Text = "&Bot Config";
            // 
            // bot0ConfigToolStripMenuItem
            // 
            this.bot0ConfigToolStripMenuItem.Name = "bot0ConfigToolStripMenuItem";
            this.bot0ConfigToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.bot0ConfigToolStripMenuItem.Text = "Bot&0 Config";
            this.bot0ConfigToolStripMenuItem.Click += new System.EventHandler(this.Bot0ConfigIni);
            // 
            // bot1ConfigToolStripMenuItem
            // 
            this.bot1ConfigToolStripMenuItem.Name = "bot1ConfigToolStripMenuItem";
            this.bot1ConfigToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.bot1ConfigToolStripMenuItem.Text = "Bot&1 Config";
            this.bot1ConfigToolStripMenuItem.Click += new System.EventHandler(this.Bot1ConfigIni);
            // 
            // bot2ConfigToolStripMenuItem
            // 
            this.bot2ConfigToolStripMenuItem.Name = "bot2ConfigToolStripMenuItem";
            this.bot2ConfigToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.bot2ConfigToolStripMenuItem.Text = "Bot&2 Config";
            this.bot2ConfigToolStripMenuItem.Click += new System.EventHandler(this.Bot2IniConfig);
            // 
            // bot3ConfigToolStripMenuItem
            // 
            this.bot3ConfigToolStripMenuItem.Name = "bot3ConfigToolStripMenuItem";
            this.bot3ConfigToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.bot3ConfigToolStripMenuItem.Text = "Bot&3 Config";
            this.bot3ConfigToolStripMenuItem.Click += new System.EventHandler(this.Bot3ConfigIni);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(200, 6);
            // 
            // selectActiveBotConfigToolStripMenuItem
            // 
            this.selectActiveBotConfigToolStripMenuItem.Name = "selectActiveBotConfigToolStripMenuItem";
            this.selectActiveBotConfigToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.selectActiveBotConfigToolStripMenuItem.Text = "Sele&ct Active Bot Config ";
            this.selectActiveBotConfigToolStripMenuItem.Click += new System.EventHandler(this.openSelectActiveBot);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(200, 6);
            // 
            // characterBuildsToolStripMenuItem
            // 
            this.characterBuildsToolStripMenuItem.Name = "characterBuildsToolStripMenuItem";
            this.characterBuildsToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.characterBuildsToolStripMenuItem.Text = "Character Builds";
            this.characterBuildsToolStripMenuItem.Click += new System.EventHandler(this.characterBuildsClick);
            // 
            // botRunConfigToolStripMenuItem
            // 
            this.botRunConfigToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.baaliniToolStripMenuItem,
            this.chaosiniToolStripMenuItem,
            this.coBaaliniToolStripMenuItem,
            this.keyiniToolStripMenuItem,
            this.leechBaaliniToolStripMenuItem,
            this.mFiniToolStripMenuItem,
            this.toolStripSeparator13,
            this.selectActiveConfigFileToolStripMenuItem});
            this.botRunConfigToolStripMenuItem.Enabled = false;
            this.botRunConfigToolStripMenuItem.Name = "botRunConfigToolStripMenuItem";
            this.botRunConfigToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.botRunConfigToolStripMenuItem.Text = "Bot&Run Config";
            // 
            // baaliniToolStripMenuItem
            // 
            this.baaliniToolStripMenuItem.Name = "baaliniToolStripMenuItem";
            this.baaliniToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.baaliniToolStripMenuItem.Text = "Baal.ini";
            this.baaliniToolStripMenuItem.Click += new System.EventHandler(this.baalIniConfig);
            // 
            // chaosiniToolStripMenuItem
            // 
            this.chaosiniToolStripMenuItem.Name = "chaosiniToolStripMenuItem";
            this.chaosiniToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.chaosiniToolStripMenuItem.Text = "Chaos.ini";
            this.chaosiniToolStripMenuItem.Click += new System.EventHandler(this.ChaosIni);
            // 
            // coBaaliniToolStripMenuItem
            // 
            this.coBaaliniToolStripMenuItem.Name = "coBaaliniToolStripMenuItem";
            this.coBaaliniToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.coBaaliniToolStripMenuItem.Text = "CoBaal.ini";
            this.coBaaliniToolStripMenuItem.Click += new System.EventHandler(this.CoBaal);
            // 
            // keyiniToolStripMenuItem
            // 
            this.keyiniToolStripMenuItem.Name = "keyiniToolStripMenuItem";
            this.keyiniToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.keyiniToolStripMenuItem.Text = "Key.ini";
            this.keyiniToolStripMenuItem.Click += new System.EventHandler(this.keyIniConfig);
            // 
            // leechBaaliniToolStripMenuItem
            // 
            this.leechBaaliniToolStripMenuItem.Name = "leechBaaliniToolStripMenuItem";
            this.leechBaaliniToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.leechBaaliniToolStripMenuItem.Text = "LeechBaal.ini";
            this.leechBaaliniToolStripMenuItem.Click += new System.EventHandler(this.LeechBaaliniConfig);
            // 
            // mFiniToolStripMenuItem
            // 
            this.mFiniToolStripMenuItem.Name = "mFiniToolStripMenuItem";
            this.mFiniToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.mFiniToolStripMenuItem.Text = "MF.ini";
            this.mFiniToolStripMenuItem.Click += new System.EventHandler(this.MFIniConfig);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(194, 6);
            // 
            // selectActiveConfigFileToolStripMenuItem
            // 
            this.selectActiveConfigFileToolStripMenuItem.Name = "selectActiveConfigFileToolStripMenuItem";
            this.selectActiveConfigFileToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.selectActiveConfigFileToolStripMenuItem.Text = "Select Active ConfigFile";
            this.selectActiveConfigFileToolStripMenuItem.Click += new System.EventHandler(this.selectActiveRun);
            // 
            // pickItConfigToolStripMenuItem
            // 
            this.pickItConfigToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.strictConfigToolStripMenuItem,
            this.moderateConfigToolStripMenuItem,
            this.greedyConfigToolStripMenuItem,
            this.toolStripSeparator14,
            this.selectActiveConfigToolStripMenuItem});
            this.pickItConfigToolStripMenuItem.Enabled = false;
            this.pickItConfigToolStripMenuItem.Name = "pickItConfigToolStripMenuItem";
            this.pickItConfigToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.pickItConfigToolStripMenuItem.Text = "P&ickIt Config";
            // 
            // strictConfigToolStripMenuItem
            // 
            this.strictConfigToolStripMenuItem.Name = "strictConfigToolStripMenuItem";
            this.strictConfigToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.strictConfigToolStripMenuItem.Text = "Strict Config";
            this.strictConfigToolStripMenuItem.Click += new System.EventHandler(this.strictPickIt);
            // 
            // moderateConfigToolStripMenuItem
            // 
            this.moderateConfigToolStripMenuItem.Name = "moderateConfigToolStripMenuItem";
            this.moderateConfigToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.moderateConfigToolStripMenuItem.Text = "Moderate Config";
            this.moderateConfigToolStripMenuItem.Click += new System.EventHandler(this.moderatePickItClick);
            // 
            // greedyConfigToolStripMenuItem
            // 
            this.greedyConfigToolStripMenuItem.Name = "greedyConfigToolStripMenuItem";
            this.greedyConfigToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.greedyConfigToolStripMenuItem.Text = "Greedy Config";
            this.greedyConfigToolStripMenuItem.Click += new System.EventHandler(this.greedyPickIt);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(178, 6);
            // 
            // selectActiveConfigToolStripMenuItem
            // 
            this.selectActiveConfigToolStripMenuItem.Name = "selectActiveConfigToolStripMenuItem";
            this.selectActiveConfigToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.selectActiveConfigToolStripMenuItem.Text = "Select Active Config";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gettingStartedToolStripMenuItem,
            this.toolStripSeparator15,
            this.aboutToolStripMenuItem,
            this.contactMeToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(24, 20);
            this.toolStripMenuItem1.Text = "?";
            // 
            // gettingStartedToolStripMenuItem
            // 
            this.gettingStartedToolStripMenuItem.Name = "gettingStartedToolStripMenuItem";
            this.gettingStartedToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.gettingStartedToolStripMenuItem.Text = "Getting S&tarted";
            this.gettingStartedToolStripMenuItem.Click += new System.EventHandler(this.openGettingStartedDlg);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(156, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.openAbout);
            // 
            // contactMeToolStripMenuItem
            // 
            this.contactMeToolStripMenuItem.Name = "contactMeToolStripMenuItem";
            this.contactMeToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.contactMeToolStripMenuItem.Text = "Co&ntact Me";
            this.contactMeToolStripMenuItem.Click += new System.EventHandler(this.openContactMe);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(329, 42);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(258, 48);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.openAOOrg);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Current Configuration: ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.LabelBot1PublicChat);
            this.groupBox3.Controls.Add(this.LabelBot1GamePass);
            this.groupBox3.Controls.Add(this.LabelBot1GameName);
            this.groupBox3.Controls.Add(this.LabelBot1ActiveRun);
            this.groupBox3.Controls.Add(this.LabelBot1CharName);
            this.groupBox3.Controls.Add(this.LabelBot1AccountName);
            this.groupBox3.Controls.Add(this.LabelBot1PickIt);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox3.Location = new System.Drawing.Point(158, 159);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(139, 137);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Bot1.ini";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "ActiveRun :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "PickIt :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 121);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "PublicChat :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 108);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Pass :";
            // 
            // LabelBot1PublicChat
            // 
            this.LabelBot1PublicChat.AutoSize = true;
            this.LabelBot1PublicChat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot1PublicChat.ForeColor = System.Drawing.Color.Red;
            this.LabelBot1PublicChat.Location = new System.Drawing.Point(76, 121);
            this.LabelBot1PublicChat.Name = "LabelBot1PublicChat";
            this.LabelBot1PublicChat.Size = new System.Drawing.Size(21, 13);
            this.LabelBot1PublicChat.TabIndex = 21;
            this.LabelBot1PublicChat.Text = "Off";
            // 
            // LabelBot1GamePass
            // 
            this.LabelBot1GamePass.AutoSize = true;
            this.LabelBot1GamePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot1GamePass.ForeColor = System.Drawing.Color.Red;
            this.LabelBot1GamePass.Location = new System.Drawing.Point(47, 108);
            this.LabelBot1GamePass.Name = "LabelBot1GamePass";
            this.LabelBot1GamePass.Size = new System.Drawing.Size(21, 13);
            this.LabelBot1GamePass.TabIndex = 20;
            this.LabelBot1GamePass.Text = "Off";
            // 
            // LabelBot1GameName
            // 
            this.LabelBot1GameName.AutoSize = true;
            this.LabelBot1GameName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot1GameName.ForeColor = System.Drawing.Color.Green;
            this.LabelBot1GameName.Location = new System.Drawing.Point(5, 95);
            this.LabelBot1GameName.Name = "LabelBot1GameName";
            this.LabelBot1GameName.Size = new System.Drawing.Size(16, 13);
            this.LabelBot1GameName.TabIndex = 19;
            this.LabelBot1GameName.Text = "...";
            // 
            // LabelBot1ActiveRun
            // 
            this.LabelBot1ActiveRun.AutoSize = true;
            this.LabelBot1ActiveRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot1ActiveRun.Location = new System.Drawing.Point(64, 49);
            this.LabelBot1ActiveRun.Name = "LabelBot1ActiveRun";
            this.LabelBot1ActiveRun.Size = new System.Drawing.Size(16, 13);
            this.LabelBot1ActiveRun.TabIndex = 18;
            this.LabelBot1ActiveRun.Text = "...";
            // 
            // LabelBot1CharName
            // 
            this.LabelBot1CharName.AutoSize = true;
            this.LabelBot1CharName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot1CharName.ForeColor = System.Drawing.Color.Green;
            this.LabelBot1CharName.Location = new System.Drawing.Point(10, 29);
            this.LabelBot1CharName.Name = "LabelBot1CharName";
            this.LabelBot1CharName.Size = new System.Drawing.Size(16, 13);
            this.LabelBot1CharName.TabIndex = 17;
            this.LabelBot1CharName.Text = "...";
            // 
            // LabelBot1AccountName
            // 
            this.LabelBot1AccountName.AutoSize = true;
            this.LabelBot1AccountName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot1AccountName.Location = new System.Drawing.Point(6, 16);
            this.LabelBot1AccountName.Name = "LabelBot1AccountName";
            this.LabelBot1AccountName.Size = new System.Drawing.Size(16, 13);
            this.LabelBot1AccountName.TabIndex = 16;
            this.LabelBot1AccountName.Text = "...";
            // 
            // LabelBot1PickIt
            // 
            this.LabelBot1PickIt.AutoSize = true;
            this.LabelBot1PickIt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot1PickIt.ForeColor = System.Drawing.Color.Green;
            this.LabelBot1PickIt.Location = new System.Drawing.Point(47, 72);
            this.LabelBot1PickIt.Name = "LabelBot1PickIt";
            this.LabelBot1PickIt.Size = new System.Drawing.Size(16, 13);
            this.LabelBot1PickIt.TabIndex = 15;
            this.LabelBot1PickIt.Text = "...";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.LabelBot2PublicChat);
            this.groupBox2.Controls.Add(this.LabelBot2GamePass);
            this.groupBox2.Controls.Add(this.LabelBot2GameName);
            this.groupBox2.Controls.Add(this.LabelBot2ActiveRun);
            this.groupBox2.Controls.Add(this.LabelBot2CharName);
            this.groupBox2.Controls.Add(this.LabelBot2AccountName);
            this.groupBox2.Controls.Add(this.LabelBot2PickIt);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox2.Location = new System.Drawing.Point(303, 159);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(139, 137);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Bot2.ini";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(6, 49);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 13);
            this.label18.TabIndex = 25;
            this.label18.Text = "ActiveRun :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(7, 72);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 13);
            this.label19.TabIndex = 24;
            this.label19.Text = "PickIt :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(7, 121);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 13);
            this.label20.TabIndex = 23;
            this.label20.Text = "PublicChat :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(7, 108);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(36, 13);
            this.label21.TabIndex = 22;
            this.label21.Text = "Pass :";
            // 
            // LabelBot2PublicChat
            // 
            this.LabelBot2PublicChat.AutoSize = true;
            this.LabelBot2PublicChat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot2PublicChat.ForeColor = System.Drawing.Color.Red;
            this.LabelBot2PublicChat.Location = new System.Drawing.Point(77, 121);
            this.LabelBot2PublicChat.Name = "LabelBot2PublicChat";
            this.LabelBot2PublicChat.Size = new System.Drawing.Size(21, 13);
            this.LabelBot2PublicChat.TabIndex = 21;
            this.LabelBot2PublicChat.Text = "Off";
            // 
            // LabelBot2GamePass
            // 
            this.LabelBot2GamePass.AutoSize = true;
            this.LabelBot2GamePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot2GamePass.ForeColor = System.Drawing.Color.Red;
            this.LabelBot2GamePass.Location = new System.Drawing.Point(48, 108);
            this.LabelBot2GamePass.Name = "LabelBot2GamePass";
            this.LabelBot2GamePass.Size = new System.Drawing.Size(21, 13);
            this.LabelBot2GamePass.TabIndex = 20;
            this.LabelBot2GamePass.Text = "Off";
            // 
            // LabelBot2GameName
            // 
            this.LabelBot2GameName.AutoSize = true;
            this.LabelBot2GameName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot2GameName.ForeColor = System.Drawing.Color.Green;
            this.LabelBot2GameName.Location = new System.Drawing.Point(6, 95);
            this.LabelBot2GameName.Name = "LabelBot2GameName";
            this.LabelBot2GameName.Size = new System.Drawing.Size(16, 13);
            this.LabelBot2GameName.TabIndex = 19;
            this.LabelBot2GameName.Text = "...";
            // 
            // LabelBot2ActiveRun
            // 
            this.LabelBot2ActiveRun.AutoSize = true;
            this.LabelBot2ActiveRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot2ActiveRun.Location = new System.Drawing.Point(65, 49);
            this.LabelBot2ActiveRun.Name = "LabelBot2ActiveRun";
            this.LabelBot2ActiveRun.Size = new System.Drawing.Size(16, 13);
            this.LabelBot2ActiveRun.TabIndex = 18;
            this.LabelBot2ActiveRun.Text = "...";
            // 
            // LabelBot2CharName
            // 
            this.LabelBot2CharName.AutoSize = true;
            this.LabelBot2CharName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot2CharName.ForeColor = System.Drawing.Color.Green;
            this.LabelBot2CharName.Location = new System.Drawing.Point(11, 29);
            this.LabelBot2CharName.Name = "LabelBot2CharName";
            this.LabelBot2CharName.Size = new System.Drawing.Size(16, 13);
            this.LabelBot2CharName.TabIndex = 17;
            this.LabelBot2CharName.Text = "...";
            // 
            // LabelBot2AccountName
            // 
            this.LabelBot2AccountName.AutoSize = true;
            this.LabelBot2AccountName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot2AccountName.Location = new System.Drawing.Point(7, 16);
            this.LabelBot2AccountName.Name = "LabelBot2AccountName";
            this.LabelBot2AccountName.Size = new System.Drawing.Size(16, 13);
            this.LabelBot2AccountName.TabIndex = 16;
            this.LabelBot2AccountName.Text = "...";
            // 
            // LabelBot2PickIt
            // 
            this.LabelBot2PickIt.AutoSize = true;
            this.LabelBot2PickIt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot2PickIt.ForeColor = System.Drawing.Color.Green;
            this.LabelBot2PickIt.Location = new System.Drawing.Point(48, 72);
            this.LabelBot2PickIt.Name = "LabelBot2PickIt";
            this.LabelBot2PickIt.Size = new System.Drawing.Size(16, 13);
            this.LabelBot2PickIt.TabIndex = 15;
            this.LabelBot2PickIt.Text = "...";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Controls.Add(this.LabelBot3PublicChat);
            this.groupBox4.Controls.Add(this.LabelBot3GamePass);
            this.groupBox4.Controls.Add(this.LabelBot3GameName);
            this.groupBox4.Controls.Add(this.LabelBot3ActiveRun);
            this.groupBox4.Controls.Add(this.LabelBot3CharName);
            this.groupBox4.Controls.Add(this.LabelBot3AccountName);
            this.groupBox4.Controls.Add(this.LabelBot3PickIt);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox4.Location = new System.Drawing.Point(448, 159);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(139, 137);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Bot3.ini";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(6, 49);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(63, 13);
            this.label29.TabIndex = 25;
            this.label29.Text = "ActiveRun :";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(7, 72);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(40, 13);
            this.label30.TabIndex = 24;
            this.label30.Text = "PickIt :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(7, 121);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(64, 13);
            this.label31.TabIndex = 23;
            this.label31.Text = "PublicChat :";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(7, 108);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(36, 13);
            this.label32.TabIndex = 22;
            this.label32.Text = "Pass :";
            // 
            // LabelBot3PublicChat
            // 
            this.LabelBot3PublicChat.AutoSize = true;
            this.LabelBot3PublicChat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot3PublicChat.ForeColor = System.Drawing.Color.Red;
            this.LabelBot3PublicChat.Location = new System.Drawing.Point(77, 121);
            this.LabelBot3PublicChat.Name = "LabelBot3PublicChat";
            this.LabelBot3PublicChat.Size = new System.Drawing.Size(21, 13);
            this.LabelBot3PublicChat.TabIndex = 21;
            this.LabelBot3PublicChat.Text = "Off";
            // 
            // LabelBot3GamePass
            // 
            this.LabelBot3GamePass.AutoSize = true;
            this.LabelBot3GamePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot3GamePass.ForeColor = System.Drawing.Color.Red;
            this.LabelBot3GamePass.Location = new System.Drawing.Point(48, 108);
            this.LabelBot3GamePass.Name = "LabelBot3GamePass";
            this.LabelBot3GamePass.Size = new System.Drawing.Size(21, 13);
            this.LabelBot3GamePass.TabIndex = 20;
            this.LabelBot3GamePass.Text = "Off";
            // 
            // LabelBot3GameName
            // 
            this.LabelBot3GameName.AutoSize = true;
            this.LabelBot3GameName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot3GameName.ForeColor = System.Drawing.Color.Green;
            this.LabelBot3GameName.Location = new System.Drawing.Point(6, 95);
            this.LabelBot3GameName.Name = "LabelBot3GameName";
            this.LabelBot3GameName.Size = new System.Drawing.Size(16, 13);
            this.LabelBot3GameName.TabIndex = 19;
            this.LabelBot3GameName.Text = "...";
            // 
            // LabelBot3ActiveRun
            // 
            this.LabelBot3ActiveRun.AutoSize = true;
            this.LabelBot3ActiveRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot3ActiveRun.Location = new System.Drawing.Point(65, 49);
            this.LabelBot3ActiveRun.Name = "LabelBot3ActiveRun";
            this.LabelBot3ActiveRun.Size = new System.Drawing.Size(16, 13);
            this.LabelBot3ActiveRun.TabIndex = 18;
            this.LabelBot3ActiveRun.Text = "...";
            // 
            // LabelBot3CharName
            // 
            this.LabelBot3CharName.AutoSize = true;
            this.LabelBot3CharName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot3CharName.ForeColor = System.Drawing.Color.Green;
            this.LabelBot3CharName.Location = new System.Drawing.Point(11, 29);
            this.LabelBot3CharName.Name = "LabelBot3CharName";
            this.LabelBot3CharName.Size = new System.Drawing.Size(16, 13);
            this.LabelBot3CharName.TabIndex = 17;
            this.LabelBot3CharName.Text = "...";
            // 
            // LabelBot3AccountName
            // 
            this.LabelBot3AccountName.AutoSize = true;
            this.LabelBot3AccountName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot3AccountName.Location = new System.Drawing.Point(7, 16);
            this.LabelBot3AccountName.Name = "LabelBot3AccountName";
            this.LabelBot3AccountName.Size = new System.Drawing.Size(16, 13);
            this.LabelBot3AccountName.TabIndex = 16;
            this.LabelBot3AccountName.Text = "...";
            // 
            // LabelBot3PickIt
            // 
            this.LabelBot3PickIt.AutoSize = true;
            this.LabelBot3PickIt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot3PickIt.ForeColor = System.Drawing.Color.Green;
            this.LabelBot3PickIt.Location = new System.Drawing.Point(48, 72);
            this.LabelBot3PickIt.Name = "LabelBot3PickIt";
            this.LabelBot3PickIt.Size = new System.Drawing.Size(16, 13);
            this.LabelBot3PickIt.TabIndex = 15;
            this.LabelBot3PickIt.Text = "...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(458, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Config Wizard Version 2.6.2";
            // 
            // LabelBot0PickitStat
            // 
            this.LabelBot0PickitStat.AutoSize = true;
            this.LabelBot0PickitStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot0PickitStat.ForeColor = System.Drawing.Color.Green;
            this.LabelBot0PickitStat.Location = new System.Drawing.Point(47, 72);
            this.LabelBot0PickitStat.Name = "LabelBot0PickitStat";
            this.LabelBot0PickitStat.Size = new System.Drawing.Size(16, 13);
            this.LabelBot0PickitStat.TabIndex = 3;
            this.LabelBot0PickitStat.Text = "...";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.LabelBot0PublicChat);
            this.groupBox1.Controls.Add(this.LabelBot0GamePassword);
            this.groupBox1.Controls.Add(this.LabelBot0GameName);
            this.groupBox1.Controls.Add(this.LabelBot0ActiveRunFile);
            this.groupBox1.Controls.Add(this.LabelBot0CharName);
            this.groupBox1.Controls.Add(this.LabelBot0AccountName);
            this.groupBox1.Controls.Add(this.LabelBot0PickitStat);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(12, 159);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(139, 137);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bot0.ini";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(5, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "ActiveRun :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "PickIt :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "PublicChat :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Pass :";
            // 
            // LabelBot0PublicChat
            // 
            this.LabelBot0PublicChat.AutoSize = true;
            this.LabelBot0PublicChat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot0PublicChat.ForeColor = System.Drawing.Color.Red;
            this.LabelBot0PublicChat.Location = new System.Drawing.Point(76, 121);
            this.LabelBot0PublicChat.Name = "LabelBot0PublicChat";
            this.LabelBot0PublicChat.Size = new System.Drawing.Size(21, 13);
            this.LabelBot0PublicChat.TabIndex = 10;
            this.LabelBot0PublicChat.Text = "Off";
            // 
            // LabelBot0GamePassword
            // 
            this.LabelBot0GamePassword.AutoSize = true;
            this.LabelBot0GamePassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot0GamePassword.ForeColor = System.Drawing.Color.Red;
            this.LabelBot0GamePassword.Location = new System.Drawing.Point(47, 108);
            this.LabelBot0GamePassword.Name = "LabelBot0GamePassword";
            this.LabelBot0GamePassword.Size = new System.Drawing.Size(21, 13);
            this.LabelBot0GamePassword.TabIndex = 9;
            this.LabelBot0GamePassword.Text = "Off";
            // 
            // LabelBot0GameName
            // 
            this.LabelBot0GameName.AutoSize = true;
            this.LabelBot0GameName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot0GameName.ForeColor = System.Drawing.Color.Green;
            this.LabelBot0GameName.Location = new System.Drawing.Point(5, 95);
            this.LabelBot0GameName.Name = "LabelBot0GameName";
            this.LabelBot0GameName.Size = new System.Drawing.Size(16, 13);
            this.LabelBot0GameName.TabIndex = 8;
            this.LabelBot0GameName.Text = "...";
            // 
            // LabelBot0ActiveRunFile
            // 
            this.LabelBot0ActiveRunFile.AutoSize = true;
            this.LabelBot0ActiveRunFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot0ActiveRunFile.Location = new System.Drawing.Point(64, 49);
            this.LabelBot0ActiveRunFile.Name = "LabelBot0ActiveRunFile";
            this.LabelBot0ActiveRunFile.Size = new System.Drawing.Size(16, 13);
            this.LabelBot0ActiveRunFile.TabIndex = 7;
            this.LabelBot0ActiveRunFile.Text = "...";
            // 
            // LabelBot0CharName
            // 
            this.LabelBot0CharName.AutoSize = true;
            this.LabelBot0CharName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot0CharName.ForeColor = System.Drawing.Color.Green;
            this.LabelBot0CharName.Location = new System.Drawing.Point(10, 29);
            this.LabelBot0CharName.Name = "LabelBot0CharName";
            this.LabelBot0CharName.Size = new System.Drawing.Size(16, 13);
            this.LabelBot0CharName.TabIndex = 6;
            this.LabelBot0CharName.Text = "...";
            // 
            // LabelBot0AccountName
            // 
            this.LabelBot0AccountName.AutoSize = true;
            this.LabelBot0AccountName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBot0AccountName.Location = new System.Drawing.Point(6, 16);
            this.LabelBot0AccountName.Name = "LabelBot0AccountName";
            this.LabelBot0AccountName.Size = new System.Drawing.Size(16, 13);
            this.LabelBot0AccountName.TabIndex = 5;
            this.LabelBot0AccountName.Text = "...";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(496, 134);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(94, 13);
            this.linkLabel1.TabIndex = 8;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "How to get started";
            this.linkLabel1.Click += new System.EventHandler(this.openGettingStartedDlg);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.DarkOrange;
            this.label11.Location = new System.Drawing.Point(9, 54);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(159, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Dark Orange : Active Bot Config";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 93);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(417, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "For now, you MUST have CTA on the 2nd weapon slot and 1 point in Redemption skill" +
                ".";
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(473, 106);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(0, 13);
            this.linkLabel2.TabIndex = 12;
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.update);
            // 
            // DlgMainProgram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 310);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(615, 344);
            this.MinimumSize = new System.Drawing.Size(615, 344);
            this.Name = "DlgMainProgram";
            this.Text = "AOBot Config Wizard - V 2.6.2";
            this.Load += new System.EventHandler(this.DlgMainProgram_Load);
            this.Activated += new System.EventHandler(this.ActivateMenuItems);
            this.Click += new System.EventHandler(this.ActivateMenuItems);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SaveConfigFile);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MainConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NewConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem seeCurrentConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advancedSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem botConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem botRunConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pickItConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bot0ConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bot1ConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bot2ConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bot3ConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem selectActiveBotConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem baaliniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mFiniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chaosiniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem coBaaliniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem keyiniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leechBaaliniToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem selectActiveConfigFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem strictConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moderateConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greedyConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem selectActiveConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gettingStartedToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem contactMeToolStripMenuItem;
        private System.Windows.Forms.Label LabelBot0PickitStat;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LabelBot0PublicChat;
        private System.Windows.Forms.Label LabelBot0GamePassword;
        private System.Windows.Forms.Label LabelBot0GameName;
        private System.Windows.Forms.Label LabelBot0ActiveRunFile;
        private System.Windows.Forms.Label LabelBot0CharName;
        private System.Windows.Forms.Label LabelBot0AccountName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label LabelBot1PublicChat;
        private System.Windows.Forms.Label LabelBot1GamePass;
        private System.Windows.Forms.Label LabelBot1GameName;
        private System.Windows.Forms.Label LabelBot1ActiveRun;
        private System.Windows.Forms.Label LabelBot1CharName;
        private System.Windows.Forms.Label LabelBot1AccountName;
        private System.Windows.Forms.Label LabelBot1PickIt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label LabelBot2PublicChat;
        private System.Windows.Forms.Label LabelBot2GamePass;
        private System.Windows.Forms.Label LabelBot2GameName;
        private System.Windows.Forms.Label LabelBot2ActiveRun;
        private System.Windows.Forms.Label LabelBot2CharName;
        private System.Windows.Forms.Label LabelBot2AccountName;
        private System.Windows.Forms.Label LabelBot2PickIt;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label LabelBot3PublicChat;
        private System.Windows.Forms.Label LabelBot3GamePass;
        private System.Windows.Forms.Label LabelBot3GameName;
        private System.Windows.Forms.Label LabelBot3ActiveRun;
        private System.Windows.Forms.Label LabelBot3CharName;
        private System.Windows.Forms.Label LabelBot3AccountName;
        private System.Windows.Forms.Label LabelBot3PickIt;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem characterBuildsToolStripMenuItem;
    }
}

