﻿namespace AOBot_Config_Wizard
{
    partial class DlgUniqueSpecs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgUniqueSpecs));
            this.amuletsRadio = new System.Windows.Forms.RadioButton();
            this.armorRadio = new System.Windows.Forms.RadioButton();
            this.beltsRadio = new System.Windows.Forms.RadioButton();
            this.bootsRadio = new System.Windows.Forms.RadioButton();
            this.glovesRadio = new System.Windows.Forms.RadioButton();
            this.helmsRadio = new System.Windows.Forms.RadioButton();
            this.ringsRadio = new System.Windows.Forms.RadioButton();
            this.shieldsRadio = new System.Windows.Forms.RadioButton();
            this.weaponsRadio = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // amuletsRadio
            // 
            this.amuletsRadio.AutoSize = true;
            this.amuletsRadio.Location = new System.Drawing.Point(50, 35);
            this.amuletsRadio.Name = "amuletsRadio";
            this.amuletsRadio.Size = new System.Drawing.Size(62, 17);
            this.amuletsRadio.TabIndex = 0;
            this.amuletsRadio.TabStop = true;
            this.amuletsRadio.Text = "Amulets";
            this.amuletsRadio.UseVisualStyleBackColor = true;
            // 
            // armorRadio
            // 
            this.armorRadio.AutoSize = true;
            this.armorRadio.Location = new System.Drawing.Point(50, 58);
            this.armorRadio.Name = "armorRadio";
            this.armorRadio.Size = new System.Drawing.Size(57, 17);
            this.armorRadio.TabIndex = 1;
            this.armorRadio.TabStop = true;
            this.armorRadio.Text = "Armors";
            this.armorRadio.UseVisualStyleBackColor = true;
            // 
            // beltsRadio
            // 
            this.beltsRadio.AutoSize = true;
            this.beltsRadio.Location = new System.Drawing.Point(50, 81);
            this.beltsRadio.Name = "beltsRadio";
            this.beltsRadio.Size = new System.Drawing.Size(48, 17);
            this.beltsRadio.TabIndex = 2;
            this.beltsRadio.TabStop = true;
            this.beltsRadio.Text = "Belts";
            this.beltsRadio.UseVisualStyleBackColor = true;
            // 
            // bootsRadio
            // 
            this.bootsRadio.AutoSize = true;
            this.bootsRadio.Location = new System.Drawing.Point(50, 104);
            this.bootsRadio.Name = "bootsRadio";
            this.bootsRadio.Size = new System.Drawing.Size(52, 17);
            this.bootsRadio.TabIndex = 3;
            this.bootsRadio.TabStop = true;
            this.bootsRadio.Text = "Boots";
            this.bootsRadio.UseVisualStyleBackColor = true;
            // 
            // glovesRadio
            // 
            this.glovesRadio.AutoSize = true;
            this.glovesRadio.Location = new System.Drawing.Point(50, 127);
            this.glovesRadio.Name = "glovesRadio";
            this.glovesRadio.Size = new System.Drawing.Size(58, 17);
            this.glovesRadio.TabIndex = 4;
            this.glovesRadio.TabStop = true;
            this.glovesRadio.Text = "Gloves";
            this.glovesRadio.UseVisualStyleBackColor = true;
            // 
            // helmsRadio
            // 
            this.helmsRadio.AutoSize = true;
            this.helmsRadio.Location = new System.Drawing.Point(50, 150);
            this.helmsRadio.Name = "helmsRadio";
            this.helmsRadio.Size = new System.Drawing.Size(54, 17);
            this.helmsRadio.TabIndex = 5;
            this.helmsRadio.TabStop = true;
            this.helmsRadio.Text = "Helms";
            this.helmsRadio.UseVisualStyleBackColor = true;
            // 
            // ringsRadio
            // 
            this.ringsRadio.AutoSize = true;
            this.ringsRadio.Location = new System.Drawing.Point(50, 173);
            this.ringsRadio.Name = "ringsRadio";
            this.ringsRadio.Size = new System.Drawing.Size(52, 17);
            this.ringsRadio.TabIndex = 6;
            this.ringsRadio.TabStop = true;
            this.ringsRadio.Text = "Rings";
            this.ringsRadio.UseVisualStyleBackColor = true;
            // 
            // shieldsRadio
            // 
            this.shieldsRadio.AutoSize = true;
            this.shieldsRadio.Location = new System.Drawing.Point(50, 196);
            this.shieldsRadio.Name = "shieldsRadio";
            this.shieldsRadio.Size = new System.Drawing.Size(59, 17);
            this.shieldsRadio.TabIndex = 7;
            this.shieldsRadio.TabStop = true;
            this.shieldsRadio.Text = "Shields";
            this.shieldsRadio.UseVisualStyleBackColor = true;
            // 
            // weaponsRadio
            // 
            this.weaponsRadio.AutoSize = true;
            this.weaponsRadio.Location = new System.Drawing.Point(50, 219);
            this.weaponsRadio.Name = "weaponsRadio";
            this.weaponsRadio.Size = new System.Drawing.Size(71, 17);
            this.weaponsRadio.TabIndex = 8;
            this.weaponsRadio.TabStop = true;
            this.weaponsRadio.Text = "Weapons";
            this.weaponsRadio.UseVisualStyleBackColor = true;
            // 
            // DlgUniqueSpecs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 422);
            this.Controls.Add(this.weaponsRadio);
            this.Controls.Add(this.shieldsRadio);
            this.Controls.Add(this.ringsRadio);
            this.Controls.Add(this.helmsRadio);
            this.Controls.Add(this.glovesRadio);
            this.Controls.Add(this.bootsRadio);
            this.Controls.Add(this.beltsRadio);
            this.Controls.Add(this.armorRadio);
            this.Controls.Add(this.amuletsRadio);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DlgUniqueSpecs";
            this.Text = "Unique Items Specifications";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton amuletsRadio;
        private System.Windows.Forms.RadioButton armorRadio;
        private System.Windows.Forms.RadioButton beltsRadio;
        private System.Windows.Forms.RadioButton bootsRadio;
        private System.Windows.Forms.RadioButton glovesRadio;
        private System.Windows.Forms.RadioButton helmsRadio;
        private System.Windows.Forms.RadioButton ringsRadio;
        private System.Windows.Forms.RadioButton shieldsRadio;
        private System.Windows.Forms.RadioButton weaponsRadio;
    }
}