﻿namespace AOBot_Config_Wizard
{
    partial class DlgSelectActiveRun
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgSelectActiveRun));
            this.label1 = new System.Windows.Forms.Label();
            this.radioButtonBaal = new System.Windows.Forms.RadioButton();
            this.radioButtonCoBaal = new System.Windows.Forms.RadioButton();
            this.radioButtonLeechBaal = new System.Windows.Forms.RadioButton();
            this.radioButtonChaos = new System.Windows.Forms.RadioButton();
            this.radioButtonKey = new System.Windows.Forms.RadioButton();
            this.radioButtonMF = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select the run file you want to use :";
            // 
            // radioButtonBaal
            // 
            this.radioButtonBaal.AutoSize = true;
            this.radioButtonBaal.Checked = true;
            this.radioButtonBaal.Location = new System.Drawing.Point(56, 82);
            this.radioButtonBaal.Name = "radioButtonBaal";
            this.radioButtonBaal.Size = new System.Drawing.Size(59, 17);
            this.radioButtonBaal.TabIndex = 1;
            this.radioButtonBaal.TabStop = true;
            this.radioButtonBaal.Text = "Baal.ini";
            this.radioButtonBaal.UseVisualStyleBackColor = true;
            // 
            // radioButtonCoBaal
            // 
            this.radioButtonCoBaal.AutoSize = true;
            this.radioButtonCoBaal.Location = new System.Drawing.Point(56, 105);
            this.radioButtonCoBaal.Name = "radioButtonCoBaal";
            this.radioButtonCoBaal.Size = new System.Drawing.Size(72, 17);
            this.radioButtonCoBaal.TabIndex = 2;
            this.radioButtonCoBaal.Text = "CoBaal.ini";
            this.radioButtonCoBaal.UseVisualStyleBackColor = true;
            // 
            // radioButtonLeechBaal
            // 
            this.radioButtonLeechBaal.AutoSize = true;
            this.radioButtonLeechBaal.Location = new System.Drawing.Point(56, 128);
            this.radioButtonLeechBaal.Name = "radioButtonLeechBaal";
            this.radioButtonLeechBaal.Size = new System.Drawing.Size(89, 17);
            this.radioButtonLeechBaal.TabIndex = 3;
            this.radioButtonLeechBaal.Text = "LeechBaal.ini";
            this.radioButtonLeechBaal.UseVisualStyleBackColor = true;
            // 
            // radioButtonChaos
            // 
            this.radioButtonChaos.AutoSize = true;
            this.radioButtonChaos.Location = new System.Drawing.Point(160, 82);
            this.radioButtonChaos.Name = "radioButtonChaos";
            this.radioButtonChaos.Size = new System.Drawing.Size(68, 17);
            this.radioButtonChaos.TabIndex = 4;
            this.radioButtonChaos.Text = "Chaos.ini";
            this.radioButtonChaos.UseVisualStyleBackColor = true;
            // 
            // radioButtonKey
            // 
            this.radioButtonKey.AutoSize = true;
            this.radioButtonKey.Location = new System.Drawing.Point(160, 105);
            this.radioButtonKey.Name = "radioButtonKey";
            this.radioButtonKey.Size = new System.Drawing.Size(56, 17);
            this.radioButtonKey.TabIndex = 5;
            this.radioButtonKey.Text = "Key.ini";
            this.radioButtonKey.UseVisualStyleBackColor = true;
            // 
            // radioButtonMF
            // 
            this.radioButtonMF.AutoSize = true;
            this.radioButtonMF.Location = new System.Drawing.Point(160, 128);
            this.radioButtonMF.Name = "radioButtonMF";
            this.radioButtonMF.Size = new System.Drawing.Size(53, 17);
            this.radioButtonMF.TabIndex = 6;
            this.radioButtonMF.Text = "MF.ini";
            this.radioButtonMF.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(189, 179);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Accept";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.SelectActiveRunOk);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(270, 179);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // DlgSelectActiveRun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(357, 214);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.radioButtonMF);
            this.Controls.Add(this.radioButtonKey);
            this.Controls.Add(this.radioButtonChaos);
            this.Controls.Add(this.radioButtonLeechBaal);
            this.Controls.Add(this.radioButtonCoBaal);
            this.Controls.Add(this.radioButtonBaal);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(373, 250);
            this.MinimumSize = new System.Drawing.Size(373, 250);
            this.Name = "DlgSelectActiveRun";
            this.Text = "Select Active Run File";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonBaal;
        private System.Windows.Forms.RadioButton radioButtonCoBaal;
        private System.Windows.Forms.RadioButton radioButtonLeechBaal;
        private System.Windows.Forms.RadioButton radioButtonChaos;
        private System.Windows.Forms.RadioButton radioButtonKey;
        private System.Windows.Forms.RadioButton radioButtonMF;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}