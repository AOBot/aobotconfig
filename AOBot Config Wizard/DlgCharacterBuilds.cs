﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AOBot_Config_Wizard
{
    public partial class DlgCharacterBuilds : Form
    {
        public DlgCharacterBuilds()
        {
            InitializeComponent();
        }

        private void DlgCharacterBuilds_Load(object sender, EventArgs e)
        {
            StreamReader str = File.OpenText(DataTypes.mainConfig.AOPath + "config\\ini\\" + DataTypes.mainConfig.BotFile);
            string content = str.ReadToEnd();
            str.Close();

            int start = content.IndexOf("AttackFile=") + 12;
            int end = content.IndexOf('\r', start);
            string part = content.Substring(start, (end - start));
            int end2 = part.IndexOf('\\');
            part = part.Remove(end2);

            switch (part)
            {
                case "Pally": radioButtonPaladin.Checked = true; break;
                case "Assasin": radioButtonAssassin.Checked = true; break;
                case "Druid": radioButtonDruid.Checked = true; break;
                case "Sorc": radioButtonSorceress.Checked = true; break;
                case "Barbarian": radioButtonBarbarian.Checked = true; break;
                case "Amazon": radioButtonAmazon.Checked = true; break;
                case "Necromancer": radioButtonNecromancer.Checked = true; break;
                default: radioButtonPaladin.Checked = true; break;
            }
        }

        private void acceptChange(object sender, EventArgs e)
        {
            string characterFolder = "";
            if (radioButtonPaladin.Checked)
                characterFolder = "Pally";
            else if (radioButtonSorceress.Checked)
                characterFolder = "Sorc";
            else if (radioButtonNecromancer.Checked)
                characterFolder = "Necromancer";
            else if (radioButtonDruid.Checked)
                characterFolder = "Druid";
            else if (radioButtonBarbarian.Checked)
                characterFolder = "Barbarian";
            else if (radioButtonAssassin.Checked)
                characterFolder = "Assasin";
            else if (radioButtonAmazon.Checked)
                characterFolder = "Amazon";
            else
                characterFolder = "Pally";

            switch (DataTypes.mainConfig.BotFile)
            {
                case "Bot0.ini": DataTypes.botIni0.CharacterBuild = characterFolder; DataTypes.currentBotIni = DataTypes.botIni0; break;
                case "Bot1.ini": DataTypes.botIni1.CharacterBuild = characterFolder; DataTypes.currentBotIni = DataTypes.botIni1; break;
                case "Bot2.ini": DataTypes.botIni2.CharacterBuild = characterFolder; DataTypes.currentBotIni = DataTypes.botIni2; break;
                case "Bot3.ini": DataTypes.botIni3.CharacterBuild = characterFolder; DataTypes.currentBotIni = DataTypes.botIni3; break;
                default: DataTypes.botIni0.CharacterBuild = characterFolder; DataTypes.currentBotIni = DataTypes.botIni0; break;
            }
            this.Close();

            new DlgAttackSequences().ShowDialog();
        }
    }
}
