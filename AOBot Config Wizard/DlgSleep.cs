﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AOBot_Config_Wizard
{
    public partial class DlgSleep : Form
    {
        public DlgSleep()
        {
            InitializeComponent();
            textBox1.Text = DataTypes.globalSleepMS;
        }

        private void getMS(object sender, EventArgs e)
        {
            DataTypes.globalSleepMS = textBox1.Text;
        }

        private void NumericValidation(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;

            if (Convert.ToInt32(e.KeyChar) >= 48 && Convert.ToInt32(e.KeyChar) <= 58 || Convert.ToInt32(e.KeyChar) == 8)
                e.Handled = false;
        
        }
    }
}
