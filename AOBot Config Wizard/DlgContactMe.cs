﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AOBot_Config_Wizard
{
    public partial class DlgContactMe : Form
    {
        public DlgContactMe()
        {
            InitializeComponent();
        }

        private void sendEmail(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("mailto:nathan259@hotmail.com");
        }

        private void openPMMe(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.aobot.org/forum/ucp.php?i=pm&mode=compose&u=13809");
        }

        private void openSupport(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.aobot.org/forum/viewforum.php?f=22");
        }
    }
}
