﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace AOBot_Config_Wizard
{
    public partial class DlgMainProgram : Form
    {
        public DlgMainProgram()
        {
            InitializeComponent();
        }

        private void DlgMainProgram_Load(object sender, EventArgs e)
        {
            if (DataTypes.globalNewVersion)
                linkLabel2.Text = "New version Available!";
        }

        private void ActivateMenuItems(object sender, EventArgs e) //Activates menu items and font colors for activeBot
        {   
            bot0ConfigToolStripMenuItem.Font = new Font(bot0ConfigToolStripMenuItem.Font, FontStyle.Regular);
            groupBox1.ForeColor = Color.Black;

            bot1ConfigToolStripMenuItem.Font = new Font(bot1ConfigToolStripMenuItem.Font, FontStyle.Regular);
            groupBox3.ForeColor = Color.Black;

            bot2ConfigToolStripMenuItem.Font = new Font(bot2ConfigToolStripMenuItem.Font, FontStyle.Regular);
            groupBox2.ForeColor = Color.Black;

            bot3ConfigToolStripMenuItem.Font = new Font(bot3ConfigToolStripMenuItem.Font, FontStyle.Regular);
            groupBox4.ForeColor = Color.Black;

            switch (DataTypes.mainConfig.BotFile)
            {
                case "Bot0.ini": 
                    bot0ConfigToolStripMenuItem.Font = new Font(bot0ConfigToolStripMenuItem.Font, FontStyle.Bold);
                    groupBox1.ForeColor = Color.DarkOrange; 
                    break;
                case "Bot1.ini":
                    bot1ConfigToolStripMenuItem.Font = new Font(bot1ConfigToolStripMenuItem.Font, FontStyle.Bold);
                    groupBox3.ForeColor = Color.DarkOrange;
                    break;
                case "Bot2.ini":
                    bot2ConfigToolStripMenuItem.Font = new Font(bot2ConfigToolStripMenuItem.Font, FontStyle.Bold);
                    groupBox2.ForeColor = Color.DarkOrange;
                    break;
                case "Bot3.ini":
                    bot3ConfigToolStripMenuItem.Font = new Font(bot3ConfigToolStripMenuItem.Font, FontStyle.Bold);
                    groupBox4.ForeColor = Color.DarkOrange;
                    break;
                default :
                    bot0ConfigToolStripMenuItem.Font = new Font(bot0ConfigToolStripMenuItem.Font, FontStyle.Regular);
                    bot1ConfigToolStripMenuItem.Font = new Font(bot1ConfigToolStripMenuItem.Font, FontStyle.Regular);
                    bot2ConfigToolStripMenuItem.Font = new Font(bot2ConfigToolStripMenuItem.Font, FontStyle.Regular);
                    bot3ConfigToolStripMenuItem.Font = new Font(bot3ConfigToolStripMenuItem.Font, FontStyle.Regular);
                    break;
            }
            botConfigToolStripMenuItem.Enabled = DataTypes.globalconfigDone;
            seeCurrentConfigToolStripMenuItem.Enabled = DataTypes.globalconfigDone;
            
            if (DataTypes.botIni0.ConfigDone)
            {
                LabelBot0AccountName.Text = DataTypes.botIni0.AccountName;
                LabelBot0CharName.Text = DataTypes.botIni0.CharName;
                LabelBot0ActiveRunFile.Text = DataTypes.botIni0.ActiveRun;
                LabelBot0ActiveRunFile.ForeColor = Color.Green;
                LabelBot0PickitStat.Text = DataTypes.botIni0.PickItSetting;
                
                if (DataTypes.botIni0.GameName == "RandomGameNames")
                    LabelBot0GameName.Text = "Random Games";
                else
                    LabelBot0GameName.Text = DataTypes.botIni0.GameName;
                
                if (String.IsNullOrEmpty(DataTypes.botIni0.GamePass))
                {
                    LabelBot0GamePassword.Text = "Off";
                    LabelBot0GamePassword.ForeColor = Color.Red;
                }
                else
                {
                    LabelBot0GamePassword.Text = DataTypes.botIni0.GamePass;
                    LabelBot0GamePassword.ForeColor = Color.Green;
                }
                
                if (DataTypes.botIni0.PublicChat == "1")
                {
                    LabelBot0PublicChat.Text = "On";
                    LabelBot0PublicChat.ForeColor = Color.Green;
                }
                else
                {
                    LabelBot0PublicChat.Text = "Off";
                    LabelBot0PublicChat.ForeColor = Color.Red;
                }
            }
            if (DataTypes.botIni1.ConfigDone)
            {
                LabelBot1AccountName.Text = DataTypes.botIni1.AccountName;
                LabelBot1CharName.Text = DataTypes.botIni1.CharName;
                LabelBot1ActiveRun.Text = DataTypes.botIni1.ActiveRun;
                LabelBot1ActiveRun.ForeColor = Color.Green;
                LabelBot1PickIt.Text = DataTypes.botIni1.PickItSetting;
                
                if (DataTypes.botIni1.GameName == "RandomGameNames")
                    LabelBot1GameName.Text = "Random Games";
                else
                    LabelBot1GameName.Text = DataTypes.botIni1.GameName;
                
                if (DataTypes.botIni1.GamePass == "")
                {
                    LabelBot1GamePass.Text = "Off";
                    LabelBot1GamePass.ForeColor = Color.Red;
                }
                else
                {
                    LabelBot1GamePass.Text = DataTypes.botIni1.GamePass;
                    LabelBot1GamePass.ForeColor = Color.Green;
                }
                
                if (DataTypes.botIni1.PublicChat == "1")
                {
                    LabelBot1PublicChat.Text = "On";
                    LabelBot1PublicChat.ForeColor = Color.Green;
                }
                else
                {
                    LabelBot1PublicChat.Text = "Off";
                    LabelBot1PublicChat.ForeColor = Color.Red;
                }
            }
            if (DataTypes.botIni2.ConfigDone)
            {
                LabelBot2AccountName.Text = DataTypes.botIni2.AccountName;
                LabelBot2CharName.Text = DataTypes.botIni2.CharName;
                LabelBot2ActiveRun.Text = DataTypes.botIni2.ActiveRun;
                LabelBot2ActiveRun.ForeColor = Color.Green;
                LabelBot2PickIt.Text = DataTypes.botIni2.PickItSetting;
                
                if (DataTypes.botIni2.GameName == "RandomGameNames")
                    LabelBot2GameName.Text = "Random Games";
                else
                    LabelBot2GameName.Text = DataTypes.botIni2.GameName;
                
                if (DataTypes.botIni2.GamePass == "")
                {
                    LabelBot2GamePass.Text = "Off";
                    LabelBot2GamePass.ForeColor = Color.Red;
                }
                else
                {
                    LabelBot2GamePass.Text = DataTypes.botIni2.GamePass;
                    LabelBot2GamePass.ForeColor = Color.Green;
                }
                
                if (DataTypes.botIni2.PublicChat == "1")
                {
                    LabelBot2PublicChat.Text = "On";
                    LabelBot2PublicChat.ForeColor = Color.Green;
                }
                else
                {
                    LabelBot2PublicChat.Text = "Off";
                    LabelBot2PublicChat.ForeColor = Color.Red;
                }
            }
            if (DataTypes.botIni3.ConfigDone)
            {
                LabelBot3AccountName.Text = DataTypes.botIni3.AccountName;
                LabelBot3CharName.Text = DataTypes.botIni3.CharName;
                LabelBot3ActiveRun.Text = DataTypes.botIni3.ActiveRun;
                LabelBot3ActiveRun.ForeColor = Color.Green;
                LabelBot3PickIt.Text = DataTypes.botIni3.PickItSetting;
                
                if (DataTypes.botIni3.GameName == "RandomGameNames")
                    LabelBot3GameName.Text = "Random Games";
                else
                    LabelBot3GameName.Text = DataTypes.botIni3.GameName;
                
                if (DataTypes.botIni3.GamePass == "")
                {
                    LabelBot3GamePass.Text = "Off";
                    LabelBot3GamePass.ForeColor = Color.Red;
                }
                else
                {
                    LabelBot3GamePass.Text = DataTypes.botIni3.GamePass;
                    LabelBot3GamePass.ForeColor = Color.Green;
                }
                
                if (DataTypes.botIni3.PublicChat == "1")
                {
                    LabelBot3PublicChat.Text = "On";
                    LabelBot3PublicChat.ForeColor = Color.Green;
                }
                else
                {
                    LabelBot3PublicChat.Text = "Off";
                    LabelBot3PublicChat.ForeColor = Color.Red;
                }
            }
        }

        private void CloseForm(object sender, EventArgs e)
        {
            this.Close();
        }

        private void openGettingStartedDlg(object sender, EventArgs e)
        {
            new DlgGettingStarted().ShowDialog();
        }

        private void NewMainConfig(object sender, EventArgs e)//Open Dlg for Awesom-O.ini file
        {
            new DlgNewMainConfig().ShowDialog();
        }

        private void SeeCurrentConfigClick(object sender, EventArgs e)
        {
            new DlgSeeCurrentConfig().ShowDialog();
        }

        private void Bot0ConfigIni(object sender, EventArgs e)
        {
            DataTypes.currentBotIni = DataTypes.botIni0;
            new DlgBot0Ini(0).ShowDialog();
        }

        private void SerializeMainInfo()
        {
            StreamWriter stw;
            stw = File.CreateText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\AOBot Config\\mainInfo.txt");
            stw.WriteLine(DataTypes.globalconfigDone);
            stw.WriteLine(DataTypes.mainConfig.AOPath);
            stw.WriteLine(DataTypes.mainConfig.GameExe);
            stw.WriteLine(DataTypes.mainConfig.GamePath);
            stw.WriteLine(DataTypes.mainConfig.BotFile);
            stw.WriteLine(DataTypes.mainConfig.OwnerName);
            stw.Close();            
        }

        private void SaveConfigFile(object sender, FormClosingEventArgs e)
        {
            DialogResult result = new DialogResult();
            if (!(DataTypes.globalconfigDone && (DataTypes.botIni0.ConfigDone || DataTypes.botIni1.ConfigDone || DataTypes.botIni2.ConfigDone || DataTypes.botIni3.ConfigDone)))
            {
                result = MessageBox.Show("You are not finished configuring the bot, if you leave now, bot won't run correctly. Do you still want to leave?", "Not finished", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    SerializeMainInfo();
                    e.Cancel = false;
                }
                else { e.Cancel = true; }
            }
            SerializeMainInfo();
        }

        private void Bot1ConfigIni(object sender, EventArgs e)
        {
            DataTypes.currentBotIni = DataTypes.botIni1;
            new DlgBot0Ini(1).ShowDialog();
        }

        private void Bot2IniConfig(object sender, EventArgs e)
        {
            DataTypes.currentBotIni = DataTypes.botIni2;
            new DlgBot0Ini(2).ShowDialog();
        }

        private void Bot3ConfigIni(object sender, EventArgs e)
        {
            DataTypes.currentBotIni = DataTypes.botIni3;
            new DlgBot0Ini(3).ShowDialog();
        }

        private void openAOOrg(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.aobot.org");
            }
            catch (Exception)
            {
                MessageBox.Show("Link error... to see website, go on www.aobot.org", "Error");
            }
        }

        private void openSelectActiveBot(object sender, EventArgs e)
        {
            new DlgSelectActiveBot().ShowDialog();
        }

        private void openAbout(object sender, EventArgs e)
        {
            new DlgAbout().ShowDialog();
        }

        private void openContactMe(object sender, EventArgs e)
        {
            new DlgContactMe().ShowDialog();
        }

        private void baalIniConfig(object sender, EventArgs e)
        {
            new DlgBaalIniConfig().ShowDialog();
        }

        private void ChaosIni(object sender, EventArgs e)
        {
            new DlgChaosIniConfig().ShowDialog();
        }

        private void CoBaal(object sender, EventArgs e)
        {
            new DlgCoBaalIniConfig().ShowDialog();
        }

        private void keyIniConfig(object sender, EventArgs e)
        {
            new DlgKeyIniConfig().ShowDialog();
        }

        private void LeechBaaliniConfig(object sender, EventArgs e)
        {
            new DlgLeechBaalIniConfig().ShowDialog();
        }

        private void MFIniConfig(object sender, EventArgs e)
        {
            new DlgMFIniConfig().ShowDialog();
        }

        private void selectActiveRun(object sender, EventArgs e)
        {
            new DlgSelectActiveRun().ShowDialog();
        }

        private void AdvancedClick(object sender, EventArgs e)
        {
            if (!(DataTypes.globalconfigDone && (DataTypes.botIni0.ConfigDone || DataTypes.botIni1.ConfigDone || DataTypes.botIni2.ConfigDone || DataTypes.botIni3.ConfigDone)))
            {
                MessageBox.Show("You must first make your main config and at least one BotX.ini config", "Minimum config not done");
                advancedSettingsToolStripMenuItem.Checked = false;
            }
            else
            {
                botRunConfigToolStripMenuItem.Enabled = advancedSettingsToolStripMenuItem.Checked;
                pickItConfigToolStripMenuItem.Enabled = advancedSettingsToolStripMenuItem.Checked;
            }
        }

        private void update(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.aobot.org/forum/viewtopic.php?f=22&t=13877");
            }
            catch (Exception)
            {
                MessageBox.Show("Link error... to see website, go on www.aobot.org", "Error");
            }
        }

        private void moderatePickItClick(object sender, EventArgs e)
        {
            //new DlgPickItConfig("Moderate").ShowDialog(); Uncomment when done
        }

        private void strictPickIt(object sender, EventArgs e)
        {
            //new DlgPickItConfig("Strict").ShowDialog();    Uncomment when done
        }

        private void greedyPickIt(object sender, EventArgs e)
        {
            //new DlgPickItConfig("Greedy").ShowDialog();   Uncomment when done
        }

        private void characterBuildsClick(object sender, EventArgs e)
        {
            new DlgCharacterBuilds().ShowDialog();
        }
    }
}
