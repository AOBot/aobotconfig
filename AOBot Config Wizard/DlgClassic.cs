﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace AOBot_Config_Wizard
{
    public partial class DlgClassic : Form
    {
        string pickItSettings = "";
        public DlgClassic(string p_pickItSettings)
        {
            InitializeComponent();
            DeserializeStuff();
            pickItSettings = p_pickItSettings;
        }
        private void numericValidation(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;

            if (Convert.ToInt32(e.KeyChar) == 48 || Convert.ToInt32(e.KeyChar) == 49 || Convert.ToInt32(e.KeyChar) == 8 || Convert.ToInt32(e.KeyChar) == 46)
                e.Handled = false;

        }
        private void ReadItemLine(CheckBox p_checkBox, StreamReader p_str)
        {
            if (p_str.ReadLine().StartsWith("--"))
                p_checkBox.Checked = false;
            else
                p_checkBox.Checked = true;
        }

        private void DeserializeStuff()
        {
            StreamReader str;
            str = File.OpenText(DataTypes.mainConfig.AOPath + "\\config\\lua\\" + pickItSettings + "\\PickitRevolution\\" + DataTypes.gameVersion + "\\includes.lua");
            string goldPileLine = "";
            string goldPile = "";
            for (int i = 0; i != 29; ++i)
            {
                goldPileLine = str.ReadLine();
                if (goldPileLine.StartsWith("_MinGoldPile ="))
                {
                goldPile = goldPileLine.Substring(goldPileLine.IndexOf('=') + 2); 
                }

            }
            goldPileTextBoxe.Text = goldPile;
            for (int i = 0; i != 4; ++i)
                str.ReadLine();
            ReadItemLine(uniqueAmmys, str);
            ReadItemLine(uniqueArmors, str);
            ReadItemLine(uniqueBelts, str);
            ReadItemLine(uniqueBoots, str);
            ReadItemLine(uniqueGloves, str);
            ReadItemLine(uniqueHelms, str);
            ReadItemLine(uniqueRings, str);
            ReadItemLine(uniqueShields, str);
            ReadItemLine(uniqueWeapons, str);
            for (int i = 0; i != 6; ++i)
                str.ReadLine();
            ReadItemLine(rareAmmys, str);
            ReadItemLine(rareArmors, str);
            ReadItemLine(rareBelts, str);
            ReadItemLine(rareBoots, str);
            ReadItemLine(rareGloves, str);
            ReadItemLine(rareHelms, str);
            ReadItemLine(rareRings, str);
            ReadItemLine(rareShields, str);
            ReadItemLine(rareWeapons, str);
            for (int i = 0; i != 6; ++i)
                str.ReadLine();
            ReadItemLine(setItemscheck, str);
            for (int i = 0; i != 6; ++i)
                str.ReadLine();
            ReadItemLine(magicAmmys, str);
            ReadItemLine(magicArmors, str);
            ReadItemLine(magicHelms, str);
            ReadItemLine(magicRings, str);
            ReadItemLine(magicShields, str);
            ReadItemLine(magicWeapons, str);
            for (int i = 0; i != 6; ++i)
                str.ReadLine();
            ReadItemLine(whiteArmors, str);
            ReadItemLine(whiteGems, str);
            ReadItemLine(whiteHelms, str);
            ReadItemLine(whiteShields, str);
            ReadItemLine(whiteWeapons, str);   
            str.Close();
            
            
            


        }
        private void SerializeStuff()
        {
            StreamReader str;
            str = File.OpenText(DataTypes.mainConfig.AOPath + "\\config\\lua\\" + pickItSettings + "\\PickitRevolution\\" + DataTypes.gameVersion + "\\includes.lua");
            string content = str.ReadToEnd();
            str.Close();

            int start = 0;
            start = content.IndexOf("_MinGoldPile =");
            int end = content.IndexOf('\r', start);
            string goldPileLine = content.Substring(start, (end - start));

            content = Regex.Replace(content, goldPileLine, "_MinGoldPile = " + goldPileTextBoxe.Text);

            int startTemp = content.IndexOf("idRareAmulets =");
            int end2 = content.IndexOf('\n', startTemp) + 1;
            string textToWrite = content.Substring(0, end2);


            StreamWriter stw;
            stw = File.CreateText(DataTypes.mainConfig.AOPath + "\\config\\lua\\" + pickItSettings + "\\PickitRevolution\\" + DataTypes.gameVersion + "\\includes.lua");
            stw.Write(textToWrite);
            stw.WriteLine("--------------------------------------------------");
            stw.WriteLine("unique.files = {");
            stw.WriteLine("--------------------------------------------------");
            stw.WriteLine("--------------- Unique Item Files ----------------");
            stw.WriteLine("--------------------------------------------------");
            
            if (uniqueAmmys.Checked)
                stw.WriteLine("[\"amulets.lua\"] = {kind = \"Amulet\"},");
            else
                stw.WriteLine("--[\"amulets.lua\"] = {kind = \"Amulet\"},");
            
            if (uniqueArmors.Checked)
                stw.WriteLine("[\"armors.lua\"] = {kind = \"Armor\"},");
            else
                stw.WriteLine("--[\"armors.lua\"] = {kind = \"Armor\"},");

            if (uniqueBelts.Checked)
                stw.WriteLine("[\"belts.lua\"] = {kind = \"Belt\"},");
            else
                stw.WriteLine("--[\"belts.lua\"] = {kind = \"Belt\"},");
            
            if (uniqueBoots.Checked)
                stw.WriteLine("[\"boots.lua\"] = {kind = \"Boots\"},");
            else
                stw.WriteLine("--[\"boots.lua\"] = {kind = \"Boots\"},");

            if (uniqueGloves.Checked)
                stw.WriteLine("[\"gloves.lua\"] = {kind = \"Gloves\"},");
            else
                stw.WriteLine("--[\"gloves.lua\"] = {kind = \"Gloves\"},");

            if (uniqueHelms.Checked)
                stw.WriteLine("[\"helms.lua\"] = {kind = \"AnyHelm\"},");
            else
                stw.WriteLine("--[\"helms.lua\"] = {kind = \"AnyHelm\"},");

            if (uniqueRings.Checked)
                stw.WriteLine("[\"rings.lua\"] = {kind = \"Ring\"},");
            else
                stw.WriteLine("--[\"rings.lua\"] = {kind = \"Ring\"},");

            if (uniqueShields.Checked)
                stw.WriteLine("[\"shields.lua\"] = {kind = \"AnyShield\"},");
            else
                stw.WriteLine("--[\"shields.lua\"] = {kind = \"AnyShield\"},");
            
            if (uniqueWeapons.Checked)
                stw.WriteLine("[\"weapons.lua\"] = {kind = \"AnyWeapon\"},");
            else
                stw.WriteLine("--[\"weapons.lua\"] = {kind = \"AnyWeapon\"},");
            stw.WriteLine("--------------------------------------------------");
            stw.WriteLine("}");
            stw.WriteLine("rare.files = {");
            stw.WriteLine("--------------------------------------------------");
            stw.WriteLine("--------------- Rare Item Files ------------------");
            stw.WriteLine("--------------------------------------------------");
            
            if (rareAmmys.Checked)
                stw.WriteLine("[\"amulets.lua\"] = {kind = \"Amulet\"},");
            else
                stw.WriteLine("--[\"amulets.lua\"] = {kind = \"Amulet\"},");

            if (rareArmors.Checked)
                stw.WriteLine("[\"armors.lua\"] = {kind = \"Armor\"},");
            else
                stw.WriteLine("--[\"armors.lua\"] = {kind = \"Armor\"},");

            if (rareBelts.Checked)
                stw.WriteLine("[\"belts.lua\"] = {kind = \"Belt\"},");
            else
                stw.WriteLine("--[\"belts.lua\"] = {kind = \"Belt\"},");

            if (rareBoots.Checked)
                stw.WriteLine("[\"boots.lua\"] = {kind = \"Boots\"},");
            else
                stw.WriteLine("--[\"boots.lua\"] = {kind = \"Boots\"},");

            if (rareGloves.Checked)
                stw.WriteLine("[\"gloves.lua\"] = {kind = \"Gloves\"},");
            else
                stw.WriteLine("--[\"gloves.lua\"] = {kind = \"Gloves\"},");

            if (rareHelms.Checked)
                stw.WriteLine("[\"helms.lua\"] = {kind = \"Helm\"},");
            else
                stw.WriteLine("--[\"helms.lua\"] = {kind = \"Helm\"},");

            if (rareShields.Checked)
                stw.WriteLine("[\"shields.lua\"] = {kind = \"Shield\"},");
            else
                stw.WriteLine("--[\"shields.lua\"] = {kind = \"Shield\"},");

            if (rareRings.Checked)
                stw.WriteLine("[\"rings.lua\"] = {kind = \"Ring\"},");
            else
                stw.WriteLine("--[\"rings.lua\"] = {kind = \"Ring\"},");

            if (rareWeapons.Checked)
                stw.WriteLine("[\"weapons.lua\"] = {kind = \"AnyWeapon\"},");
            else
                stw.WriteLine("--[\"weapons.lua\"] = {kind = \"AnyWeapon\"},");
            stw.WriteLine("--------------------------------------------------");
            stw.WriteLine("}");
            stw.WriteLine("set.files = {");
            stw.WriteLine("--------------------------------------------------");
            stw.WriteLine("--------------- Set Item Files -------------------");
            stw.WriteLine("--------------------------------------------------");
            
            if (setItemscheck.Checked)
                stw.WriteLine("[\"setItems.lua\"] = {noLimiters = true}");
            else
                stw.WriteLine("--[\"setItems.lua\"] = {noLimiters = true}");
            stw.WriteLine("--------------------------------------------------");
            stw.WriteLine("}");
            stw.WriteLine("magic.files = {");
            stw.WriteLine("--------------------------------------------------");
            stw.WriteLine("--------------- Magic Item Files -----------------");
            stw.WriteLine("--------------------------------------------------");
            
            if (magicAmmys.Checked)
                stw.WriteLine("[\"amulets.lua\"] = {kind = \"Amulet\"},");
            else
                stw.WriteLine("--[\"amulets.lua\"] = {kind = \"Amulet\"},");

            if (magicArmors.Checked)
                stw.WriteLine("[\"armors.lua\"] = {kind = \"Armor\"},");
            else
                stw.WriteLine("--[\"armors.lua\"] = {kind = \"Armor\"},");

            if (magicHelms.Checked)
                stw.WriteLine("[\"helms.lua\"] = {kind = \"Helm\"},");
            else
                stw.WriteLine("--[\"helms.lua\"] = {kind = \"Helm\"},");

            if (magicRings.Checked)
                stw.WriteLine("[\"rings.lua\"] = {kind = \"Ring\"},");
            else
                stw.WriteLine("--[\"rings.lua\"] = {kind = \"Ring\"},");

            if (magicShields.Checked)
                stw.WriteLine("[\"shields.lua\"] = {kind = \"Shield\"},");
            else
                stw.WriteLine("--[\"shields.lua\"] = {kind = \"Shield\"},");

            if (magicWeapons.Checked)
                stw.WriteLine("[\"weapons.lua\"] = {kind = \"AnyWeapon\"},");
            else
                stw.WriteLine("--[\"weapons.lua\"] = {kind = \"AnyWeapon\"},");
            stw.WriteLine("--------------------------------------------------");
            stw.WriteLine("}");
            stw.WriteLine("white.files = {");
            stw.WriteLine("--------------------------------------------------");
            stw.WriteLine("--------------- White Item Files -----------------");
            stw.WriteLine("--------------------------------------------------");
            
            if (whiteArmors.Checked)
                stw.WriteLine("[\"armors.lua\"] = {kind = \"Armor\"},");
            else
                stw.WriteLine("--[\"armors.lua\"] = {kind = \"Armor\"},");

            if (whiteGems.Checked)
                stw.WriteLine("[\"gems.lua\"] = {kind = \"Gem\"},");
            else
                stw.WriteLine("--[\"gems.lua\"] = {kind = \"Gem\"},");

            if (whiteHelms.Checked)
                stw.WriteLine("[\"helms.lua\"] = {kind = \"AnyHelm\"},");
            else
                stw.WriteLine("--[\"helms.lua\"] = {kind = \"AnyHelm\"},");

            if (whiteShields.Checked)
                stw.WriteLine("[\"shields.lua\"] = {kind = \"AnyShield\"},");
            else
                stw.WriteLine("--[\"shields.lua\"] = {kind = \"AnyShield\"},");

            if (whiteWeapons.Checked)
                stw.WriteLine("[\"weapons.lua\"] = {kind = \"AnyWeapon\"},");
            else
                stw.WriteLine("--[\"weapons.lua\"] = {kind = \"AnyWeapon\"},");
            stw.WriteLine("--------------------------------------------------");
            stw.WriteLine("}");
            stw.Close();
        }

        private void SaveIncludes(object sender, EventArgs e)
        {
            SerializeStuff();

        }

        private void OK(object sender, EventArgs e)
        {
            SaveIncludes(sender, e);
            this.Close();
        }

        private void uniqueClick(object sender, EventArgs e)
        {
            DlgUniqueSpecs unique = new DlgUniqueSpecs();
            unique.ShowDialog();
        }
    }
}
