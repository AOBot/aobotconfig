﻿namespace AOBot_Config_Wizard
{
    partial class DlgCharacterBuilds
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgCharacterBuilds));
            this.label1 = new System.Windows.Forms.Label();
            this.radioButtonPaladin = new System.Windows.Forms.RadioButton();
            this.radioButtonAmazon = new System.Windows.Forms.RadioButton();
            this.radioButtonDruid = new System.Windows.Forms.RadioButton();
            this.radioButtonBarbarian = new System.Windows.Forms.RadioButton();
            this.radioButtonNecromancer = new System.Windows.Forms.RadioButton();
            this.radioButtonAssassin = new System.Windows.Forms.RadioButton();
            this.radioButtonSorceress = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Choose your character build :";
            // 
            // radioButtonPaladin
            // 
            this.radioButtonPaladin.AutoSize = true;
            this.radioButtonPaladin.Checked = true;
            this.radioButtonPaladin.Location = new System.Drawing.Point(55, 155);
            this.radioButtonPaladin.Name = "radioButtonPaladin";
            this.radioButtonPaladin.Size = new System.Drawing.Size(60, 17);
            this.radioButtonPaladin.TabIndex = 1;
            this.radioButtonPaladin.TabStop = true;
            this.radioButtonPaladin.Text = "Paladin";
            this.radioButtonPaladin.UseVisualStyleBackColor = true;
            // 
            // radioButtonAmazon
            // 
            this.radioButtonAmazon.AutoSize = true;
            this.radioButtonAmazon.Location = new System.Drawing.Point(55, 40);
            this.radioButtonAmazon.Name = "radioButtonAmazon";
            this.radioButtonAmazon.Size = new System.Drawing.Size(63, 17);
            this.radioButtonAmazon.TabIndex = 2;
            this.radioButtonAmazon.Text = "Amazon";
            this.radioButtonAmazon.UseVisualStyleBackColor = true;
            // 
            // radioButtonDruid
            // 
            this.radioButtonDruid.AutoSize = true;
            this.radioButtonDruid.Location = new System.Drawing.Point(55, 109);
            this.radioButtonDruid.Name = "radioButtonDruid";
            this.radioButtonDruid.Size = new System.Drawing.Size(50, 17);
            this.radioButtonDruid.TabIndex = 3;
            this.radioButtonDruid.Text = "Druid";
            this.radioButtonDruid.UseVisualStyleBackColor = true;
            // 
            // radioButtonBarbarian
            // 
            this.radioButtonBarbarian.AutoSize = true;
            this.radioButtonBarbarian.Location = new System.Drawing.Point(55, 86);
            this.radioButtonBarbarian.Name = "radioButtonBarbarian";
            this.radioButtonBarbarian.Size = new System.Drawing.Size(70, 17);
            this.radioButtonBarbarian.TabIndex = 4;
            this.radioButtonBarbarian.Text = "Barbarian";
            this.radioButtonBarbarian.UseVisualStyleBackColor = true;
            // 
            // radioButtonNecromancer
            // 
            this.radioButtonNecromancer.AutoSize = true;
            this.radioButtonNecromancer.Location = new System.Drawing.Point(55, 132);
            this.radioButtonNecromancer.Name = "radioButtonNecromancer";
            this.radioButtonNecromancer.Size = new System.Drawing.Size(89, 17);
            this.radioButtonNecromancer.TabIndex = 5;
            this.radioButtonNecromancer.Text = "Necromancer";
            this.radioButtonNecromancer.UseVisualStyleBackColor = true;
            // 
            // radioButtonAssassin
            // 
            this.radioButtonAssassin.AutoSize = true;
            this.radioButtonAssassin.Location = new System.Drawing.Point(55, 63);
            this.radioButtonAssassin.Name = "radioButtonAssassin";
            this.radioButtonAssassin.Size = new System.Drawing.Size(66, 17);
            this.radioButtonAssassin.TabIndex = 6;
            this.radioButtonAssassin.Text = "Assassin";
            this.radioButtonAssassin.UseVisualStyleBackColor = true;
            // 
            // radioButtonSorceress
            // 
            this.radioButtonSorceress.AutoSize = true;
            this.radioButtonSorceress.Location = new System.Drawing.Point(55, 178);
            this.radioButtonSorceress.Name = "radioButtonSorceress";
            this.radioButtonSorceress.Size = new System.Drawing.Size(72, 17);
            this.radioButtonSorceress.TabIndex = 8;
            this.radioButtonSorceress.Text = "Sorceress";
            this.radioButtonSorceress.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(208, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Note: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(208, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(214, 39);
            this.label3.TabIndex = 10;
            this.label3.Text = "- You MUST have enigma or teleport\r\nskill on your char to be able to use\r\nthe bot" +
                ".";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(208, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(153, 52);
            this.label4.TabIndex = 11;
            this.label4.Text = "- Having a setup other than a \r\n  Paladin Hammerdin might not\r\n  run as good. But" +
                " it might work\r\n  perfectly too.";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(256, 188);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.acceptChange);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(337, 188);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 13;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(208, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(201, 39);
            this.label5.TabIndex = 14;
            this.label5.Text = "- Make sure you setup your attack\r\nsequences after this. If you don\'t,\r\nyou won\'t" +
                " be able to bot.";
            // 
            // DlgCharacterBuilds
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(424, 223);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.radioButtonSorceress);
            this.Controls.Add(this.radioButtonAssassin);
            this.Controls.Add(this.radioButtonNecromancer);
            this.Controls.Add(this.radioButtonBarbarian);
            this.Controls.Add(this.radioButtonDruid);
            this.Controls.Add(this.radioButtonAmazon);
            this.Controls.Add(this.radioButtonPaladin);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DlgCharacterBuilds";
            this.Text = "Character Builds";
            this.Load += new System.EventHandler(this.DlgCharacterBuilds_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonPaladin;
        private System.Windows.Forms.RadioButton radioButtonAmazon;
        private System.Windows.Forms.RadioButton radioButtonDruid;
        private System.Windows.Forms.RadioButton radioButtonBarbarian;
        private System.Windows.Forms.RadioButton radioButtonNecromancer;
        private System.Windows.Forms.RadioButton radioButtonAssassin;
        private System.Windows.Forms.RadioButton radioButtonSorceress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label5;
    }
}