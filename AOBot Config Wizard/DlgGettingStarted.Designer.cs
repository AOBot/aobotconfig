﻿namespace AOBot_Config_Wizard
{
    partial class DlgGettingStarted
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Installing Awesom-O");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Configuring Awesom-O");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Starting Awesom-O");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Installing Auto-Awesom");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Utilising Auto-Awesom");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("(Optionnal)", new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode5});
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Beginners", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode6});
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Modifying Inis");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Run config files");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Advanced Users", new System.Windows.Forms.TreeNode[] {
            treeNode8,
            treeNode9});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgGettingStarted));
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.texte = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.texteLink = new System.Windows.Forms.LinkLabel();
            this.linkWinrar = new System.Windows.Forms.LinkLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(12, 132);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "installAO";
            treeNode1.Text = "Installing Awesom-O";
            treeNode2.Name = "configureAO";
            treeNode2.Text = "Configuring Awesom-O";
            treeNode3.Name = "startingAO";
            treeNode3.Text = "Starting Awesom-O";
            treeNode4.Name = "installAutoAwesom";
            treeNode4.Text = "Installing Auto-Awesom";
            treeNode5.Name = "utiliseAutoAwesom";
            treeNode5.Text = "Utilising Auto-Awesom";
            treeNode6.Name = "Noeud4";
            treeNode6.Text = "(Optionnal)";
            treeNode7.Name = "Beginners";
            treeNode7.Text = "Beginners";
            treeNode8.Name = "modifyingInis";
            treeNode8.Text = "Modifying Inis";
            treeNode9.Name = "runFiles";
            treeNode9.Text = "Run config files";
            treeNode10.Name = "Noeud8";
            treeNode10.Text = "Advanced Users";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode7,
            treeNode10});
            this.treeView1.Size = new System.Drawing.Size(121, 290);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.HelpChosen);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Getting started";
            // 
            // texte
            // 
            this.texte.AutoSize = true;
            this.texte.Location = new System.Drawing.Point(178, 132);
            this.texte.Name = "texte";
            this.texte.Size = new System.Drawing.Size(222, 13);
            this.texte.TabIndex = 2;
            this.texte.Text = "Click on a subject to your left to get help on it.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(175, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 33);
            this.label3.TabIndex = 3;
            this.label3.Text = "Getting Started";
            // 
            // texteLink
            // 
            this.texteLink.AutoSize = true;
            this.texteLink.Location = new System.Drawing.Point(260, 368);
            this.texteLink.Name = "texteLink";
            this.texteLink.Size = new System.Drawing.Size(0, 13);
            this.texteLink.TabIndex = 4;
            this.texteLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkClicked);
            // 
            // linkWinrar
            // 
            this.linkWinrar.AutoSize = true;
            this.linkWinrar.Location = new System.Drawing.Point(260, 394);
            this.linkWinrar.Name = "linkWinrar";
            this.linkWinrar.Size = new System.Drawing.Size(0, 13);
            this.linkWinrar.TabIndex = 5;
            this.linkWinrar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.winrarClicked);
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(504, 399);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // DlgGettingStarted
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(591, 434);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.linkWinrar);
            this.Controls.Add(this.texteLink);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.texte);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.treeView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(607, 470);
            this.MinimumSize = new System.Drawing.Size(607, 470);
            this.Name = "DlgGettingStarted";
            this.Text = "Getting Started";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label texte;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel texteLink;
        private System.Windows.Forms.LinkLabel linkWinrar;
        private System.Windows.Forms.Button button1;
    }
}