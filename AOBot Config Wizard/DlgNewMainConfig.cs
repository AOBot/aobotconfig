﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AOBot_Config_Wizard
{
    public partial class DlgNewMainConfig : Form
    {
        public DlgNewMainConfig()
        {
            InitializeComponent();
            if (DataTypes.globalconfigDone == true)
            {
                textBox1.Text = DataTypes.mainConfig.AOPath;
                textBox2.Text = DataTypes.mainConfig.GameExe;
                textBox3.Text = DataTypes.mainConfig.OwnerName;
            }
        }
        private void OpenBrowserToAO(object sender, EventArgs e)
        {
            OpenFileDialog ofdtao = new OpenFileDialog();
            ofdtao.Multiselect = false;
            ofdtao.ReadOnlyChecked = true;
            ofdtao.CheckPathExists = true;
            ofdtao.Filter = "| *Awesom-O.exe*";
            ofdtao.Title = "Select Awesom-O Executable";
            ofdtao.FileName = "Awesom-O.exe";

            if (ofdtao.ShowDialog() == DialogResult.OK)
                textBox1.Text = System.IO.Path.GetDirectoryName(ofdtao.FileName) + '\\';
        }

        private void BrowseToGame(object sender, EventArgs e)
        {
            OpenFileDialog ofdtg = new OpenFileDialog();
            ofdtg.Multiselect = false;
            ofdtg.ReadOnlyChecked = true;
            ofdtg.CheckPathExists = true;
            ofdtg.Filter = " | *Game.exe*";
            ofdtg.Title = "Select Game.exe File";
            ofdtg.FileName = "Game.exe";

            if (ofdtg.ShowDialog() == DialogResult.OK)
            {
                DataTypes.mainConfig.GamePath = System.IO.Path.GetDirectoryName(ofdtg.FileName) + '\\';
                textBox2.Text = DataTypes.mainConfig.GamePath + "game.exe";
            }
        }
        private void NewMainConfigOk(object sender, EventArgs e)
        {
            DataTypes.mainConfig.OwnerName = textBox3.Text;
            DataTypes.mainConfig.GameExe = textBox2.Text;
            DataTypes.mainConfig.AOPath = textBox1.Text;
            DataTypes.mainConfig.WindowName = "Diablo II";
            DataTypes.mainConfig.BotFile = "Bot0.ini";
            DlgConfirmNewMainConfig confirmmainconfig = new DlgConfirmNewMainConfig();
            if (confirmmainconfig.ShowDialog() == DialogResult.Yes)
            {
                try
                {
                    StreamWriter SW;
                    SW = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\Awesom-O.ini");
                    SW.WriteLine("--------------------------------------------------------");
                    SW.WriteLine("--                  INI OPTIONS                       --");
                    SW.WriteLine("--                EDIT ACCORDINGLY                    --");
                    SW.WriteLine("--------------------------------------------------------");
                    SW.WriteLine("");
                    SW.WriteLine("[Settings]");
                    SW.WriteLine("");
                    SW.WriteLine("------------------- IRC CHAT ---------------------------");
                    SW.WriteLine("IrcNickname=\"\"");
                    SW.WriteLine("IrcChannel=\"#Awesom-O\"");
                    SW.WriteLine("IrcServer=\"149.9.1.16\"");
                    SW.WriteLine("IrcPort=6667");
                    SW.WriteLine("--------- GAME PATHS, EXECUTABLES & SWITCHES -----------");
                    SW.WriteLine("GamePath=\"" + DataTypes.mainConfig.GamePath + '\"');
                    SW.WriteLine("GameExe=\"" + DataTypes.mainConfig.GameExe + '\"');
                    SW.WriteLine("KeyChange=99");
                    SW.WriteLine("Ping=999");
                    SW.WriteLine("ActiveBots=0");
                    SW.WriteLine("Debug=1");
                    SW.WriteLine("----------------- NOLOADER OPTIONS ---------------------");
                    SW.WriteLine("OwnerName=\"" + DataTypes.mainConfig.OwnerName + '\"');
                    SW.WriteLine("WindowName=\"Diablo II\"");
                    SW.WriteLine("---------------- BOT CONFIG & FILES --------------------");
                    SW.WriteLine("");
                    SW.WriteLine("[Bot0]");
                    SW.WriteLine("");
                    SW.WriteLine("File=\"Bot0.ini\"");
                    SW.WriteLine("");
                    SW.WriteLine("[Bot1]");
                    SW.WriteLine("");
                    SW.WriteLine("File=\"Bot1.ini\"");
                    SW.WriteLine("");
                    SW.WriteLine("[Bot2]");
                    SW.WriteLine("");
                    SW.WriteLine("File=\"Bot2.ini\"");
                    SW.WriteLine("");
                    SW.WriteLine("[Bot3]");
                    SW.WriteLine("");
                    SW.WriteLine("File=\"Bot3.ini\"");
                    SW.WriteLine("--------------------------------------------------------");
                    SW.WriteLine("--                 NEW INI STYLE                      --");
                    SW.WriteLine("--                 AWESOM-O 3.6                       --");
                    SW.WriteLine("--------------------------------------------------------");
                    SW.Close();
                    //Serialisation Awesom-O.ini avec Bot0 par défaut.
                    DataTypes.globalconfigDone = true;
                    Close();
                    try
                    {
                        Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers", true);
                        key.SetValue(DataTypes.mainConfig.AOPath + "Awesom-O.exe", "WIN98", Microsoft.Win32.RegistryValueKind.String);
                    }
                    catch (Exception)
                    { }
                }
                catch(DirectoryNotFoundException)
                {
                    MessageBox.Show("Please, enter your paths correctly.", "Directory Not Found");

                }
            }
        }
        
    }
}
