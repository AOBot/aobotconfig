﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AOBot_Config_Wizard
{
    public partial class DlgLeechBaalIniConfig : Form
    {
        public DlgLeechBaalIniConfig()
        {
            InitializeComponent();
            DeserializeLeechBaalIni();
        }
        private void ShopClick(object sender, EventArgs e)
        {
            try
            {
                LeechBaalIniFile.Items.Insert(LeechBaalIniFile.SelectedIndex, "Shop, 60");
            }
            catch (Exception)
            {
                LeechBaalIniFile.Items.Add("Shop, 60");
            }
        }

        private void Delete(object sender, EventArgs e)
        {
            try
            {
                Object selection = LeechBaalIniFile.SelectedItem;
                int selectionIndex = LeechBaalIniFile.SelectedIndex;
                LeechBaalIniFile.Items.Remove(selection);
                LeechBaalIniFile.SetSelected(selectionIndex, true);
            }
            catch (ArgumentOutOfRangeException)
            { }
        }

        private void MoveUp(object sender, EventArgs e)
        {
            try
            {

                int selection = LeechBaalIniFile.SelectedIndex;
                LeechBaalIniFile.Items.Insert(selection - 1, LeechBaalIniFile.SelectedItem);
                LeechBaalIniFile.Items.RemoveAt(selection + 1);
                LeechBaalIniFile.SetSelected(selection - 1, true);

            }
            catch (ArgumentOutOfRangeException)
            { }

        }

        private void MoveDown(object sender, EventArgs e)
        {
            try
            {
                int selection = LeechBaalIniFile.SelectedIndex;
                Object selectedItem = LeechBaalIniFile.SelectedItem;
                if (selectedItem != null)
                {

                    LeechBaalIniFile.Items.Insert(selection + 2, LeechBaalIniFile.SelectedItem);
                    LeechBaalIniFile.Items.RemoveAt(selection);
                    LeechBaalIniFile.SetSelected(selection + 1, true);
                }

            }
            catch (ArgumentOutOfRangeException)
            { }
        }

        private void Sell(object sender, EventArgs e)
        {
            try
            {
                LeechBaalIniFile.Items.Insert(LeechBaalIniFile.SelectedIndex, "Sell, 60");
            }
            catch (Exception)
            {
                LeechBaalIniFile.Items.Add("Sell, 60");
            }
        }

        private void Heal(object sender, EventArgs e)
        {
            try
            {
                LeechBaalIniFile.Items.Insert(LeechBaalIniFile.SelectedIndex, "Heal, 60");
            }
            catch (Exception)
            {
                LeechBaalIniFile.Items.Add("Heal, 60");
            }
        }

        private void Resurrect(object sender, EventArgs e)
        {
            try
            {
                LeechBaalIniFile.Items.Insert(LeechBaalIniFile.SelectedIndex, "Resurrect, 60");
            }
            catch (Exception)
            {
                LeechBaalIniFile.Items.Add("Resurrect, 60");
            }
        }

        private void Repair(object sender, EventArgs e)
        {
            try
            {
                LeechBaalIniFile.Items.Insert(LeechBaalIniFile.SelectedIndex, "Repair, 60");
            }
            catch (Exception)
            {
                LeechBaalIniFile.Items.Add("Repair, 60");
            }
        }

        private void Stash(object sender, EventArgs e)
        {
            try
            {
                LeechBaalIniFile.Items.Insert(LeechBaalIniFile.SelectedIndex, "Stash, 60");
            }
            catch (Exception)
            {
                LeechBaalIniFile.Items.Add("Stash, 60");
            }
        }

        private void Gamble(object sender, EventArgs e)
        {
            try
            {
                LeechBaalIniFile.Items.Insert(LeechBaalIniFile.SelectedIndex, "Gamble, 180");
            }
            catch (Exception)
            {
                LeechBaalIniFile.Items.Add("Gamble, 180");
            }
        }

        private void LeechBaal(object sender, EventArgs e)
        {
            try
            {
                LeechBaalIniFile.Items.Insert(LeechBaalIniFile.SelectedIndex, "LeechBaal, 600");
            }
            catch (Exception)
            {
                LeechBaalIniFile.Items.Add("LeechBaal, 600");
            }
        }

        private void ActChange(object sender, EventArgs e)
        {
            DlgActChange act = new DlgActChange();
            if (act.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    LeechBaalIniFile.Items.Insert(LeechBaalIniFile.SelectedIndex, DataTypes.globalActChange);
                }
                catch (Exception)
                {
                    LeechBaalIniFile.Items.Add(DataTypes.globalActChange);
                }
            }
        }

        private void Sleep(object sender, EventArgs e)
        {
            DlgSleep sleep = new DlgSleep();
            if (sleep.ShowDialog() == DialogResult.OK)
                try
                {
                    LeechBaalIniFile.Items.Insert(LeechBaalIniFile.SelectedIndex, "Sleep, " + DataTypes.globalSleepMS);
                }
                catch (Exception)
                {
                    LeechBaalIniFile.Items.Add("Sleep, " + DataTypes.globalSleepMS);
                }
        }

        private void acceptLeechBaal(object sender, EventArgs e)
        {
            StreamWriter stw;
            stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\LeechBaal.ini");
            for (int i = 1; i <= LeechBaalIniFile.Items.Count; ++i)
            {
                LeechBaalIniFile.SetSelected(i - 1, true);
                stw.WriteLine(LeechBaalIniFile.SelectedItem);
            }
            stw.Close();
            this.Close();
        }
        private void DeserializeLeechBaalIni()
        {
            StreamReader str;
            str = File.OpenText(DataTypes.mainConfig.AOPath + "config\\ini\\LeechBaal.ini");
            while (!str.EndOfStream)
                LeechBaalIniFile.Items.Add(str.ReadLine());
        }
    }
}
