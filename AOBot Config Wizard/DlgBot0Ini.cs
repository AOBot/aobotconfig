﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace AOBot_Config_Wizard
{
    public partial class DlgBot0Ini : Form
    {
        int p_botTNumber;
        string botFile = "";
        public DlgBot0Ini(int p_botNumber)
        {
            InitializeComponent();
            p_botTNumber = p_botNumber;
            switch (p_botTNumber)
            {
                case 0: botFile = "bot0.ini"; break;
                case 1: botFile = "bot1.ini"; break;
                case 2: botFile = "bot2.ini"; break;
                case 3: botFile = "bot3.ini"; break;
                default: botFile = "bot0.ini"; break;
            }
            
            if (DataTypes.currentBotIni.ConfigDone)
            {
                textBoxAccountName.Text = DataTypes.currentBotIni.AccountName;
                textBoxAccountPassword.Text = DataTypes.currentBotIni.Password;
                textBoxCharName.Text = DataTypes.currentBotIni.CharName;
                textBoxGambleFrom.Text = Convert.ToString(DataTypes.currentBotIni.GambleFrom);
                textBoxGambleTo.Text = Convert.ToString(DataTypes.currentBotIni.GambleTo);
                textBoxInventoryRow1.Text = DataTypes.currentBotIni.InventoryRow1;
                textBoxInventoryRow2.Text = DataTypes.currentBotIni.InventoryRow2;
                textBoxInventoryRow3.Text = DataTypes.currentBotIni.InventoryRow3;
                textBoxInventoryRow4.Text = DataTypes.currentBotIni.InventoryRow4;
                textBoxGamePassword.Text = DataTypes.currentBotIni.GamePass;
                
                if (DataTypes.currentBotIni.GameName == "RandomGameNames")
                {
                    checkBox1.Checked = true;
                    textBoxGameName.Text = "Run-%d";
                }
                else
                {
                    checkBox1.Checked = false;
                    textBoxGameName.Text = DataTypes.currentBotIni.GameName;
                }
                
                if (DataTypes.currentBotIni.ActiveRun == "Baal.ini")
                    radioButtonBaal.Checked = true;
                else if (DataTypes.currentBotIni.ActiveRun == "Chaos.ini")
                        radioButtonChaos.Checked = true;
                    else if (DataTypes.currentBotIni.ActiveRun == "CoBaal.ini")
                            radioButtonCoBaal.Checked = true;
                        else if (DataTypes.currentBotIni.ActiveRun == "Key.ini")
                                radioButtonKey.Checked = true;
                            else if (DataTypes.currentBotIni.ActiveRun == "MF.ini")
                                    radioButtonMF.Checked = true;
                                else if (DataTypes.currentBotIni.ActiveRun == "LeechBaal.ini")
                                        radioButtonLeechBaal.Checked = true;
                                    else
                                        radioButtonBaal.Checked = true;
                
                if (DataTypes.currentBotIni.Difficulty == "Hell")
                    radioButtonHellDifficulty.Checked = true;
                else if (DataTypes.currentBotIni.Difficulty == "Nightmare")
                        radioButtonNightMareDifficulty.Checked = true;
                    else if (DataTypes.currentBotIni.Difficulty == "Normal")
                            radioButtonNormalDifficulty.Checked = true;
                        else
                            radioButtonHellDifficulty.Checked = true;
                
                if (DataTypes.currentBotIni.PickItSetting == "Greedy")
                    radioButtonGreedy.Checked = true;
                else if (DataTypes.currentBotIni.PickItSetting == "Moderate")
                        radioButtonModerate.Checked = true;
                    else if (DataTypes.currentBotIni.PickItSetting == "Strict")
                            radioButtonStrict.Checked = true;
                        else
                            radioButtonModerate.Checked = true;
                
                if (DataTypes.currentBotIni.PublicChat == "1")
                    radioButtonPublicChatOn.Checked = true;
                else
                    radioButtonPublicChatOff.Checked = true;
                }
        }
        private void EnableGameName(object sender, EventArgs e)
        {
            textBoxGameName.Enabled = (! checkBox1.Checked);
        }
        private void BotOIniOk(object sender, EventArgs e)
        {
            if (textBoxAccountName.Text.Length == 0 || textBoxAccountPassword.Text.Length == 0 || textBoxCharName.Text.Length == 0 || (textBoxGameName.Text.Length == 0 && checkBox1.Checked == false) || textBoxCharName.Text == "(Must be an Hammerdin)")
                MessageBox.Show("You have some empty or wrong fields, please fill all of them correctly.", "Missing or Bad Information");
            else
                if (textBoxInventoryRow1.Text.Length < 10 || textBoxInventoryRow2.Text.Length < 10 || textBoxInventoryRow3.Text.Length < 10 || textBoxInventoryRow4.Text.Length < 10)
                    MessageBox.Show("Your inventory rows config must contain 10 numbers each row.", "Incorrect Inventory Config");
                else
                    if (Convert.ToInt32(textBoxGambleFrom.Text) < Convert.ToInt32(textBoxGambleTo.Text))
                        MessageBox.Show("The GambleFrom number must be bigger than the GambleTo number, please correct it.", "Incorrect Gambling Config");
                    else
                    {
                        DlgConfirmBotConfig bot0confirm = new DlgConfirmBotConfig();
                        if (bot0confirm.ShowDialog() == DialogResult.Yes)
                        {
                            DataTypes.currentBotIni.AccountName = textBoxAccountName.Text;
                            DataTypes.currentBotIni.Password = textBoxAccountPassword.Text;
                            DataTypes.currentBotIni.CharName = textBoxCharName.Text;
                            DataTypes.currentBotIni.GambleFrom = textBoxGambleFrom.Text;
                            DataTypes.currentBotIni.GambleTo = textBoxGambleTo.Text;
                            if (radioButtonBaal.Checked)
                                DataTypes.currentBotIni.ActiveRun = "Baal.ini";
                            else if (radioButtonCoBaal.Checked)
                                    DataTypes.currentBotIni.ActiveRun = "CoBaal.ini";
                                else if (radioButtonChaos.Checked)
                                        DataTypes.currentBotIni.ActiveRun = "Chaos.ini";
                                    else if (radioButtonKey.Checked)
                                            DataTypes.currentBotIni.ActiveRun = "Key.ini";
                                        else if (radioButtonLeechBaal.Checked)
                                                DataTypes.currentBotIni.ActiveRun = "LeechBaal.ini";
                                            else if (radioButtonMF.Checked)
                                                    DataTypes.currentBotIni.ActiveRun = "MF.ini";
                                                else
                                                    DataTypes.currentBotIni.ActiveRun = "MF.ini";
                            
                            DataTypes.currentBotIni.InventoryRow1 = textBoxInventoryRow1.Text;
                            DataTypes.currentBotIni.InventoryRow2 = textBoxInventoryRow2.Text;
                            DataTypes.currentBotIni.InventoryRow3 = textBoxInventoryRow3.Text;
                            DataTypes.currentBotIni.InventoryRow4 = textBoxInventoryRow4.Text;
                            
                            if (radioButtonHellDifficulty.Checked)
                                DataTypes.currentBotIni.Difficulty = "Hell";
                            else if (radioButtonNightMareDifficulty.Checked)
                                    DataTypes.currentBotIni.Difficulty = "Nightmare";
                                else if (radioButtonNormalDifficulty.Checked)
                                        DataTypes.currentBotIni.Difficulty = "Normal";
                                    else
                                        DataTypes.currentBotIni.Difficulty = "Hell";
                            
                            if (checkBox1.Checked)
                                DataTypes.currentBotIni.GameName = "RandomGameNames";
                            else
                                DataTypes.currentBotIni.GameName = textBoxGameName.Text;
                            
                            DataTypes.currentBotIni.GamePass = textBoxGamePassword.Text;
                            
                            if (radioButtonPublicChatOn.Checked)
                                DataTypes.currentBotIni.PublicChat = "1";
                            else
                                DataTypes.currentBotIni.PublicChat = "0";
                            
                            if (radioButtonGreedy.Checked)
                                DataTypes.currentBotIni.PickItSetting = "Greedy";
                            else if (radioButtonModerate.Checked)
                                    DataTypes.currentBotIni.PickItSetting = "Moderate";
                                else if (radioButtonStrict.Checked)
                                        DataTypes.currentBotIni.PickItSetting = "Strict";
                                    else
                                        DataTypes.currentBotIni.PickItSetting = "Moderate";
                            
                            SerializeToConfigFile();
                        }
                    }
            switch (p_botTNumber)
            {
                case 0: DataTypes.botIni0 = DataTypes.currentBotIni; break;
                case 1: DataTypes.botIni1 = DataTypes.currentBotIni; break;
                case 2: DataTypes.botIni2 = DataTypes.currentBotIni; break;
                case 3: DataTypes.botIni3 = DataTypes.currentBotIni; break;
                default: DataTypes.botIni0 = DataTypes.currentBotIni; break;
            }
        }
        private void numericValidation(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
            if (Convert.ToInt32(e.KeyChar) == 48 || Convert.ToInt32(e.KeyChar) == 49 || Convert.ToInt32(e.KeyChar) == 8 || Convert.ToInt32(e.KeyChar) == 46)
                e.Handled = false;
        }
        private string ModifyLine(string p_content, string p_textToFind, string p_textToPut)
        {
            try
            {
                int start = p_content.IndexOf(p_textToFind);
                int end = p_content.IndexOf('\r', start);

                string part = p_content.Substring(start, (end - start));
                p_content = Regex.Replace(p_content, part, p_textToPut);
            }
            catch (Exception)
            { }
            return p_content;
        }
        private void SerializeToConfigFile()
        {
            try
            {
                StreamReader reader = new StreamReader(DataTypes.mainConfig.AOPath + "config\\ini\\" + botFile);
                string content = reader.ReadToEnd();
                reader.Close();                
                
                for (; ; )
                {
                    try
                    {
                        int start = content.IndexOf("-BotFile=", 0);
                        int end = content.IndexOf('\r', start);
                        string part = content.Substring(start, (end - start));

                        content = content.Remove(start, (end - start));
                    }
                    catch (Exception)
                    {
                        break;
                    }
                }
                content = ModifyLine(content, "BotFile=", "BotFile=\"" + DataTypes.currentBotIni.ActiveRun + "\"");

                if (DataTypes.currentBotIni.ActiveRun == "LeechBaal.ini")
                    content = ModifyLine(content, "LeechActive=", "LeechActive=1");
                else
                    content = ModifyLine(content, "LeechActive=", "LeechActive=0");
                
                if (DataTypes.currentBotIni.GameName != "RandomGameNames")
                {
                    content = ModifyLine(content, "GameName=", "GameName=\"" + DataTypes.currentBotIni.GameName + "\"");
                    content = ModifyLine(content, "MakeRandom=", "MakeRandom=0");
                }
                else
                {
                    content = ModifyLine(content, "GameName=", "GameName=\"Run-%d\"");
                    content = ModifyLine(content, "MakeRandom=", "MakeRandom=1");
                }
                for (; ; )
                {
                    try
                    {
                        int start = content.IndexOf("-PickLua=", 0);

                        int end = content.IndexOf('\r', start);
                        string part = content.Substring(start, (end - start));

                        content = content.Remove(start, (end - start));
                    }
                    catch (Exception)
                    {
                        break;
                    }
                }
                for (; ; )
                {
                    try
                    {
                        int start = content.IndexOf("-DumpLua=", 0);

                        int end = content.IndexOf('\r', start);
                        string part = content.Substring(start, (end - start));

                        content = content.Remove(start, (end - start));
                    }
                    catch (Exception)
                    {
                        break;
                    }
                }
                for (; ; )
                {
                    try
                    {
                        int start = content.IndexOf("-IdentifyLua=", 0);

                        int end = content.IndexOf('\r', start);
                        string part = content.Substring(start, (end - start));

                        content = content.Remove(start, (end - start));
                    }
                    catch (Exception)
                    {
                        break;
                    }
                }
                for (; ; )
                {
                    try
                    {
                        int start = content.IndexOf("-SellLua=", 0);

                        int end = content.IndexOf('\r', start);
                        string part = content.Substring(start, (end - start));

                        content = content.Remove(start, (end - start));
                    }
                    catch (Exception)
                    {
                        break;
                    }
                }
                for (; ; )
                {
                    try
                    {
                        int start = content.IndexOf("-GambleLua=", 0);

                        int end = content.IndexOf('\r', start);
                        string part = content.Substring(start, (end - start));

                        content = content.Remove(start, (end - start));
                    }
                    catch (Exception)
                    {
                        break;
                    }
                }
                string[,] changes = new string[30, 2];

                changes.SetValue("Inventory" + '[' + '0' + ']' + '=', 0, 0);
                changes.SetValue("Inventory" + '[' + '0' + ']' + '=' + "\"" + DataTypes.currentBotIni.InventoryRow1 + "\"", 0, 1);

                changes.SetValue("Inventory[1]=", 1, 0);
                changes.SetValue("Inventory[1]=\"" + DataTypes.currentBotIni.InventoryRow2 + "\"", 1, 1);

                changes.SetValue("Inventory[2]=", 2, 0);
                changes.SetValue("Inventory[2]=\"" + DataTypes.currentBotIni.InventoryRow3 + "\"", 2, 1);

                changes.SetValue("Inventory[3]=", 3, 0);
                changes.SetValue("Inventory[3]=\"" + DataTypes.currentBotIni.InventoryRow4 + "\"", 3, 1);

                changes.SetValue("Username=", 4, 0);
                changes.SetValue("Username=\"" + DataTypes.currentBotIni.AccountName + "\"", 4, 1);

                changes.SetValue("Charname=", 5, 0);
                changes.SetValue("Charname=\"" + DataTypes.currentBotIni.CharName + "\"", 5, 1);

                changes.SetValue("Difficulty=", 6, 0);
                changes.SetValue("Difficulty=" + DataTypes.currentBotIni.Difficulty, 6, 1);

                changes.SetValue("GambleFrom=", 7, 0);
                changes.SetValue("GambleFrom=" + DataTypes.currentBotIni.GambleFrom, 7, 1);

                changes.SetValue("GambleTo=", 8, 0);
                changes.SetValue("GambleTo=" + DataTypes.currentBotIni.GambleTo, 8, 1);

                changes.SetValue("GamePass=", 9, 0);
                changes.SetValue("GamePass=\"" + DataTypes.currentBotIni.GamePass + "\"", 9, 1);

                changes.SetValue("Password=", 10, 0);
                changes.SetValue("Password=\"" + DataTypes.currentBotIni.Password + "\"", 10, 1);

                changes.SetValue("PublicChat=", 11, 0);
                changes.SetValue("PublicChat=" + DataTypes.currentBotIni.PublicChat, 11, 1);

                changes.SetValue("LeechMasterAccount=", 12, 0);
                changes.SetValue("LeechMasterAccount=\"" + DataTypes.currentBotIni.RunnerAccount + "\"", 12, 1);

                changes.SetValue("LeechMasterCharacter=", 13, 0);
                changes.SetValue("LeechMasterCharacter=\"" + DataTypes.currentBotIni.RunnerCharName + "\"", 13, 1);

                changes.SetValue("LeechGamePassword=", 14, 0);
                changes.SetValue("LeechGamePassword=\"" + DataTypes.currentBotIni.RunnerGamePass + "\"", 14, 1);

                changes.SetValue("PickLua=", 15, 0);
                changes.SetValue("PickLua=\"" + DataTypes.currentBotIni.PickItSetting + "\\" + "pickItem.lua\"", 15, 1);

                changes.SetValue("DumpLua=", 16, 0);
                changes.SetValue("DumpLua=\"" + DataTypes.currentBotIni.PickItSetting + "\\dumpItem.lua\"", 16, 1);

                changes.SetValue("IdentifyLua=", 17, 0);
                changes.SetValue("IdentifyLua=\"" + DataTypes.currentBotIni.PickItSetting + "\\identifyItem.lua\"", 17, 1);

                changes.SetValue("SellLua=", 18, 0);
                changes.SetValue("SellLua=\"" + DataTypes.currentBotIni.PickItSetting + "\\sellItem.lua\"", 18, 1);

                changes.SetValue("GambleLua=", 19, 0);
                changes.SetValue("GambleLua=\"" + DataTypes.currentBotIni.PickItSetting + "\\gambleItem.lua\"", 19, 1);

                changes.SetValue("LoaderFlags=", 20, 0);
                changes.SetValue("LoaderFlags=" + "\"\"", 20, 1);

                changes.SetValue("InventoryLock=", 21, 0);
                changes.SetValue("InventoryLock=1", 21, 1);

                for (int i = 0; i != 22; ++i)
                {
                    content = ModifyLine(content, changes[i, 0], changes[i, 1]);
                }
                changes.Initialize();
                int index = 0;
                for (; ; )
                {
                    int start2 = content.IndexOf("Inventory", index);
                    int end2 = content.IndexOf('\r', start2);
                    string part2 = content.Substring(start2, (end2 - start2));
                    if (part2.Substring(part2.IndexOf('y') + 2, 1) == "0")
                        content = content.Replace(part2, "Inventory[0]=\"" + DataTypes.currentBotIni.InventoryRow1 + '\"');
                    else if (part2.Substring(part2.IndexOf('y') + 2, 1) == "1")
                        content = content.Replace(part2, "Inventory[1]=\"" + DataTypes.currentBotIni.InventoryRow2 + '\"');
                    else if (part2.Substring(part2.IndexOf('y') + 2, 1) == "2")
                        content = content.Replace(part2, "Inventory[2]=\"" + DataTypes.currentBotIni.InventoryRow3 + '\"');
                    else if (part2.Substring(part2.IndexOf('y') + 2, 1) == "3")
                    {
                        content = content.Replace(part2, "Inventory[3]=\"" + DataTypes.currentBotIni.InventoryRow4 + '\"');
                        break;
                    }
                    else
                    { }
                    index = end2;
                }
                StreamWriter stw;  //Ecrit le texte modifié
                stw = File.CreateText(DataTypes.mainConfig.AOPath + "config//ini//" + botFile);
                stw.Write(content);
                stw.Close();
                DataTypes.currentBotIni.ConfigDone = true;
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("You must first config your Awesom-O.ini file using Main Config menu.", "No Main Config");
                this.Close();
            }
            catch (FileNotFoundException)
            {
                StreamReader str;
                str = File.OpenText(DataTypes.mainConfig.AOPath + "config//ini//bot0.ini");
                string content = str.ReadToEnd();
                str.Close();

                StreamWriter stw;
                stw = File.CreateText(DataTypes.mainConfig.AOPath + "config//ini//" + botFile);
                stw.Write(content);
                stw.Close();

                SerializeToConfigFile();
            }
        }
        private void ClearTextBox(object sender, EventArgs e)
        {
            textBoxCharName.Clear();
        }
        private void NoSpacesValidation(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
            if (Convert.ToInt32(e.KeyChar) == 32)
                e.Handled = true;
        }
        private void LeechBaalInfoDialog(object sender, EventArgs e)
        {
            DlgBot0LeechBaalInfo leechBaal = new DlgBot0LeechBaalInfo();
            leechBaal.ShowDialog();
        }
        private void allChatOffButton(object sender, EventArgs e)
        {
            try
            {
                StreamReader str;
                str = File.OpenText(DataTypes.mainConfig.AOPath + "config\\ini\\" + botFile);
                string content = str.ReadToEnd();
                str.Close();

                content = ModifyLine(content, "Chat=", "Chat=0");
                content = ModifyLine(content, "ChatMessages=", "ChatMessages=0");
                content = ModifyLine(content, "PublicChat=", "PublicChat=0");
                content = ModifyLine(content, "JoinChannel=", "JoinChannel=\"\"");
                content = ModifyLine(content, "ChatMessage=", "ChatMessage=\"\"");
                content = ModifyLine(content, "TooShortMessage=", "TooShortMessage=\"\"");
                content = ModifyLine(content, "DndMessage=", "DndMessage=\"\"");
                content = ModifyLine(content, "ChickenExitMessage=", "ChickenExitMessage=\"\"");
                content = ModifyLine(content, "ChickenTownMessage=", "ChickenTownMessage=\"\"");
                content = ModifyLine(content, "BaalStartMessage=", "BaalStartMessage=\"\"");
                content = ModifyLine(content, "BaalHotMessage=", "BaalHotMessage=\"\"");
                content = ModifyLine(content, "BaalWarmMessage=", "BaalWarmMessage=\"\"");
                content = ModifyLine(content, "BaalColdMessage=", "BaalColdMessage=\"\"");
                content = ModifyLine(content, "NgMessage=", "NgMessage=\"\"");
                content = ModifyLine(content, "BaalKillMessage=", "BaalKillMessage=\"\"");
                content = ModifyLine(content, "VoteMessage=", "VoteMessage=\"\"");
                content = ModifyLine(content, "DiabloStartMessage=", "DiabloStartMessage=\"\"");
                content = ModifyLine(content, "DiabloHotMessage=", "DiabloHotMessage=\"\"");
                content = ModifyLine(content, "DiabloWarmMessage=", "DiabloWarmMessage=\"\"");
                content = ModifyLine(content, "DiabloColdMessage=", "DiabloColdMessage=\"\"");
                content = ModifyLine(content, "DiabloKillMessage=", "DiabloKillMessage=\"\"");
                content = ModifyLine(content, "NextBossMessage=", "NextBossMessage=\"\"");

                StreamWriter stw;
                stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + botFile);
                stw.Write(content);
                stw.Close();

                radioButtonPublicChatOff.Checked = true;
                radioButtonPublicChatOn.Checked = false;

                MessageBox.Show("All chat has been turned off (Safer this way).", "Chat turned off", MessageBoxButtons.OK);
            }
            catch (FileNotFoundException)
            {
                StreamReader str;
                str = File.OpenText(DataTypes.mainConfig.AOPath + "config//ini//bot0.ini");
                string content = str.ReadToEnd();
                str.Close();

                StreamWriter stw;
                stw = File.CreateText(DataTypes.mainConfig.AOPath + "config//ini//" + botFile);
                stw.Write(content);
                stw.Close();

                allChatOffButton(sender, e);
            }
        }
        private void ResetDelays(object sender, EventArgs e)
        {
            try
            {
                StreamReader str;
                str = File.OpenText(DataTypes.mainConfig.AOPath + "config\\ini\\" + botFile);
                string content = str.ReadToEnd();
                str.Close();

                content = ModifyLine(content, "ClickDelay=", "ClickDelay=80");
                content = ModifyLine(content, "KeyDelay=", "KeyDelay=40");
                content = ModifyLine(content, "CreateDelay=", "CreateDelay=12000");
                content = ModifyLine(content, "LaunchDelay=", "LaunchDelay=8000");
                content = ModifyLine(content, "GameTimeout=", "GameTimeout=0");
                content = ModifyLine(content, "TeleportDelay=", "TeleportDelay=600");
                content = ModifyLine(content, "WalkDelay=", "WalkDelay=70");
                content = ModifyLine(content, "KillDelay=", "KillDelay=2500");
                content = ModifyLine(content, "TempBanDelay=", "TempBanDelay=900000");
                content = ModifyLine(content, "MinGameTime=", "MinGameTime=220");


                StreamWriter stw;
                stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + botFile);
                stw.Write(content);
                stw.Close();

                radioButtonPublicChatOff.Checked = true;
                radioButtonPublicChatOn.Checked = false;

                MessageBox.Show("All delays have been reseted to default.", "Default Delays", MessageBoxButtons.OK);
            }
            catch (FileNotFoundException)
            {
                StreamReader str;
                str = File.OpenText(DataTypes.mainConfig.AOPath + "config//ini//bot0.ini");
                string content = str.ReadToEnd();
                str.Close();

                StreamWriter stw;
                stw = File.CreateText(DataTypes.mainConfig.AOPath + "config//ini//" + botFile);
                stw.Write(content);
                stw.Close();

                allChatOffButton(sender, e);
            }
        }
    }
}