﻿namespace AOBot_Config_Wizard
{
    partial class DlgClassic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgClassic));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.goldPileTextBoxe = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.whiteWeapons = new System.Windows.Forms.CheckBox();
            this.whiteShields = new System.Windows.Forms.CheckBox();
            this.whiteGems = new System.Windows.Forms.CheckBox();
            this.whiteHelms = new System.Windows.Forms.CheckBox();
            this.whiteArmors = new System.Windows.Forms.CheckBox();
            this.setItemscheck = new System.Windows.Forms.CheckBox();
            this.magicWeapons = new System.Windows.Forms.CheckBox();
            this.magicShields = new System.Windows.Forms.CheckBox();
            this.magicRings = new System.Windows.Forms.CheckBox();
            this.magicHelms = new System.Windows.Forms.CheckBox();
            this.magicArmors = new System.Windows.Forms.CheckBox();
            this.magicAmmys = new System.Windows.Forms.CheckBox();
            this.rareWeapons = new System.Windows.Forms.CheckBox();
            this.rareShields = new System.Windows.Forms.CheckBox();
            this.rareRings = new System.Windows.Forms.CheckBox();
            this.rareHelms = new System.Windows.Forms.CheckBox();
            this.rareGloves = new System.Windows.Forms.CheckBox();
            this.rareBoots = new System.Windows.Forms.CheckBox();
            this.rareBelts = new System.Windows.Forms.CheckBox();
            this.rareArmors = new System.Windows.Forms.CheckBox();
            this.rareAmmys = new System.Windows.Forms.CheckBox();
            this.uniqueWeapons = new System.Windows.Forms.CheckBox();
            this.uniqueShields = new System.Windows.Forms.CheckBox();
            this.uniqueRings = new System.Windows.Forms.CheckBox();
            this.uniqueHelms = new System.Windows.Forms.CheckBox();
            this.uniqueGloves = new System.Windows.Forms.CheckBox();
            this.uniqueBoots = new System.Windows.Forms.CheckBox();
            this.uniqueBelts = new System.Windows.Forms.CheckBox();
            this.uniqueArmors = new System.Windows.Forms.CheckBox();
            this.uniqueAmmys = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.uniqueButton = new System.Windows.Forms.Button();
            this.rareButton = new System.Windows.Forms.Button();
            this.magicButton = new System.Windows.Forms.Button();
            this.setButton = new System.Windows.Forms.Button();
            this.whiteButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.goldPileTextBoxe);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(223, 67);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General Settings";
            // 
            // goldPileTextBoxe
            // 
            this.goldPileTextBoxe.Location = new System.Drawing.Point(111, 26);
            this.goldPileTextBoxe.Name = "goldPileTextBoxe";
            this.goldPileTextBoxe.Size = new System.Drawing.Size(106, 20);
            this.goldPileTextBoxe.TabIndex = 1;
            this.goldPileTextBoxe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericValidation);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Minimum Gold Pile :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.whiteWeapons);
            this.groupBox2.Controls.Add(this.whiteShields);
            this.groupBox2.Controls.Add(this.whiteGems);
            this.groupBox2.Controls.Add(this.whiteHelms);
            this.groupBox2.Controls.Add(this.whiteArmors);
            this.groupBox2.Controls.Add(this.setItemscheck);
            this.groupBox2.Controls.Add(this.magicWeapons);
            this.groupBox2.Controls.Add(this.magicShields);
            this.groupBox2.Controls.Add(this.magicRings);
            this.groupBox2.Controls.Add(this.magicHelms);
            this.groupBox2.Controls.Add(this.magicArmors);
            this.groupBox2.Controls.Add(this.magicAmmys);
            this.groupBox2.Controls.Add(this.rareWeapons);
            this.groupBox2.Controls.Add(this.rareShields);
            this.groupBox2.Controls.Add(this.rareRings);
            this.groupBox2.Controls.Add(this.rareHelms);
            this.groupBox2.Controls.Add(this.rareGloves);
            this.groupBox2.Controls.Add(this.rareBoots);
            this.groupBox2.Controls.Add(this.rareBelts);
            this.groupBox2.Controls.Add(this.rareArmors);
            this.groupBox2.Controls.Add(this.rareAmmys);
            this.groupBox2.Controls.Add(this.uniqueWeapons);
            this.groupBox2.Controls.Add(this.uniqueShields);
            this.groupBox2.Controls.Add(this.uniqueRings);
            this.groupBox2.Controls.Add(this.uniqueHelms);
            this.groupBox2.Controls.Add(this.uniqueGloves);
            this.groupBox2.Controls.Add(this.uniqueBoots);
            this.groupBox2.Controls.Add(this.uniqueBelts);
            this.groupBox2.Controls.Add(this.uniqueArmors);
            this.groupBox2.Controls.Add(this.uniqueAmmys);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(12, 85);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(513, 264);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Includes";
            // 
            // whiteWeapons
            // 
            this.whiteWeapons.AutoSize = true;
            this.whiteWeapons.Location = new System.Drawing.Point(417, 143);
            this.whiteWeapons.Name = "whiteWeapons";
            this.whiteWeapons.Size = new System.Drawing.Size(72, 17);
            this.whiteWeapons.TabIndex = 37;
            this.whiteWeapons.Text = "Weapons";
            this.whiteWeapons.UseVisualStyleBackColor = true;
            // 
            // whiteShields
            // 
            this.whiteShields.AutoSize = true;
            this.whiteShields.Location = new System.Drawing.Point(417, 120);
            this.whiteShields.Name = "whiteShields";
            this.whiteShields.Size = new System.Drawing.Size(60, 17);
            this.whiteShields.TabIndex = 36;
            this.whiteShields.Text = "Shields";
            this.whiteShields.UseVisualStyleBackColor = true;
            // 
            // whiteGems
            // 
            this.whiteGems.AutoSize = true;
            this.whiteGems.Location = new System.Drawing.Point(417, 74);
            this.whiteGems.Name = "whiteGems";
            this.whiteGems.Size = new System.Drawing.Size(53, 17);
            this.whiteGems.TabIndex = 35;
            this.whiteGems.Text = "Gems";
            this.whiteGems.UseVisualStyleBackColor = true;
            // 
            // whiteHelms
            // 
            this.whiteHelms.AutoSize = true;
            this.whiteHelms.Location = new System.Drawing.Point(417, 97);
            this.whiteHelms.Name = "whiteHelms";
            this.whiteHelms.Size = new System.Drawing.Size(55, 17);
            this.whiteHelms.TabIndex = 34;
            this.whiteHelms.Text = "Helms";
            this.whiteHelms.UseVisualStyleBackColor = true;
            // 
            // whiteArmors
            // 
            this.whiteArmors.AutoSize = true;
            this.whiteArmors.Location = new System.Drawing.Point(417, 51);
            this.whiteArmors.Name = "whiteArmors";
            this.whiteArmors.Size = new System.Drawing.Size(58, 17);
            this.whiteArmors.TabIndex = 33;
            this.whiteArmors.Text = "Armors";
            this.whiteArmors.UseVisualStyleBackColor = true;
            // 
            // setItemscheck
            // 
            this.setItemscheck.AutoSize = true;
            this.setItemscheck.Location = new System.Drawing.Point(311, 51);
            this.setItemscheck.Name = "setItemscheck";
            this.setItemscheck.Size = new System.Drawing.Size(70, 17);
            this.setItemscheck.TabIndex = 32;
            this.setItemscheck.Text = "Set Items";
            this.setItemscheck.UseVisualStyleBackColor = true;
            // 
            // magicWeapons
            // 
            this.magicWeapons.AutoSize = true;
            this.magicWeapons.Checked = true;
            this.magicWeapons.CheckState = System.Windows.Forms.CheckState.Checked;
            this.magicWeapons.Location = new System.Drawing.Point(212, 166);
            this.magicWeapons.Name = "magicWeapons";
            this.magicWeapons.Size = new System.Drawing.Size(72, 17);
            this.magicWeapons.TabIndex = 31;
            this.magicWeapons.Text = "Weapons";
            this.magicWeapons.UseVisualStyleBackColor = true;
            // 
            // magicShields
            // 
            this.magicShields.AutoSize = true;
            this.magicShields.Location = new System.Drawing.Point(212, 143);
            this.magicShields.Name = "magicShields";
            this.magicShields.Size = new System.Drawing.Size(60, 17);
            this.magicShields.TabIndex = 30;
            this.magicShields.Text = "Shields";
            this.magicShields.UseVisualStyleBackColor = true;
            // 
            // magicRings
            // 
            this.magicRings.AutoSize = true;
            this.magicRings.Location = new System.Drawing.Point(212, 120);
            this.magicRings.Name = "magicRings";
            this.magicRings.Size = new System.Drawing.Size(53, 17);
            this.magicRings.TabIndex = 29;
            this.magicRings.Text = "Rings";
            this.magicRings.UseVisualStyleBackColor = true;
            // 
            // magicHelms
            // 
            this.magicHelms.AutoSize = true;
            this.magicHelms.Location = new System.Drawing.Point(212, 97);
            this.magicHelms.Name = "magicHelms";
            this.magicHelms.Size = new System.Drawing.Size(55, 17);
            this.magicHelms.TabIndex = 28;
            this.magicHelms.Text = "Helms";
            this.magicHelms.UseVisualStyleBackColor = true;
            // 
            // magicArmors
            // 
            this.magicArmors.AutoSize = true;
            this.magicArmors.Location = new System.Drawing.Point(212, 74);
            this.magicArmors.Name = "magicArmors";
            this.magicArmors.Size = new System.Drawing.Size(58, 17);
            this.magicArmors.TabIndex = 24;
            this.magicArmors.Text = "Armors";
            this.magicArmors.UseVisualStyleBackColor = true;
            // 
            // magicAmmys
            // 
            this.magicAmmys.AutoSize = true;
            this.magicAmmys.Location = new System.Drawing.Point(212, 51);
            this.magicAmmys.Name = "magicAmmys";
            this.magicAmmys.Size = new System.Drawing.Size(63, 17);
            this.magicAmmys.TabIndex = 23;
            this.magicAmmys.Text = "Amulets";
            this.magicAmmys.UseVisualStyleBackColor = true;
            // 
            // rareWeapons
            // 
            this.rareWeapons.AutoSize = true;
            this.rareWeapons.Checked = true;
            this.rareWeapons.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rareWeapons.Location = new System.Drawing.Point(111, 235);
            this.rareWeapons.Name = "rareWeapons";
            this.rareWeapons.Size = new System.Drawing.Size(72, 17);
            this.rareWeapons.TabIndex = 22;
            this.rareWeapons.Text = "Weapons";
            this.rareWeapons.UseVisualStyleBackColor = true;
            // 
            // rareShields
            // 
            this.rareShields.AutoSize = true;
            this.rareShields.Checked = true;
            this.rareShields.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rareShields.Location = new System.Drawing.Point(111, 212);
            this.rareShields.Name = "rareShields";
            this.rareShields.Size = new System.Drawing.Size(60, 17);
            this.rareShields.TabIndex = 21;
            this.rareShields.Text = "Shields";
            this.rareShields.UseVisualStyleBackColor = true;
            // 
            // rareRings
            // 
            this.rareRings.AutoSize = true;
            this.rareRings.Checked = true;
            this.rareRings.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rareRings.Location = new System.Drawing.Point(111, 189);
            this.rareRings.Name = "rareRings";
            this.rareRings.Size = new System.Drawing.Size(53, 17);
            this.rareRings.TabIndex = 20;
            this.rareRings.Text = "Rings";
            this.rareRings.UseVisualStyleBackColor = true;
            // 
            // rareHelms
            // 
            this.rareHelms.AutoSize = true;
            this.rareHelms.Checked = true;
            this.rareHelms.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rareHelms.Location = new System.Drawing.Point(111, 166);
            this.rareHelms.Name = "rareHelms";
            this.rareHelms.Size = new System.Drawing.Size(55, 17);
            this.rareHelms.TabIndex = 19;
            this.rareHelms.Text = "Helms";
            this.rareHelms.UseVisualStyleBackColor = true;
            // 
            // rareGloves
            // 
            this.rareGloves.AutoSize = true;
            this.rareGloves.Checked = true;
            this.rareGloves.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rareGloves.Location = new System.Drawing.Point(111, 143);
            this.rareGloves.Name = "rareGloves";
            this.rareGloves.Size = new System.Drawing.Size(59, 17);
            this.rareGloves.TabIndex = 18;
            this.rareGloves.Text = "Gloves";
            this.rareGloves.UseVisualStyleBackColor = true;
            // 
            // rareBoots
            // 
            this.rareBoots.AutoSize = true;
            this.rareBoots.Checked = true;
            this.rareBoots.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rareBoots.Location = new System.Drawing.Point(111, 120);
            this.rareBoots.Name = "rareBoots";
            this.rareBoots.Size = new System.Drawing.Size(53, 17);
            this.rareBoots.TabIndex = 17;
            this.rareBoots.Text = "Boots";
            this.rareBoots.UseVisualStyleBackColor = true;
            // 
            // rareBelts
            // 
            this.rareBelts.AutoSize = true;
            this.rareBelts.Checked = true;
            this.rareBelts.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rareBelts.Location = new System.Drawing.Point(111, 97);
            this.rareBelts.Name = "rareBelts";
            this.rareBelts.Size = new System.Drawing.Size(49, 17);
            this.rareBelts.TabIndex = 16;
            this.rareBelts.Text = "Belts";
            this.rareBelts.UseVisualStyleBackColor = true;
            // 
            // rareArmors
            // 
            this.rareArmors.AutoSize = true;
            this.rareArmors.Checked = true;
            this.rareArmors.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rareArmors.Location = new System.Drawing.Point(111, 74);
            this.rareArmors.Name = "rareArmors";
            this.rareArmors.Size = new System.Drawing.Size(58, 17);
            this.rareArmors.TabIndex = 15;
            this.rareArmors.Text = "Armors";
            this.rareArmors.UseVisualStyleBackColor = true;
            // 
            // rareAmmys
            // 
            this.rareAmmys.AutoSize = true;
            this.rareAmmys.Checked = true;
            this.rareAmmys.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rareAmmys.Location = new System.Drawing.Point(111, 51);
            this.rareAmmys.Name = "rareAmmys";
            this.rareAmmys.Size = new System.Drawing.Size(63, 17);
            this.rareAmmys.TabIndex = 14;
            this.rareAmmys.Text = "Amulets";
            this.rareAmmys.UseVisualStyleBackColor = true;
            // 
            // uniqueWeapons
            // 
            this.uniqueWeapons.AutoSize = true;
            this.uniqueWeapons.Checked = true;
            this.uniqueWeapons.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uniqueWeapons.Location = new System.Drawing.Point(9, 235);
            this.uniqueWeapons.Name = "uniqueWeapons";
            this.uniqueWeapons.Size = new System.Drawing.Size(72, 17);
            this.uniqueWeapons.TabIndex = 13;
            this.uniqueWeapons.Text = "Weapons";
            this.uniqueWeapons.UseVisualStyleBackColor = true;
            // 
            // uniqueShields
            // 
            this.uniqueShields.AutoSize = true;
            this.uniqueShields.Location = new System.Drawing.Point(9, 212);
            this.uniqueShields.Name = "uniqueShields";
            this.uniqueShields.Size = new System.Drawing.Size(60, 17);
            this.uniqueShields.TabIndex = 12;
            this.uniqueShields.Text = "Shields";
            this.uniqueShields.UseVisualStyleBackColor = true;
            // 
            // uniqueRings
            // 
            this.uniqueRings.AutoSize = true;
            this.uniqueRings.Checked = true;
            this.uniqueRings.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uniqueRings.Location = new System.Drawing.Point(9, 189);
            this.uniqueRings.Name = "uniqueRings";
            this.uniqueRings.Size = new System.Drawing.Size(53, 17);
            this.uniqueRings.TabIndex = 11;
            this.uniqueRings.Text = "Rings";
            this.uniqueRings.UseVisualStyleBackColor = true;
            // 
            // uniqueHelms
            // 
            this.uniqueHelms.AutoSize = true;
            this.uniqueHelms.Checked = true;
            this.uniqueHelms.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uniqueHelms.Location = new System.Drawing.Point(9, 166);
            this.uniqueHelms.Name = "uniqueHelms";
            this.uniqueHelms.Size = new System.Drawing.Size(55, 17);
            this.uniqueHelms.TabIndex = 10;
            this.uniqueHelms.Text = "Helms";
            this.uniqueHelms.UseVisualStyleBackColor = true;
            // 
            // uniqueGloves
            // 
            this.uniqueGloves.AutoSize = true;
            this.uniqueGloves.Checked = true;
            this.uniqueGloves.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uniqueGloves.Location = new System.Drawing.Point(9, 143);
            this.uniqueGloves.Name = "uniqueGloves";
            this.uniqueGloves.Size = new System.Drawing.Size(59, 17);
            this.uniqueGloves.TabIndex = 9;
            this.uniqueGloves.Text = "Gloves";
            this.uniqueGloves.UseVisualStyleBackColor = true;
            // 
            // uniqueBoots
            // 
            this.uniqueBoots.AutoSize = true;
            this.uniqueBoots.Location = new System.Drawing.Point(9, 120);
            this.uniqueBoots.Name = "uniqueBoots";
            this.uniqueBoots.Size = new System.Drawing.Size(53, 17);
            this.uniqueBoots.TabIndex = 8;
            this.uniqueBoots.Text = "Boots";
            this.uniqueBoots.UseVisualStyleBackColor = true;
            // 
            // uniqueBelts
            // 
            this.uniqueBelts.AutoSize = true;
            this.uniqueBelts.Checked = true;
            this.uniqueBelts.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uniqueBelts.Location = new System.Drawing.Point(9, 97);
            this.uniqueBelts.Name = "uniqueBelts";
            this.uniqueBelts.Size = new System.Drawing.Size(49, 17);
            this.uniqueBelts.TabIndex = 7;
            this.uniqueBelts.Text = "Belts";
            this.uniqueBelts.UseVisualStyleBackColor = true;
            // 
            // uniqueArmors
            // 
            this.uniqueArmors.AutoSize = true;
            this.uniqueArmors.Checked = true;
            this.uniqueArmors.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uniqueArmors.Location = new System.Drawing.Point(9, 74);
            this.uniqueArmors.Name = "uniqueArmors";
            this.uniqueArmors.Size = new System.Drawing.Size(58, 17);
            this.uniqueArmors.TabIndex = 6;
            this.uniqueArmors.Text = "Armors";
            this.uniqueArmors.UseVisualStyleBackColor = true;
            // 
            // uniqueAmmys
            // 
            this.uniqueAmmys.AutoSize = true;
            this.uniqueAmmys.Location = new System.Drawing.Point(9, 51);
            this.uniqueAmmys.Name = "uniqueAmmys";
            this.uniqueAmmys.Size = new System.Drawing.Size(63, 17);
            this.uniqueAmmys.TabIndex = 5;
            this.uniqueAmmys.Text = "Amulets";
            this.uniqueAmmys.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(426, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "White";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(331, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Set";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(223, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Magic";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(123, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Rare";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Unique";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(450, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OK);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(450, 41);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.whiteButton);
            this.groupBox3.Controls.Add(this.setButton);
            this.groupBox3.Controls.Add(this.magicButton);
            this.groupBox3.Controls.Add(this.rareButton);
            this.groupBox3.Controls.Add(this.uniqueButton);
            this.groupBox3.Location = new System.Drawing.Point(12, 355);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(513, 60);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Specific settings";
            // 
            // uniqueButton
            // 
            this.uniqueButton.Location = new System.Drawing.Point(9, 19);
            this.uniqueButton.Name = "uniqueButton";
            this.uniqueButton.Size = new System.Drawing.Size(75, 23);
            this.uniqueButton.TabIndex = 0;
            this.uniqueButton.Text = "Uniques";
            this.uniqueButton.UseVisualStyleBackColor = true;
            this.uniqueButton.Click += new System.EventHandler(this.uniqueClick);
            // 
            // rareButton
            // 
            this.rareButton.Location = new System.Drawing.Point(108, 19);
            this.rareButton.Name = "rareButton";
            this.rareButton.Size = new System.Drawing.Size(75, 23);
            this.rareButton.TabIndex = 1;
            this.rareButton.Text = "Rares";
            this.rareButton.UseVisualStyleBackColor = true;
            // 
            // magicButton
            // 
            this.magicButton.Location = new System.Drawing.Point(212, 19);
            this.magicButton.Name = "magicButton";
            this.magicButton.Size = new System.Drawing.Size(75, 23);
            this.magicButton.TabIndex = 2;
            this.magicButton.Text = "Magic";
            this.magicButton.UseVisualStyleBackColor = true;
            // 
            // setButton
            // 
            this.setButton.Location = new System.Drawing.Point(320, 19);
            this.setButton.Name = "setButton";
            this.setButton.Size = new System.Drawing.Size(75, 23);
            this.setButton.TabIndex = 3;
            this.setButton.Text = "Set Items";
            this.setButton.UseVisualStyleBackColor = true;
            // 
            // whiteButton
            // 
            this.whiteButton.Location = new System.Drawing.Point(429, 19);
            this.whiteButton.Name = "whiteButton";
            this.whiteButton.Size = new System.Drawing.Size(75, 23);
            this.whiteButton.TabIndex = 4;
            this.whiteButton.Text = "Whites";
            this.whiteButton.UseVisualStyleBackColor = true;
            // 
            // DlgClassicModerate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(537, 427);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(545, 461);
            this.MinimumSize = new System.Drawing.Size(545, 461);
            this.Name = "DlgClassicModerate";
            this.Text = "Classic Game - Moderate PickIt Editor";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox goldPileTextBoxe;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox uniqueWeapons;
        private System.Windows.Forms.CheckBox uniqueShields;
        private System.Windows.Forms.CheckBox uniqueRings;
        private System.Windows.Forms.CheckBox uniqueHelms;
        private System.Windows.Forms.CheckBox uniqueGloves;
        private System.Windows.Forms.CheckBox uniqueBoots;
        private System.Windows.Forms.CheckBox uniqueBelts;
        private System.Windows.Forms.CheckBox uniqueArmors;
        private System.Windows.Forms.CheckBox uniqueAmmys;
        private System.Windows.Forms.CheckBox setItemscheck;
        private System.Windows.Forms.CheckBox magicWeapons;
        private System.Windows.Forms.CheckBox magicShields;
        private System.Windows.Forms.CheckBox magicRings;
        private System.Windows.Forms.CheckBox magicHelms;
        private System.Windows.Forms.CheckBox magicArmors;
        private System.Windows.Forms.CheckBox magicAmmys;
        private System.Windows.Forms.CheckBox rareWeapons;
        private System.Windows.Forms.CheckBox rareShields;
        private System.Windows.Forms.CheckBox rareRings;
        private System.Windows.Forms.CheckBox rareHelms;
        private System.Windows.Forms.CheckBox rareGloves;
        private System.Windows.Forms.CheckBox rareBoots;
        private System.Windows.Forms.CheckBox rareBelts;
        private System.Windows.Forms.CheckBox rareArmors;
        private System.Windows.Forms.CheckBox rareAmmys;
        private System.Windows.Forms.CheckBox whiteWeapons;
        private System.Windows.Forms.CheckBox whiteShields;
        private System.Windows.Forms.CheckBox whiteGems;
        private System.Windows.Forms.CheckBox whiteHelms;
        private System.Windows.Forms.CheckBox whiteArmors;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button whiteButton;
        private System.Windows.Forms.Button setButton;
        private System.Windows.Forms.Button magicButton;
        private System.Windows.Forms.Button rareButton;
        private System.Windows.Forms.Button uniqueButton;
    }
}