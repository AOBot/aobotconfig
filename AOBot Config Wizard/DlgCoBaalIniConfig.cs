﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AOBot_Config_Wizard
{
    public partial class DlgCoBaalIniConfig : Form
    {
        public DlgCoBaalIniConfig()
        {
            InitializeComponent();
            DeserializeCoBaalIni();
        }
        private void ShopClick(object sender, EventArgs e)
        {
            try
            {
                CoBaalIniFile.Items.Insert(CoBaalIniFile.SelectedIndex, "Shop, 60");
            }
            catch (Exception)
            {
                CoBaalIniFile.Items.Add("Shop, 60");
            }
        }

        private void Delete(object sender, EventArgs e)
        {
            try
            {
                Object selection = CoBaalIniFile.SelectedItem;
                int selectionIndex = CoBaalIniFile.SelectedIndex;
                CoBaalIniFile.Items.Remove(selection);
                CoBaalIniFile.SetSelected(selectionIndex, true);
            }
            catch (ArgumentOutOfRangeException)
            { }
        }

        private void MoveUp(object sender, EventArgs e)
        {
            try
            {

                int selection = CoBaalIniFile.SelectedIndex;
                CoBaalIniFile.Items.Insert(selection - 1, CoBaalIniFile.SelectedItem);
                CoBaalIniFile.Items.RemoveAt(selection + 1);
                CoBaalIniFile.SetSelected(selection - 1, true);

            }
            catch (ArgumentOutOfRangeException)
            { }

        }

        private void MoveDown(object sender, EventArgs e)
        {
            try
            {
                int selection = CoBaalIniFile.SelectedIndex;
                Object selectedItem = CoBaalIniFile.SelectedItem;
                if (selectedItem != null)
                {

                    CoBaalIniFile.Items.Insert(selection + 2, CoBaalIniFile.SelectedItem);
                    CoBaalIniFile.Items.RemoveAt(selection);
                    CoBaalIniFile.SetSelected(selection + 1, true);
                }

            }
            catch (ArgumentOutOfRangeException)
            { }
        }

        private void Sell(object sender, EventArgs e)
        {
            try
            {
                CoBaalIniFile.Items.Insert(CoBaalIniFile.SelectedIndex, "Sell, 60");
            }
            catch (Exception)
            {
                CoBaalIniFile.Items.Add("Sell, 60");
            }
        }

        private void Heal(object sender, EventArgs e)
        {
            try
            {
                CoBaalIniFile.Items.Insert(CoBaalIniFile.SelectedIndex, "Heal, 60");
            }
            catch (Exception)
            {
                CoBaalIniFile.Items.Add("Heal, 60");
            }
        }

        private void Resurrect(object sender, EventArgs e)
        {
            try
            {
                CoBaalIniFile.Items.Insert(CoBaalIniFile.SelectedIndex, "Resurrect, 60");
            }
            catch (Exception)
            {
                CoBaalIniFile.Items.Add("Resurrect, 60");
            }
        }

        private void Repair(object sender, EventArgs e)
        {
            try
            {
                CoBaalIniFile.Items.Insert(CoBaalIniFile.SelectedIndex, "Repair, 60");
            }
            catch (Exception)
            {
                CoBaalIniFile.Items.Add("Repair, 60");
            }
        }

        private void Stash(object sender, EventArgs e)
        {
            try
            {
                CoBaalIniFile.Items.Insert(CoBaalIniFile.SelectedIndex, "Stash, 60");
            }
            catch (Exception)
            {
                CoBaalIniFile.Items.Add("Stash, 60");
            }
        }

        private void Gamble(object sender, EventArgs e)
        {
            try
            {
                CoBaalIniFile.Items.Insert(CoBaalIniFile.SelectedIndex, "Gamble, 180");
            }
            catch (Exception)
            {
                CoBaalIniFile.Items.Add("Gamble, 180");
            }
        }

        private void CoBaal(object sender, EventArgs e)
        {
            try
            {
                CoBaalIniFile.Items.Insert(CoBaalIniFile.SelectedIndex, "CoBaal, 600");
            }
            catch (Exception)
            {
                CoBaalIniFile.Items.Add("CoBaal, 600");
            }
        }

        private void ActChange(object sender, EventArgs e)
        {
            DlgActChange act = new DlgActChange();
            if (act.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    CoBaalIniFile.Items.Insert(CoBaalIniFile.SelectedIndex, DataTypes.globalActChange);
                }
                catch (Exception)
                {
                    CoBaalIniFile.Items.Add(DataTypes.globalActChange);
                }
            }
        }

        private void Sleep(object sender, EventArgs e)
        {
            DlgSleep sleep = new DlgSleep();
            if (sleep.ShowDialog() == DialogResult.OK)
                try
                {
                    CoBaalIniFile.Items.Insert(CoBaalIniFile.SelectedIndex, "Sleep, " + DataTypes.globalSleepMS);
                }
                catch (Exception)
                {
                    CoBaalIniFile.Items.Add("Sleep, " + DataTypes.globalSleepMS);
                }
        }

        private void acceptCoBaal(object sender, EventArgs e)
        {
            StreamWriter stw;
            stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\CoBaal.ini");
            for (int i = 1; i <= CoBaalIniFile.Items.Count; ++i)
            {
                CoBaalIniFile.SetSelected(i - 1, true);
                stw.WriteLine(CoBaalIniFile.SelectedItem);
            }
            stw.Close();
            this.Close();
        }
        private void DeserializeCoBaalIni()
        {
            StreamReader str;
            str = File.OpenText(DataTypes.mainConfig.AOPath + "config\\ini\\CoBaal.ini");
            while (!str.EndOfStream)
                CoBaalIniFile.Items.Add(str.ReadLine());
        }
    }
}
