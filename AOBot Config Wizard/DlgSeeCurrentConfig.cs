﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AOBot_Config_Wizard
{
    public partial class DlgSeeCurrentConfig : Form
    {
        public DlgSeeCurrentConfig()
        {
            InitializeComponent();
            textBox1gamePath.Text = DataTypes.mainConfig.GamePath;
            textBox1.Text = DataTypes.mainConfig.GameExe;
            if (DataTypes.mainConfig.BotFile == "Bot0.ini")
                radioButton1.Checked = true;
            else
                if (DataTypes.mainConfig.BotFile == "Bot1.ini")
                    radioButton2.Checked = true;
                else
                    if (DataTypes.mainConfig.BotFile == "Bot2.ini")
                        radioButton3.Checked = true;
                    else
                        if (DataTypes.mainConfig.BotFile == "Bot3.ini")
                            radioButton4.Checked = true;
            textBox2.Text = DataTypes.mainConfig.OwnerName;
            textBox3.Text = DataTypes.mainConfig.AOPath;
        }      
    }
}
