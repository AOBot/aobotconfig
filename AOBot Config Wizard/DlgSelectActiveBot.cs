﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace AOBot_Config_Wizard
{
    public partial class DlgSelectActiveBot : Form
    {
        public DlgSelectActiveBot()
        {
            InitializeComponent();
            ActivateActiveBots();
        }
        private void ActivateActiveBots()
        {
            if ((!DataTypes.botIni0.ConfigDone) && (!DataTypes.botIni1.ConfigDone) && (!DataTypes.botIni2.ConfigDone) && (!DataTypes.botIni3.ConfigDone))
            {
                MessageBox.Show("You must configure at least one BotFile first", "No bot file configured");
                this.Close();
            }
            else
            {
                radioButton1.Enabled = DataTypes.botIni0.ConfigDone;
                radioButton2.Enabled = DataTypes.botIni1.ConfigDone;
                radioButton3.Enabled = DataTypes.botIni2.ConfigDone;
                radioButton4.Enabled = DataTypes.botIni3.ConfigDone;
            }
        }
        private string replaceText = "";
        private string textToSearch = "";
        private void SaveActiveBotRunInfo(object sender, EventArgs e)
        {
            StreamReader reader = new StreamReader(DataTypes.mainConfig.AOPath + "config\\ini\\Awesom-O.ini");
            string content = reader.ReadToEnd();
            reader.Close();
            if (radioButton1.Checked)
                replaceText = "ActiveBots=0";
            else
                if (radioButton2.Checked)
                    replaceText = "ActiveBots=1";
                else
                    if (radioButton3.Checked)
                        replaceText = "ActiveBots=2";
                    else
                        if (radioButton4.Checked)
                            replaceText = "ActiveBots=3";
            if (DataTypes.mainConfig.BotFile == "Bot0.ini")
                textToSearch = "ActiveBots=0";
            else
                if (DataTypes.mainConfig.BotFile == "Bot1.ini")
                    textToSearch = "ActiveBots=1";
                else
                    if (DataTypes.mainConfig.BotFile == "Bot2.ini")
                        textToSearch = "ActiveBots=2";
                    else
                        if (DataTypes.mainConfig.BotFile == "Bot3.ini")
                            textToSearch = "ActiveBots=3";
            content = Regex.Replace(content, textToSearch, replaceText);

            StreamWriter writer = new StreamWriter(DataTypes.mainConfig.AOPath + "config\\ini\\Awesom-O.ini");
            writer.Write(content);
            writer.Close();

            if (radioButton1.Checked)
                DataTypes.mainConfig.BotFile = "Bot0.ini";
            else
                if (radioButton2.Checked)
                    DataTypes.mainConfig.BotFile = "Bot1.ini";
                else
                    if (radioButton3.Checked)
                        DataTypes.mainConfig.BotFile = "Bot2.ini";
                    else
                        if (radioButton4.Checked)
                            DataTypes.mainConfig.BotFile = "Bot3.ini";
            Close();
           
        }
    }
}
