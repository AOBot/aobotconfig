﻿namespace AOBot_Config_Wizard
{
    partial class DlgBot0Ini
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgBot0Ini));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxCharName = new System.Windows.Forms.TextBox();
            this.textBoxAccountPassword = new System.Windows.Forms.MaskedTextBox();
            this.textBoxAccountName = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonLeechBaal = new System.Windows.Forms.RadioButton();
            this.radioButtonChaos = new System.Windows.Forms.RadioButton();
            this.radioButtonKey = new System.Windows.Forms.RadioButton();
            this.radioButtonMF = new System.Windows.Forms.RadioButton();
            this.radioButtonCoBaal = new System.Windows.Forms.RadioButton();
            this.radioButtonBaal = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButtonStrict = new System.Windows.Forms.RadioButton();
            this.radioButtonModerate = new System.Windows.Forms.RadioButton();
            this.radioButtonGreedy = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.radioButtonPublicChatOff = new System.Windows.Forms.RadioButton();
            this.radioButtonPublicChatOn = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxGamePassword = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textBoxGameName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBoxGambleTo = new System.Windows.Forms.MaskedTextBox();
            this.textBoxGambleFrom = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.radioButtonNormalDifficulty = new System.Windows.Forms.RadioButton();
            this.radioButtonNightMareDifficulty = new System.Windows.Forms.RadioButton();
            this.radioButtonHellDifficulty = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.textBoxInventoryRow1 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxInventoryRow4 = new System.Windows.Forms.TextBox();
            this.textBoxInventoryRow3 = new System.Windows.Forms.TextBox();
            this.textBoxInventoryRow2 = new System.Windows.Forms.TextBox();
            this.buttonOk = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.allChatOff = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Account name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxCharName);
            this.groupBox1.Controls.Add(this.textBoxAccountPassword);
            this.groupBox1.Controls.Add(this.textBoxAccountName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(258, 123);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Account Details";
            // 
            // textBoxCharName
            // 
            this.textBoxCharName.Location = new System.Drawing.Point(94, 77);
            this.textBoxCharName.Name = "textBoxCharName";
            this.textBoxCharName.Size = new System.Drawing.Size(150, 20);
            this.textBoxCharName.TabIndex = 8;
            this.textBoxCharName.Text = "(Must be an Hammerdin)";
            this.textBoxCharName.Click += new System.EventHandler(this.ClearTextBox);
            this.textBoxCharName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoSpacesValidation);
            // 
            // textBoxAccountPassword
            // 
            this.textBoxAccountPassword.AllowPromptAsInput = false;
            this.textBoxAccountPassword.Location = new System.Drawing.Point(94, 51);
            this.textBoxAccountPassword.Name = "textBoxAccountPassword";
            this.textBoxAccountPassword.PromptChar = ' ';
            this.textBoxAccountPassword.Size = new System.Drawing.Size(150, 20);
            this.textBoxAccountPassword.TabIndex = 7;
            this.textBoxAccountPassword.UseSystemPasswordChar = true;
            this.textBoxAccountPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoSpacesValidation);
            // 
            // textBoxAccountName
            // 
            this.textBoxAccountName.AllowPromptAsInput = false;
            this.textBoxAccountName.BeepOnError = true;
            this.textBoxAccountName.Location = new System.Drawing.Point(94, 25);
            this.textBoxAccountName.Name = "textBoxAccountName";
            this.textBoxAccountName.PromptChar = '-';
            this.textBoxAccountName.Size = new System.Drawing.Size(150, 20);
            this.textBoxAccountName.TabIndex = 6;
            this.textBoxAccountName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoSpacesValidation);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Char Name :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonLeechBaal);
            this.groupBox2.Controls.Add(this.radioButtonChaos);
            this.groupBox2.Controls.Add(this.radioButtonKey);
            this.groupBox2.Controls.Add(this.radioButtonMF);
            this.groupBox2.Controls.Add(this.radioButtonCoBaal);
            this.groupBox2.Controls.Add(this.radioButtonBaal);
            this.groupBox2.Location = new System.Drawing.Point(410, 115);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(104, 93);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Active Bot File";
            // 
            // radioButtonLeechBaal
            // 
            this.radioButtonLeechBaal.AutoSize = true;
            this.radioButtonLeechBaal.Location = new System.Drawing.Point(6, 53);
            this.radioButtonLeechBaal.Name = "radioButtonLeechBaal";
            this.radioButtonLeechBaal.Size = new System.Drawing.Size(79, 17);
            this.radioButtonLeechBaal.TabIndex = 5;
            this.radioButtonLeechBaal.Text = "Leech Baal";
            this.radioButtonLeechBaal.UseVisualStyleBackColor = true;
            this.radioButtonLeechBaal.Click += new System.EventHandler(this.LeechBaalInfoDialog);
            // 
            // radioButtonChaos
            // 
            this.radioButtonChaos.AutoSize = true;
            this.radioButtonChaos.Location = new System.Drawing.Point(6, 70);
            this.radioButtonChaos.Name = "radioButtonChaos";
            this.radioButtonChaos.Size = new System.Drawing.Size(55, 17);
            this.radioButtonChaos.TabIndex = 4;
            this.radioButtonChaos.Text = "Chaos";
            this.radioButtonChaos.UseVisualStyleBackColor = true;
            // 
            // radioButtonKey
            // 
            this.radioButtonKey.AutoSize = true;
            this.radioButtonKey.Location = new System.Drawing.Point(60, 36);
            this.radioButtonKey.Name = "radioButtonKey";
            this.radioButtonKey.Size = new System.Drawing.Size(43, 17);
            this.radioButtonKey.TabIndex = 3;
            this.radioButtonKey.Text = "Key";
            this.radioButtonKey.UseVisualStyleBackColor = true;
            // 
            // radioButtonMF
            // 
            this.radioButtonMF.AutoSize = true;
            this.radioButtonMF.Location = new System.Drawing.Point(60, 19);
            this.radioButtonMF.Name = "radioButtonMF";
            this.radioButtonMF.Size = new System.Drawing.Size(40, 17);
            this.radioButtonMF.TabIndex = 2;
            this.radioButtonMF.Text = "MF";
            this.radioButtonMF.UseVisualStyleBackColor = true;
            // 
            // radioButtonCoBaal
            // 
            this.radioButtonCoBaal.AutoSize = true;
            this.radioButtonCoBaal.Location = new System.Drawing.Point(6, 36);
            this.radioButtonCoBaal.Name = "radioButtonCoBaal";
            this.radioButtonCoBaal.Size = new System.Drawing.Size(58, 17);
            this.radioButtonCoBaal.TabIndex = 1;
            this.radioButtonCoBaal.Text = "Cobaal";
            this.radioButtonCoBaal.UseVisualStyleBackColor = true;
            // 
            // radioButtonBaal
            // 
            this.radioButtonBaal.AutoSize = true;
            this.radioButtonBaal.Checked = true;
            this.radioButtonBaal.Location = new System.Drawing.Point(6, 19);
            this.radioButtonBaal.Name = "radioButtonBaal";
            this.radioButtonBaal.Size = new System.Drawing.Size(46, 17);
            this.radioButtonBaal.TabIndex = 0;
            this.radioButtonBaal.TabStop = true;
            this.radioButtonBaal.Text = "Baal";
            this.radioButtonBaal.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButtonStrict);
            this.groupBox3.Controls.Add(this.radioButtonModerate);
            this.groupBox3.Controls.Add(this.radioButtonGreedy);
            this.groupBox3.Location = new System.Drawing.Point(410, 312);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(104, 107);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "PickIt Files";
            // 
            // radioButtonStrict
            // 
            this.radioButtonStrict.AutoSize = true;
            this.radioButtonStrict.Location = new System.Drawing.Point(18, 70);
            this.radioButtonStrict.Name = "radioButtonStrict";
            this.radioButtonStrict.Size = new System.Drawing.Size(49, 17);
            this.radioButtonStrict.TabIndex = 2;
            this.radioButtonStrict.Text = "Strict";
            this.radioButtonStrict.UseVisualStyleBackColor = true;
            // 
            // radioButtonModerate
            // 
            this.radioButtonModerate.AutoSize = true;
            this.radioButtonModerate.Checked = true;
            this.radioButtonModerate.Location = new System.Drawing.Point(18, 47);
            this.radioButtonModerate.Name = "radioButtonModerate";
            this.radioButtonModerate.Size = new System.Drawing.Size(70, 17);
            this.radioButtonModerate.TabIndex = 1;
            this.radioButtonModerate.TabStop = true;
            this.radioButtonModerate.Text = "Moderate";
            this.radioButtonModerate.UseVisualStyleBackColor = true;
            // 
            // radioButtonGreedy
            // 
            this.radioButtonGreedy.AutoSize = true;
            this.radioButtonGreedy.Location = new System.Drawing.Point(18, 24);
            this.radioButtonGreedy.Name = "radioButtonGreedy";
            this.radioButtonGreedy.Size = new System.Drawing.Size(59, 17);
            this.radioButtonGreedy.TabIndex = 0;
            this.radioButtonGreedy.Text = "Greedy";
            this.radioButtonGreedy.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.radioButtonPublicChatOff);
            this.groupBox4.Controls.Add(this.radioButtonPublicChatOn);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.textBoxGamePassword);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.checkBox1);
            this.groupBox4.Controls.Add(this.textBoxGameName);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(12, 312);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(392, 107);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Games Options";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(242, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(133, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "(More risky if you put it on.)";
            // 
            // radioButtonPublicChatOff
            // 
            this.radioButtonPublicChatOff.AutoSize = true;
            this.radioButtonPublicChatOff.Checked = true;
            this.radioButtonPublicChatOff.ForeColor = System.Drawing.Color.Red;
            this.radioButtonPublicChatOff.Location = new System.Drawing.Point(197, 77);
            this.radioButtonPublicChatOff.Name = "radioButtonPublicChatOff";
            this.radioButtonPublicChatOff.Size = new System.Drawing.Size(39, 17);
            this.radioButtonPublicChatOff.TabIndex = 8;
            this.radioButtonPublicChatOff.TabStop = true;
            this.radioButtonPublicChatOff.Text = "Off";
            this.radioButtonPublicChatOff.UseVisualStyleBackColor = true;
            // 
            // radioButtonPublicChatOn
            // 
            this.radioButtonPublicChatOn.AutoSize = true;
            this.radioButtonPublicChatOn.ForeColor = System.Drawing.Color.Green;
            this.radioButtonPublicChatOn.Location = new System.Drawing.Point(135, 77);
            this.radioButtonPublicChatOn.Name = "radioButtonPublicChatOn";
            this.radioButtonPublicChatOn.Size = new System.Drawing.Size(39, 17);
            this.radioButtonPublicChatOn.TabIndex = 7;
            this.radioButtonPublicChatOn.Text = "On";
            this.radioButtonPublicChatOn.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "InGame Public Chat :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(213, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "(Leave empty for public runs.)";
            // 
            // textBoxGamePassword
            // 
            this.textBoxGamePassword.Location = new System.Drawing.Point(107, 47);
            this.textBoxGamePassword.Name = "textBoxGamePassword";
            this.textBoxGamePassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxGamePassword.TabIndex = 4;
            this.textBoxGamePassword.Text = "a";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Game Password :";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(213, 23);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(132, 17);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "Make Random Names";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.EnableGameName);
            // 
            // textBoxGameName
            // 
            this.textBoxGameName.Enabled = false;
            this.textBoxGameName.Location = new System.Drawing.Point(107, 21);
            this.textBoxGameName.Name = "textBoxGameName";
            this.textBoxGameName.Size = new System.Drawing.Size(100, 20);
            this.textBoxGameName.TabIndex = 1;
            this.textBoxGameName.Text = "Run-%d";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Games Name :";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBoxGambleTo);
            this.groupBox5.Controls.Add(this.textBoxGambleFrom);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Location = new System.Drawing.Point(276, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(128, 123);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Gambling settings";
            // 
            // textBoxGambleTo
            // 
            this.textBoxGambleTo.AsciiOnly = true;
            this.textBoxGambleTo.Location = new System.Drawing.Point(48, 61);
            this.textBoxGambleTo.Mask = "9999999";
            this.textBoxGambleTo.Name = "textBoxGambleTo";
            this.textBoxGambleTo.PromptChar = ' ';
            this.textBoxGambleTo.Size = new System.Drawing.Size(70, 20);
            this.textBoxGambleTo.TabIndex = 7;
            this.textBoxGambleTo.Text = "1000000";
            // 
            // textBoxGambleFrom
            // 
            this.textBoxGambleFrom.AllowPromptAsInput = false;
            this.textBoxGambleFrom.AsciiOnly = true;
            this.textBoxGambleFrom.Location = new System.Drawing.Point(48, 35);
            this.textBoxGambleFrom.Mask = "9999999";
            this.textBoxGambleFrom.Name = "textBoxGambleFrom";
            this.textBoxGambleFrom.PromptChar = ' ';
            this.textBoxGambleFrom.Size = new System.Drawing.Size(70, 20);
            this.textBoxGambleFrom.TabIndex = 6;
            this.textBoxGambleFrom.Text = "2000000";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Gamble ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(92, 84);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "gold.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 64);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "To :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "From :";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.radioButtonNormalDifficulty);
            this.groupBox6.Controls.Add(this.radioButtonNightMareDifficulty);
            this.groupBox6.Controls.Add(this.radioButtonHellDifficulty);
            this.groupBox6.Location = new System.Drawing.Point(410, 209);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(104, 99);
            this.groupBox6.TabIndex = 9;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Difficulty";
            // 
            // radioButtonNormalDifficulty
            // 
            this.radioButtonNormalDifficulty.AutoSize = true;
            this.radioButtonNormalDifficulty.Location = new System.Drawing.Point(30, 65);
            this.radioButtonNormalDifficulty.Name = "radioButtonNormalDifficulty";
            this.radioButtonNormalDifficulty.Size = new System.Drawing.Size(58, 17);
            this.radioButtonNormalDifficulty.TabIndex = 2;
            this.radioButtonNormalDifficulty.Text = "Normal";
            this.radioButtonNormalDifficulty.UseVisualStyleBackColor = true;
            // 
            // radioButtonNightMareDifficulty
            // 
            this.radioButtonNightMareDifficulty.AutoSize = true;
            this.radioButtonNightMareDifficulty.Location = new System.Drawing.Point(30, 42);
            this.radioButtonNightMareDifficulty.Name = "radioButtonNightMareDifficulty";
            this.radioButtonNightMareDifficulty.Size = new System.Drawing.Size(73, 17);
            this.radioButtonNightMareDifficulty.TabIndex = 1;
            this.radioButtonNightMareDifficulty.Text = "Nightmare";
            this.radioButtonNightMareDifficulty.UseVisualStyleBackColor = true;
            // 
            // radioButtonHellDifficulty
            // 
            this.radioButtonHellDifficulty.AutoSize = true;
            this.radioButtonHellDifficulty.Checked = true;
            this.radioButtonHellDifficulty.Location = new System.Drawing.Point(30, 19);
            this.radioButtonHellDifficulty.Name = "radioButtonHellDifficulty";
            this.radioButtonHellDifficulty.Size = new System.Drawing.Size(43, 17);
            this.radioButtonHellDifficulty.TabIndex = 0;
            this.radioButtonHellDifficulty.TabStop = true;
            this.radioButtonHellDifficulty.Text = "Hell";
            this.radioButtonHellDifficulty.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.textBoxInventoryRow1);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.label18);
            this.groupBox7.Controls.Add(this.label17);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.label15);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.textBoxInventoryRow4);
            this.groupBox7.Controls.Add(this.textBoxInventoryRow3);
            this.groupBox7.Controls.Add(this.textBoxInventoryRow2);
            this.groupBox7.Location = new System.Drawing.Point(12, 143);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(392, 165);
            this.groupBox7.TabIndex = 10;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Inventory Locks";
            // 
            // textBoxInventoryRow1
            // 
            this.textBoxInventoryRow1.Location = new System.Drawing.Point(263, 25);
            this.textBoxInventoryRow1.MaxLength = 10;
            this.textBoxInventoryRow1.Name = "textBoxInventoryRow1";
            this.textBoxInventoryRow1.Size = new System.Drawing.Size(100, 20);
            this.textBoxInventoryRow1.TabIndex = 11;
            this.textBoxInventoryRow1.Text = "0000111111";
            this.textBoxInventoryRow1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericValidation);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 145);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(317, 13);
            this.label19.TabIndex = 10;
            this.label19.Text = "If not done correctly, bot could sell things you don\'t wanted to sell.";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 70);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(196, 52);
            this.label18.TabIndex = 9;
            this.label18.Text = "Make sure that each 0 is an empty\r\nsquare in your inventory and that \r\neach 1 is " +
                "a square you dont want \r\nthe bot to touch, stuff you want to keep.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(153, 26);
            this.label17.TabIndex = 8;
            this.label17.Text = "Each number represents \r\na 1X1 square in your inventory.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(213, 106);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 7;
            this.label16.Text = "Row 4 :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(213, 80);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Row 3 :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(213, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "Row 2 :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(213, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Row 1 :";
            // 
            // textBoxInventoryRow4
            // 
            this.textBoxInventoryRow4.Location = new System.Drawing.Point(263, 103);
            this.textBoxInventoryRow4.MaxLength = 10;
            this.textBoxInventoryRow4.Name = "textBoxInventoryRow4";
            this.textBoxInventoryRow4.Size = new System.Drawing.Size(100, 20);
            this.textBoxInventoryRow4.TabIndex = 3;
            this.textBoxInventoryRow4.Text = "0000111111";
            this.textBoxInventoryRow4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericValidation);
            // 
            // textBoxInventoryRow3
            // 
            this.textBoxInventoryRow3.Location = new System.Drawing.Point(263, 77);
            this.textBoxInventoryRow3.MaxLength = 10;
            this.textBoxInventoryRow3.Name = "textBoxInventoryRow3";
            this.textBoxInventoryRow3.Size = new System.Drawing.Size(100, 20);
            this.textBoxInventoryRow3.TabIndex = 2;
            this.textBoxInventoryRow3.Text = "0000111111";
            this.textBoxInventoryRow3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericValidation);
            // 
            // textBoxInventoryRow2
            // 
            this.textBoxInventoryRow2.Location = new System.Drawing.Point(263, 51);
            this.textBoxInventoryRow2.MaxLength = 10;
            this.textBoxInventoryRow2.Name = "textBoxInventoryRow2";
            this.textBoxInventoryRow2.Size = new System.Drawing.Size(100, 20);
            this.textBoxInventoryRow2.TabIndex = 1;
            this.textBoxInventoryRow2.Text = "0000111111";
            this.textBoxInventoryRow2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericValidation);
            // 
            // buttonOk
            // 
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Location = new System.Drawing.Point(449, 5);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 12;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.BotOIniOk);
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(449, 34);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // allChatOff
            // 
            this.allChatOff.Location = new System.Drawing.Point(418, 63);
            this.allChatOff.Name = "allChatOff";
            this.allChatOff.Size = new System.Drawing.Size(106, 23);
            this.allChatOff.TabIndex = 16;
            this.allChatOff.Text = "Set All Chat Off";
            this.allChatOff.UseVisualStyleBackColor = true;
            this.allChatOff.Click += new System.EventHandler(this.allChatOffButton);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(416, 92);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "Reset Delays";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.ResetDelays);
            // 
            // DlgBot0Ini
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(534, 431);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.allChatOff);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(542, 465);
            this.MinimumSize = new System.Drawing.Size(542, 465);
            this.Name = "DlgBot0Ini";
            this.Text = "Botx.ini Config";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButtonChaos;
        private System.Windows.Forms.RadioButton radioButtonKey;
        private System.Windows.Forms.RadioButton radioButtonMF;
        private System.Windows.Forms.RadioButton radioButtonCoBaal;
        private System.Windows.Forms.RadioButton radioButtonBaal;
        private System.Windows.Forms.RadioButton radioButtonLeechBaal;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButtonStrict;
        private System.Windows.Forms.RadioButton radioButtonModerate;
        private System.Windows.Forms.RadioButton radioButtonGreedy;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox textBoxGameName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton radioButtonPublicChatOff;
        private System.Windows.Forms.RadioButton radioButtonPublicChatOn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxGamePassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton radioButtonNormalDifficulty;
        private System.Windows.Forms.RadioButton radioButtonNightMareDifficulty;
        private System.Windows.Forms.RadioButton radioButtonHellDifficulty;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxInventoryRow4;
        private System.Windows.Forms.TextBox textBoxInventoryRow3;
        private System.Windows.Forms.TextBox textBoxInventoryRow2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.MaskedTextBox textBoxGambleFrom;
        private System.Windows.Forms.MaskedTextBox textBoxGambleTo;
        private System.Windows.Forms.TextBox textBoxInventoryRow1;
        private System.Windows.Forms.MaskedTextBox textBoxAccountPassword;
        private System.Windows.Forms.MaskedTextBox textBoxAccountName;
        private System.Windows.Forms.TextBox textBoxCharName;
        private System.Windows.Forms.Button allChatOff;
        private System.Windows.Forms.Button button2;
    }
}