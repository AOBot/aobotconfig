﻿namespace AOBot_Config_Wizard
{
    partial class DlgLeechBaalIniConfig
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgLeechBaalIniConfig));
            this.MoveDownButton = new System.Windows.Forms.Button();
            this.MoveUpButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.ButtonAnnuler = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ActButton = new System.Windows.Forms.Button();
            this.LeechBaalButton = new System.Windows.Forms.Button();
            this.GambleButton = new System.Windows.Forms.Button();
            this.StashButton = new System.Windows.Forms.Button();
            this.RepairButton = new System.Windows.Forms.Button();
            this.ResurrectButton = new System.Windows.Forms.Button();
            this.HealButton = new System.Windows.Forms.Button();
            this.SellButton = new System.Windows.Forms.Button();
            this.ShopButton = new System.Windows.Forms.Button();
            this.SleepButton = new System.Windows.Forms.Button();
            this.LeechBaalIniFile = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MoveDownButton
            // 
            this.MoveDownButton.Location = new System.Drawing.Point(336, 200);
            this.MoveDownButton.Name = "MoveDownButton";
            this.MoveDownButton.Size = new System.Drawing.Size(104, 23);
            this.MoveDownButton.TabIndex = 17;
            this.MoveDownButton.Text = "Move Down";
            this.MoveDownButton.UseVisualStyleBackColor = true;
            this.MoveDownButton.Click += new System.EventHandler(this.MoveDown);
            // 
            // MoveUpButton
            // 
            this.MoveUpButton.Location = new System.Drawing.Point(336, 171);
            this.MoveUpButton.Name = "MoveUpButton";
            this.MoveUpButton.Size = new System.Drawing.Size(104, 23);
            this.MoveUpButton.TabIndex = 16;
            this.MoveUpButton.Text = "Move Up";
            this.MoveUpButton.UseVisualStyleBackColor = true;
            this.MoveUpButton.Click += new System.EventHandler(this.MoveUp);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(336, 113);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(104, 23);
            this.DeleteButton.TabIndex = 15;
            this.DeleteButton.Text = "Delete Selected";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.Delete);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(414, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "You should have an act change at the beginning and each time your bot changes act" +
                ".";
            // 
            // ButtonAnnuler
            // 
            this.ButtonAnnuler.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonAnnuler.Location = new System.Drawing.Point(374, 433);
            this.ButtonAnnuler.Name = "ButtonAnnuler";
            this.ButtonAnnuler.Size = new System.Drawing.Size(75, 23);
            this.ButtonAnnuler.TabIndex = 13;
            this.ButtonAnnuler.Text = "Cancel";
            this.ButtonAnnuler.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(293, 433);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 12;
            this.button12.Text = "Accept";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.acceptLeechBaal);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(277, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "It is easy, click on a button to the left to add it to your run.";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(15, 97);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ActButton);
            this.splitContainer1.Panel1.Controls.Add(this.LeechBaalButton);
            this.splitContainer1.Panel1.Controls.Add(this.GambleButton);
            this.splitContainer1.Panel1.Controls.Add(this.StashButton);
            this.splitContainer1.Panel1.Controls.Add(this.RepairButton);
            this.splitContainer1.Panel1.Controls.Add(this.ResurrectButton);
            this.splitContainer1.Panel1.Controls.Add(this.HealButton);
            this.splitContainer1.Panel1.Controls.Add(this.SellButton);
            this.splitContainer1.Panel1.Controls.Add(this.ShopButton);
            this.splitContainer1.Panel1.Controls.Add(this.SleepButton);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.LeechBaalIniFile);
            this.splitContainer1.Size = new System.Drawing.Size(296, 320);
            this.splitContainer1.SplitterDistance = 98;
            this.splitContainer1.TabIndex = 10;
            // 
            // ActButton
            // 
            this.ActButton.Location = new System.Drawing.Point(3, 15);
            this.ActButton.Name = "ActButton";
            this.ActButton.Size = new System.Drawing.Size(75, 23);
            this.ActButton.TabIndex = 9;
            this.ActButton.Text = "Act Change";
            this.ActButton.UseVisualStyleBackColor = true;
            this.ActButton.Click += new System.EventHandler(this.ActChange);
            // 
            // LeechBaalButton
            // 
            this.LeechBaalButton.Location = new System.Drawing.Point(3, 276);
            this.LeechBaalButton.Name = "LeechBaalButton";
            this.LeechBaalButton.Size = new System.Drawing.Size(75, 23);
            this.LeechBaalButton.TabIndex = 8;
            this.LeechBaalButton.Text = "LeechBaal";
            this.LeechBaalButton.UseVisualStyleBackColor = true;
            this.LeechBaalButton.Click += new System.EventHandler(this.LeechBaal);
            // 
            // GambleButton
            // 
            this.GambleButton.Location = new System.Drawing.Point(3, 247);
            this.GambleButton.Name = "GambleButton";
            this.GambleButton.Size = new System.Drawing.Size(75, 23);
            this.GambleButton.TabIndex = 7;
            this.GambleButton.Text = "Gamble";
            this.GambleButton.UseVisualStyleBackColor = true;
            this.GambleButton.Click += new System.EventHandler(this.Gamble);
            // 
            // StashButton
            // 
            this.StashButton.Location = new System.Drawing.Point(3, 218);
            this.StashButton.Name = "StashButton";
            this.StashButton.Size = new System.Drawing.Size(75, 23);
            this.StashButton.TabIndex = 6;
            this.StashButton.Text = "Stash";
            this.StashButton.UseVisualStyleBackColor = true;
            this.StashButton.Click += new System.EventHandler(this.Stash);
            // 
            // RepairButton
            // 
            this.RepairButton.Location = new System.Drawing.Point(3, 189);
            this.RepairButton.Name = "RepairButton";
            this.RepairButton.Size = new System.Drawing.Size(75, 23);
            this.RepairButton.TabIndex = 5;
            this.RepairButton.Text = "Repair";
            this.RepairButton.UseVisualStyleBackColor = true;
            this.RepairButton.Click += new System.EventHandler(this.Repair);
            // 
            // ResurrectButton
            // 
            this.ResurrectButton.Location = new System.Drawing.Point(3, 160);
            this.ResurrectButton.Name = "ResurrectButton";
            this.ResurrectButton.Size = new System.Drawing.Size(75, 23);
            this.ResurrectButton.TabIndex = 4;
            this.ResurrectButton.Text = "Resurrect";
            this.ResurrectButton.UseVisualStyleBackColor = true;
            this.ResurrectButton.Click += new System.EventHandler(this.Resurrect);
            // 
            // HealButton
            // 
            this.HealButton.Location = new System.Drawing.Point(3, 131);
            this.HealButton.Name = "HealButton";
            this.HealButton.Size = new System.Drawing.Size(75, 23);
            this.HealButton.TabIndex = 3;
            this.HealButton.Text = "Heal";
            this.HealButton.UseVisualStyleBackColor = true;
            this.HealButton.Click += new System.EventHandler(this.Heal);
            // 
            // SellButton
            // 
            this.SellButton.Location = new System.Drawing.Point(3, 102);
            this.SellButton.Name = "SellButton";
            this.SellButton.Size = new System.Drawing.Size(75, 23);
            this.SellButton.TabIndex = 2;
            this.SellButton.Text = "Sell";
            this.SellButton.UseVisualStyleBackColor = true;
            this.SellButton.Click += new System.EventHandler(this.Sell);
            // 
            // ShopButton
            // 
            this.ShopButton.Location = new System.Drawing.Point(3, 73);
            this.ShopButton.Name = "ShopButton";
            this.ShopButton.Size = new System.Drawing.Size(75, 23);
            this.ShopButton.TabIndex = 1;
            this.ShopButton.Text = "Shop";
            this.ShopButton.UseVisualStyleBackColor = true;
            this.ShopButton.Click += new System.EventHandler(this.ShopClick);
            // 
            // SleepButton
            // 
            this.SleepButton.Location = new System.Drawing.Point(3, 44);
            this.SleepButton.Name = "SleepButton";
            this.SleepButton.Size = new System.Drawing.Size(75, 23);
            this.SleepButton.TabIndex = 0;
            this.SleepButton.Text = "Sleep xx Ms";
            this.SleepButton.UseVisualStyleBackColor = true;
            this.SleepButton.Click += new System.EventHandler(this.Sleep);
            // 
            // LeechBaalIniFile
            // 
            this.LeechBaalIniFile.FormattingEnabled = true;
            this.LeechBaalIniFile.Location = new System.Drawing.Point(3, 3);
            this.LeechBaalIniFile.Name = "LeechBaalIniFile";
            this.LeechBaalIniFile.Size = new System.Drawing.Size(188, 316);
            this.LeechBaalIniFile.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(285, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Here you can customize quite easily your LeechBaal.ini file.";
            // 
            // DlgLeechBaalIniConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ButtonAnnuler;
            this.ClientSize = new System.Drawing.Size(461, 466);
            this.Controls.Add(this.MoveDownButton);
            this.Controls.Add(this.MoveUpButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ButtonAnnuler);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(477, 502);
            this.MinimumSize = new System.Drawing.Size(477, 502);
            this.Name = "DlgLeechBaalIniConfig";
            this.Text = "LeechBaal.ini Config";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button MoveDownButton;
        private System.Windows.Forms.Button MoveUpButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ButtonAnnuler;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button ActButton;
        private System.Windows.Forms.Button LeechBaalButton;
        private System.Windows.Forms.Button GambleButton;
        private System.Windows.Forms.Button StashButton;
        private System.Windows.Forms.Button RepairButton;
        private System.Windows.Forms.Button ResurrectButton;
        private System.Windows.Forms.Button HealButton;
        private System.Windows.Forms.Button SellButton;
        private System.Windows.Forms.Button ShopButton;
        private System.Windows.Forms.Button SleepButton;
        private System.Windows.Forms.ListBox LeechBaalIniFile;
        private System.Windows.Forms.Label label1;
    }
}