﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace AOBot_Config_Wizard
{
    public partial class DlgSelectActiveRun : Form
    {
        public DlgSelectActiveRun()
        {
            InitializeComponent();
        }

        private void SelectActiveRunOk(object sender, EventArgs e)
        {
            string activeBotFile = DataTypes.mainConfig.BotFile;
            StreamReader reader = new StreamReader(DataTypes.mainConfig.AOPath + "config\\ini\\" + activeBotFile);
            string content = reader.ReadToEnd();
            reader.Close();
            string searchText= "";
            
            if (activeBotFile == "Bot0.ini")
                searchText = DataTypes.botIni0.ActiveRun;
            else
                if (activeBotFile == "Bot1.ini")
                    searchText = DataTypes.botIni1.ActiveRun;
                else
                    if (activeBotFile == "Bot2.ini")
                        searchText = DataTypes.botIni2.ActiveRun;
                    else
                        if (activeBotFile == "Bot3.ini")
                            searchText = DataTypes.botIni3.ActiveRun;
            
            string replaceText = "";

            if (radioButtonBaal.Checked)
                replaceText = "Baal.ini";
            else
                if (radioButtonChaos.Checked)
                    replaceText = "Chaos.ini";
                else
                    if (radioButtonCoBaal.Checked)
                        replaceText = "CoBaal.ini";
                    else
                        if (radioButtonKey.Checked)
                            replaceText = "Key.ini";
                        else
                            if (radioButtonLeechBaal.Checked)
                                replaceText = "LeechBaal.ini";
                            else
                                if (radioButtonMF.Checked)
                                    replaceText = "MF.ini";

            content = Regex.Replace( content, searchText, replaceText );

            StreamWriter writer = new StreamWriter(DataTypes.mainConfig.AOPath + "config\\ini\\" + activeBotFile);
            writer.Write(content);
            writer.Close();
            if (activeBotFile == "Bot0.ini")
                DataTypes.botIni0.ActiveRun = replaceText;
            else
                if (activeBotFile == "Bot1.ini")
                    DataTypes.botIni1.ActiveRun = replaceText;
                else
                    if (activeBotFile == "Bot2.ini")
                        DataTypes.botIni2.ActiveRun = replaceText;
                    else
                        if (activeBotFile == "Bot3.ini")
                            DataTypes.botIni3.ActiveRun = replaceText;
            this.Close();
            }             
        }
    }

