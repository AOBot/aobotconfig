﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AOBot_Config_Wizard
{
    public partial class DlgAttackSequences : Form
    {
        string charBuild = "";
        public DlgAttackSequences()
        {
            InitializeComponent();
        }//Done
        public void ActivateBuilds()
        {
            switch (DataTypes.mainConfig.BotFile)
            {
                case "Bot0.ini": DataTypes.currentBotIni = DataTypes.botIni0; break;
                case "Bot1.ini": DataTypes.currentBotIni = DataTypes.botIni1; break;
                case "Bot2.ini": DataTypes.currentBotIni = DataTypes.botIni2; break;
                case "Bot3.ini": DataTypes.currentBotIni = DataTypes.botIni3; break;
                default: DataTypes.currentBotIni = DataTypes.botIni0; break;
            }
            charBuild = DataTypes.currentBotIni.CharacterBuild;
            switch (charBuild)
            {
                case "Pally": 
                    radioButton1.Visible = true; radioButton1.Text = "Hammerdin";
                    radioButton2.Visible = true; radioButton2.Text = "ZSmiter";
                    radioButton3.Visible = true; radioButton3.Text = "Smiter";
                    radioButton4.Visible = true; radioButton4.Text = "Zealer";
                    radioButton5.Visible = true; radioButton5.Text = "FOHer (Fist of Heaven)";
                    radioButton6.Visible = false; radioButton6.Text = "";
                    break;
                case "Sorc":
                    radioButton1.Visible = true; radioButton1.Text = "Blizz Sorc";
                    radioButton2.Visible = true; radioButton2.Text = "Lightning Sorc";
                    radioButton3.Visible = true; radioButton3.Text = "Fireball/Meteor Sorc";
                    radioButton4.Visible = true; radioButton4.Text = "Orb Sorc";
                    radioButton5.Visible = false; radioButton5.Text = "";
                    radioButton6.Visible = false; radioButton6.Text = "";
                    break;
                case "Assasin":
                    radioButton1.Visible = true; radioButton1.Text = "Trapper";
                    radioButton2.Visible = false; radioButton2.Text = "";
                    radioButton3.Visible = false; radioButton3.Text = "";
                    radioButton4.Visible = false; radioButton4.Text = "";
                    radioButton5.Visible = false; radioButton5.Text = "";
                    radioButton6.Visible = false; radioButton6.Text = "";
                    break;
                case "Amazon":
                    radioButton1.Visible = true; radioButton1.Text = "Javazon";
                    radioButton2.Visible = true; radioButton2.Text = "Bowazon";
                    radioButton3.Visible = false; radioButton3.Text = "";
                    radioButton4.Visible = false; radioButton4.Text = "";
                    radioButton5.Visible = false; radioButton5.Text = "";
                    radioButton6.Visible = false; radioButton6.Text = "";
                    break;
                case "Barbarian":
                    radioButton1.Visible = true; radioButton1.Text = "Whirlwind barb";
                    radioButton2.Visible = false; radioButton2.Text = "";
                    radioButton3.Visible = false; radioButton3.Text = "";
                    radioButton4.Visible = false; radioButton4.Text = "";
                    radioButton5.Visible = false; radioButton5.Text = "";
                    radioButton6.Visible = false; radioButton6.Text = "";
                    break;
                case "Druid":
                    radioButton1.Visible = true; radioButton1.Text = "Wind Druid";
                    radioButton2.Visible = true; radioButton2.Text = "Fury Werewolf";
                    radioButton3.Visible = false; radioButton3.Text = "";
                    radioButton4.Visible = false; radioButton4.Text = "";
                    radioButton5.Visible = false; radioButton5.Text = "";
                    radioButton6.Visible = false; radioButton6.Text = "";
                    break;
                case "Necromancer":
                    radioButton1.Visible = true; radioButton1.Text = "Poison Necro";
                    radioButton2.Visible = true; radioButton2.Text = "Bone Necro";
                    radioButton3.Visible = false; radioButton3.Text = "";
                    radioButton4.Visible = false; radioButton4.Text = "";
                    radioButton5.Visible = false; radioButton5.Text = "";
                    radioButton6.Visible = false; radioButton6.Text = "";
                    break;
                default :
                    radioButton1.Visible = false; radioButton1.Text = "";
                    radioButton2.Visible = false; radioButton2.Text = "";
                    radioButton3.Visible = false; radioButton3.Text = "";
                    radioButton4.Visible = false; radioButton4.Text = "";
                    radioButton5.Visible = false; radioButton5.Text = "";
                    radioButton6.Visible = false; radioButton6.Text = "";
                    break;
            }
        }//Done
        private void radioClick(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                switch (charBuild)
                {
                    case "Pally":  //Hammerdin
                        checkBox1.Visible = true;
                        checkBox1.Text = "Blessed Hammer";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Holy Shield";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Concentration";
                        checkBox4.Visible = true;
                        checkBox4.Text = "Vigor";
                        checkBox5.Visible = true;
                        checkBox5.Text = "Redemption";
                        checkBox6.Visible = true;
                        checkBox6.Text = "CTA (runeword)";
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    case "Assasin": //Trapper
                        checkBox1.Visible = true;
                        checkBox1.Text = "Lightning Sentry";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Death Sentry";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Shadow Master";
                        checkBox4.Visible = true;
                        checkBox4.Text = "CTA (runeword)";
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    case "Druid":  //Elemental druid
                        checkBox1.Visible = true;
                        checkBox1.Text = "Tornado";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Hurricane";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Oak Sage";
                        checkBox4.Visible = true;
                        checkBox4.Text = "CTA (runeword)";
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    case "Sorc": //Blizz sorc
                        checkBox1.Visible = true;
                        checkBox1.Text = "Blizzard";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Frozen Orb";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Shiver Armor";
                        checkBox4.Visible = true;
                        checkBox4.Text = "Frozen Armor";
                        checkBox5.Visible = true;
                        checkBox5.Text = "Energy Shield";
                        checkBox6.Visible = true;
                        checkBox6.Text = "Static";
                        checkBox7.Visible = true;
                        checkBox7.Text = "CTA (runeword)";
                        checkBox8.Visible = false;
                        break;
                    case "Amazon":  //Javazon
                        checkBox1.Visible = true;
                        checkBox1.Text = "Lightning Fury";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Lightning Strike";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Valkyrie";
                        checkBox4.Visible = true;
                        checkBox4.Text = "CTA (runeword)";
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    case "Barbarian": //Whirlwind barbarian
                        checkBox1.Visible = true;
                        checkBox1.Text = "Whirlwind";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Battle Order";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Battle Command";
                        checkBox4.Visible = true;
                        checkBox4.Text = "Shout";
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    case "Necromancer":  //Poison necro
                        checkBox1.Visible = true;
                        checkBox1.Text = "Poison Nova";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Lower Resist";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Bone Armor";
                        checkBox4.Visible = true;
                        checkBox4.Text = "Fire Golem";
                        checkBox5.Visible = true;
                        checkBox5.Text = "CTA (runeword)";
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    default:
                        checkBox1.Visible = false;
                        checkBox2.Visible = false;
                        checkBox3.Visible = false;
                        checkBox4.Visible = false;
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;


                }
            }
            else if (radioButton2.Checked)
            {
                switch (charBuild)
                {
                    case "Pally": //Zsmiter
                        checkBox1.Visible = true;
                        checkBox1.Text = "Zeal";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Holy Shield";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Redemption";
                        checkBox4.Visible = true;
                        checkBox4.Text = "CTA (runeword)";
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    case "Druid": //Fury werewolf
                        checkBox1.Visible = true;
                        checkBox1.Text = "Fury";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Heart of the wolverine";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Oak Sage";
                        checkBox4.Visible = true;
                        checkBox4.Text = "Cyclone Armor";
                        checkBox5.Visible = true;
                        checkBox5.Text = "CTA (runeword)";
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    case "Sorc": //Lightning
                        checkBox1.Visible = true;
                        checkBox1.Text = "Lightning";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "ThunderStorm";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Static";
                        checkBox4.Visible = true;
                        checkBox4.Text = "Chain Lightning";
                        checkBox5.Visible = true;
                        checkBox5.Text = "Energy Shield";
                        checkBox6.Visible = true;
                        checkBox6.Text = "Frozen Armor";
                        checkBox7.Visible = true;
                        checkBox7.Text = "Shiver Armor";
                        checkBox8.Visible = true;
                        checkBox8.Text = "CTA (runeword)";
                        break;
                    case "Amazon": //Bowazon
                        checkBox1.Visible = true;
                        checkBox1.Text = "Multiple Shot";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Strafe";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Guided Arrow";
                        checkBox4.Visible = true;
                        checkBox4.Text = "Valkyrie";
                        checkBox5.Visible = true;
                        checkBox5.Text = "CTA (runeword)";
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    case "Necromancer": //Bone necro
                        checkBox1.Visible = true;
                        checkBox1.Text = "Bone Spear";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Bone Spirit";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Bone Armor";
                        checkBox4.Visible = true;
                        checkBox4.Text = "Bone Prison";
                        checkBox5.Visible = true;
                        checkBox5.Text = "Fire Golem";
                        checkBox6.Visible = true;
                        checkBox6.Text = "Amplify Damage";
                        checkBox7.Visible = true;
                        checkBox7.Text = "CTA (runeword)";
                        checkBox8.Visible = false;
                        break;
                    default:
                        checkBox1.Visible = false;
                        checkBox2.Visible = false;
                        checkBox3.Visible = false;
                        checkBox4.Visible = false;
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;

                }
            }
            else if (radioButton3.Checked)
            {
                switch (charBuild)
                {
                    case "Pally": //Smiter
                        checkBox1.Visible = true;
                        checkBox1.Text = "Smite";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Holy Shield";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Redemption";
                        checkBox4.Visible = true;
                        checkBox4.Text = "CTA (runeword)";
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    case "Sorc": //FireBall/Meteor
                        checkBox1.Visible = true;
                        checkBox1.Text = "Fireball";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Meteor";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Static";
                        checkBox4.Visible = true;
                        checkBox4.Text = "Energy Shield";
                        checkBox5.Visible = true;
                        checkBox5.Text = "Frozen Armor";
                        checkBox6.Visible = true;
                        checkBox6.Text = "Shiver Armor";
                        checkBox7.Visible = true;
                        checkBox7.Text = "CTA (runeword)";
                        checkBox8.Visible = false;
                        break;
                    default:
                        checkBox1.Visible = false;
                        checkBox2.Visible = false;
                        checkBox3.Visible = false;
                        checkBox4.Visible = false;
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;

                }

            }
            else if (radioButton4.Checked)
            {
                switch (charBuild)
                {
                    case "Pally": //Zealer
                        checkBox1.Visible = true;
                        checkBox1.Text = "Zeal";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Holy Shield";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Redemption";
                        checkBox4.Visible = true;
                        checkBox4.Text = "CTA (runeword)";
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    case "Sorc": //Orb Sorc
                        checkBox1.Visible = true;
                        checkBox1.Text = "Frozen Orb";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Frozen Armor";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Shiver Armor";
                        checkBox4.Visible = true;
                        checkBox4.Text = "Static";
                        checkBox5.Visible = true;
                        checkBox5.Text = "Energy Shield";
                        checkBox6.Visible = true;
                        checkBox6.Text = "CTA (runeword)";
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    default:
                        checkBox1.Visible = false;
                        checkBox2.Visible = false;
                        checkBox3.Visible = false;
                        checkBox4.Visible = false;
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;

                }

            }
            else if (radioButton3.Checked)
            {
                switch (charBuild)
                {
                    case "Pally": //Smiter
                        checkBox1.Visible = true;
                        checkBox1.Text = "Smite";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Holy Shield";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Redemption";
                        checkBox4.Visible = true;
                        checkBox4.Text = "CTA (runeword)";
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    case "Sorc": //FireBall/Meteor
                        checkBox1.Visible = true;
                        checkBox1.Text = "Fireball";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Meteor";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Static";
                        checkBox4.Visible = true;
                        checkBox4.Text = "Energy Shield";
                        checkBox5.Visible = true;
                        checkBox5.Text = "Frozen Armor";
                        checkBox6.Visible = true;
                        checkBox6.Text = "Shiver Armor";
                        checkBox7.Visible = true;
                        checkBox7.Text = "CTA (runeword)";
                        checkBox8.Visible = false;
                        break;
                    default:
                        checkBox1.Visible = false;
                        checkBox2.Visible = false;
                        checkBox3.Visible = false;
                        checkBox4.Visible = false;
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;

                }

            }
            else if (radioButton5.Checked)
            {
                switch (charBuild)
                {
                    case "Pally": //FOHer
                        checkBox1.Visible = true;
                        checkBox1.Text = "Fist of Heaven";
                        checkBox1.Checked = true;
                        checkBox1.Enabled = false;
                        checkBox2.Visible = true;
                        checkBox2.Text = "Holy Shield";
                        checkBox3.Visible = true;
                        checkBox3.Text = "Redemption";
                        checkBox4.Visible = true;
                        checkBox4.Text = "CTA (runeword)";
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;
                    default:
                        checkBox1.Visible = false;
                        checkBox2.Visible = false;
                        checkBox3.Visible = false;
                        checkBox4.Visible = false;
                        checkBox5.Visible = false;
                        checkBox6.Visible = false;
                        checkBox7.Visible = false;
                        checkBox8.Visible = false;
                        break;

                }

            }

        }//Done
        private void DlgAttackSequences_Load(object sender, EventArgs e)
        {
            ActivateBuilds();
        }//Done
        private void button1_Click(object sender, EventArgs e)
        {
            SaveCharClass();
            SaveAttackSequence();
            this.Close();
        }//Done
        private void SaveCharClass()
        {
            StreamReader str = File.OpenText(DataTypes.mainConfig.AOPath + "config\\ini\\" + DataTypes.mainConfig.BotFile);
            string content = str.ReadToEnd();
            str.Close();

            int start = content.IndexOf("AttackFile=") + 12;
            int end = content.IndexOf('\r', start);
            string part = content.Substring(start, (end - start));
            int end2 = part.IndexOf('\\');
            part = part.Remove(end2);

            content = content.Replace(part, DataTypes.currentBotIni.CharacterBuild);

            StreamWriter stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + DataTypes.mainConfig.BotFile);
            stw.Write(content);
            stw.Close();
        }//Done
        private void SaveAttackSequence()
        {
            int noRadio = GetRadioButtonNumber();
            switch (charBuild)
            {
                case "Pally": SavePallyAttack(noRadio); break;
                case "Sorc": SaveSorcAttack(noRadio); break;
                case "Druid": SaveDruidAttack(noRadio); break;
                case "Assasin": SaveAssasinAttack(noRadio); break;
                case "Amazon": SaveAmazonAttack(noRadio); break;
                case "Necromancer": SaveNecromancerAttack(noRadio); break;
                case "Barbarian": SaveBarbarianAttack(noRadio); break;
                default: SavePallyAttack(noRadio); break;

            }
        }//Done
        private int GetRadioButtonNumber()
        {
            int noRadio = 0;
            if (radioButton1.Checked)
                noRadio = 1;
            else if (radioButton2.Checked)
                noRadio = 2;
            else if (radioButton3.Checked)
                noRadio = 3;
            else if (radioButton4.Checked)
                noRadio = 4;
            else if (radioButton5.Checked)
                noRadio = 5;
            else if (radioButton6.Checked)
                noRadio = 6;

            return noRadio;
        }//Done

        private void SavePallyAttack(int p_noRadio)
        {
            switch (p_noRadio)
            {
                case 1: SaveHammerdin(); break;
                case 2: SaveZSmiter(); break;
                case 3: SaveSmiter(); break;
                case 4: SaveZeal(); break;
                case 5: SaveFoher(); break;
                default: SaveHammerdin(); break;
            }
        } //Done
        private void SaveHammerdin()
        {
            StreamWriter stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + charBuild + "\\Attack.ini");
            if (checkBox3.Checked)
            {
                stw.WriteLine("SelectRightSkill Concentration");
                stw.WriteLine("Sleep 100");
            }
            stw.WriteLine("CastLeftSkill");
            stw.WriteLine("Sleep 500");
            stw.WriteLine("RecastLeftSkill");
            stw.WriteLine("Sleep 500");
            stw.WriteLine("RecastLeftSkill");
            stw.WriteLine("Sleep 500");
            stw.WriteLine("RecastLeftSkill");
            stw.WriteLine("Sleep 500");
            stw.WriteLine("RecastLeftSkill");
            stw.WriteLine("Sleep 500");
            if (checkBox5.Checked)
            {
                stw.WriteLine("SelectRightSkill Redemption");
                stw.WriteLine("Sleep 250");
            }
            stw.Close();
            stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + charBuild + "\\PreAttack.ini");
            if (checkBox3.Checked)
            {
                stw.WriteLine("SelectRightSkill Concentration");
                stw.WriteLine("Sleep 100");
            }
            stw.WriteLine("CastLeftSkill");
            stw.WriteLine("Sleep 500");
            stw.WriteLine("RecastLeftSkill");
            stw.WriteLine("Sleep 500");
            stw.WriteLine("RecastLeftSkill");
            stw.WriteLine("Sleep 500");
            stw.WriteLine("RecastLeftSkill");
            stw.WriteLine("Sleep 500");
            stw.WriteLine("RecastLeftSkill");
            stw.WriteLine("Sleep 500");
            stw.WriteLine("RecastLeftSkill");
            stw.WriteLine("Sleep 500");
            stw.WriteLine("RecastLeftSkill");
            stw.WriteLine("Sleep 500");
            if (checkBox5.Checked)
            {
                stw.WriteLine("SelectRightSkill Redemption");
                stw.WriteLine("Sleep 200");
            }
            stw.Close();
            stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + charBuild + "\\PreCast.ini");
            if (checkBox6.Checked)
            {
            stw.WriteLine("Sleep 400");
            stw.WriteLine("SwitchWeapons");
            stw.WriteLine("Sleep 100");
            stw.WriteLine();
            stw.WriteLine("SelectRightSkill BattleCommand");
            stw.WriteLine("Sleep 100");
            stw.WriteLine("CastRightSkill");
            stw.WriteLine("Sleep 300");
            stw.WriteLine();
            stw.WriteLine("SelectRightSkill BattleOrders");
            stw.WriteLine("Sleep 100");
            stw.WriteLine("CastRightSkill");
            stw.WriteLine("Sleep 300");
            stw.WriteLine();
            stw.WriteLine("Sleep 400");
            stw.WriteLine("SwitchWeapons");
            stw.WriteLine("Sleep 100");
            stw.WriteLine();
            }
            if (checkBox2.Checked)
            {
                stw.WriteLine("SelectRightSkill HolyShield");
                stw.WriteLine("Sleep 100");
                stw.WriteLine("CastRightSkill");
                stw.WriteLine("Sleep 300");
            }
            stw.Close();
            stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + charBuild + "\\PreWalk.ini");
            if (checkBox4.Checked)
            {
                stw.WriteLine("SelectRightSkill Vigor");
                stw.WriteLine("Sleep 100");
            }
            stw.Close();
        } //Done...Need testing
        private void SaveZSmiter()
        {
        }
        private void SaveSmiter()
        {
        }
        private void SaveZeal()
        {
        }
        private void SaveFoher()
        {
        }

        private void SaveSorcAttack(int p_noRadio)
        {
            switch (p_noRadio)
            {
                case 1: SaveBlizzSorc(); break;
                case 2: SaveLightSorc(); break;
                case 3: SaveFireSorc(); break;
                case 4: SaveOrbSorc(); break;
                default: SaveBlizzSorc(); break;
            }
        }//Done
        private void SaveBlizzSorc()
        {
            StreamWriter stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + charBuild + "\\Attack.ini");
            stw.WriteLine("SelectRightSkill Blizzard");
            stw.WriteLine("Sleep 100");
            stw.WriteLine("CastRightSkillOnTarget");
            if (checkBox2.Checked)
            {
                stw.WriteLine("Sleep 700");
                stw.WriteLine("SelectLeftSkill FrozenOrb");
                stw.WriteLine("Sleep 100");
                stw.WriteLine("CastLeftSkillOnTarget");
                stw.WriteLine("Sleep 1100");
                stw.WriteLine("RecastLeftSkillOnTarget");
                stw.WriteLine("Sleep 1100");
                stw.WriteLine("RecastLeftSkillOnTarget");
                stw.WriteLine("Sleep 1100");
            }
            else
            { stw.WriteLine("Sleep 1900"); }
            stw.Close();
            stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + charBuild + "\\PreAttack.ini");
            stw.WriteLine("SelectRightSkill Blizzard");
            stw.WriteLine("Sleep 100");
            stw.WriteLine("CastRightSkillOnTarget");
            if (checkBox2.Checked)
            {
                stw.WriteLine("Sleep 700");
                if (checkBox6.Checked)
                {
                    stw.WriteLine("SelectRightSkill StaticField");
                    stw.WriteLine("Sleep 100");
                    stw.WriteLine("CastRightSkill");
                    stw.WriteLine("Sleep 500");
                }
                stw.WriteLine("SelectLeftSkill FrozenOrb");
                stw.WriteLine("Sleep 100");
                stw.WriteLine("CastLeftSkillOnTarget");
                stw.WriteLine("Sleep 1100");
                stw.WriteLine("RecastLeftSkillOnTarget");
                stw.WriteLine("Sleep 1100");
                stw.WriteLine("RecastLeftSkillOnTarget");
                stw.WriteLine("Sleep 1100");
            }
            else
            { 
                stw.WriteLine("Sleep 1900");
                if (checkBox6.Checked)
                {
                    stw.WriteLine("SelectRightSkill StaticField");
                    stw.WriteLine("Sleep 100");
                    stw.WriteLine("CastRightSkill");
                    stw.WriteLine("Sleep 500");
                }
            }
            stw.Close();
            stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + charBuild + "\\PreCast.ini");
            if (checkBox7.Checked)
            {
                stw.WriteLine("Sleep 400");
                stw.WriteLine("SwitchWeapons");
                stw.WriteLine("Sleep 100");
                stw.WriteLine();
                stw.WriteLine("SelectRightSkill BattleCommand");
                stw.WriteLine("Sleep 100");
                stw.WriteLine("CastRightSkill");
                stw.WriteLine("Sleep 300");
                stw.WriteLine();
                stw.WriteLine("SelectRightSkill BattleOrders");
                stw.WriteLine("Sleep 100");
                stw.WriteLine("CastRightSkill");
                stw.WriteLine("Sleep 300");
                stw.WriteLine();
                stw.WriteLine("Sleep 400");
                stw.WriteLine("SwitchWeapons");
                stw.WriteLine("Sleep 100");
                stw.WriteLine();
            }
            if (checkBox3.Checked)
            {
                stw.WriteLine("SelectRightSkill ShiverArmor");
                stw.WriteLine("Sleep 100");
                stw.WriteLine("CastRightSkill");
                stw.WriteLine("Sleep 300");
            }
            else if (checkBox4.Checked)
            {
                stw.WriteLine("SelectRightSkill FrozenArmor");
                stw.WriteLine("Sleep 100");
                stw.WriteLine("CastRightSkill");
                stw.WriteLine("Sleep 300");
            }
            if (checkBox5.Checked)
            {
                stw.WriteLine("SelectRightSkill EnergyShield");
                stw.WriteLine("Sleep 100");
                stw.WriteLine("CastRightSkill");
                stw.WriteLine("Sleep 300");
            }
            stw.Close();
            stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + charBuild + "\\PreWalk.ini");
            stw.Close();
        }//Done...Need testing
        private void SaveLightSorc()
        {
        }
        private void SaveFireSorc()
        {
        }
        private void SaveOrbSorc()
        {
        }

        private void SaveDruidAttack(int p_noRadio)
        {
            switch (p_noRadio)
            {
                case 1: SaveWindDruid(); break;
                case 2: SaveFuryDruid(); break;
                default: SaveWindDruid(); break;
            }
        }//Done
        private void SaveWindDruid()
        {
        }
        private void SaveFuryDruid()
        {
        }

        private void SaveAssasinAttack(int p_noRadio)
        {
            switch (p_noRadio)
            {
                case 1: SaveTrapper(); break;
                default: SaveTrapper(); break;
            }
        }//Done
        private void SaveTrapper()
        {
        }

        private void SaveAmazonAttack(int p_noRadio)
        {
            switch (p_noRadio)
            {
                case 1: SaveJavazon(); break;
                case 2: SaveBowazon(); break;
                default: SaveJavazon(); break;
            }
        }//Done
        private void SaveJavazon()
        {
            try
            {
                StreamWriter stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + charBuild + "\\Attack.ini");
                if (checkBox2.Checked)
                {
                    stw.WriteLine("SelectLeftSkill LightningStrike");
                    stw.WriteLine("Sleep 100");
                    stw.WriteLine("CastLeftSkillOnTarget");
                    stw.WriteLine("Sleep 500");
                    stw.WriteLine("RecastLeftSkillOnTarget");
                    stw.WriteLine("Sleep 500");
                }
                stw.WriteLine("SelectLeftSkill LightningFury");
                stw.WriteLine("Sleep 100");
                stw.WriteLine("CastLeftSkillOnTarget");
                stw.WriteLine("Sleep 500");
                stw.WriteLine("RecastLeftSkillOnTarget");
                stw.WriteLine("Sleep 500");
                stw.WriteLine("RecastLeftSkillOnTarget");
                stw.WriteLine("Sleep 500");
                stw.Close();
                stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + charBuild + "\\PreAttack.ini");
                if (checkBox2.Checked)
                {
                    stw.WriteLine("SelectLeftSkill LightningStrike");
                    stw.WriteLine("Sleep 100");
                    stw.WriteLine("CastLeftSkillOnTarget");
                    stw.WriteLine("Sleep 500");
                    stw.WriteLine("RecastLeftSkillOnTarget");
                    stw.WriteLine("Sleep 500");
                }
                stw.WriteLine("SelectLeftSkill LightningFury");
                stw.WriteLine("Sleep 100");
                stw.WriteLine("CastLeftSkillOnTarget");
                stw.WriteLine("Sleep 500");
                stw.WriteLine("RecastLeftSkillOnTarget");
                stw.WriteLine("Sleep 500");
                stw.WriteLine("RecastLeftSkillOnTarget");
                stw.WriteLine("Sleep 500");
                stw.Close();
                stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + charBuild + "\\PreCast.ini");
                if (checkBox4.Checked)
                {
                    stw.WriteLine("Sleep 400");
                    stw.WriteLine("SwitchWeapons");
                    stw.WriteLine("Sleep 100");
                    stw.WriteLine();
                    stw.WriteLine("SelectRightSkill BattleCommand");
                    stw.WriteLine("Sleep 100");
                    stw.WriteLine("CastRightSkill");
                    stw.WriteLine("Sleep 300");
                    stw.WriteLine();
                    stw.WriteLine("SelectRightSkill BattleOrders");
                    stw.WriteLine("Sleep 100");
                    stw.WriteLine("CastRightSkill");
                    stw.WriteLine("Sleep 300");
                    stw.WriteLine();
                    stw.WriteLine("Sleep 400");
                    stw.WriteLine("SwitchWeapons");
                    stw.WriteLine("Sleep 100");
                    stw.WriteLine();
                }
                if (checkBox3.Checked)
                {
                    stw.WriteLine("SelectRightSkill Valkyrie");
                    stw.WriteLine("Sleep 100");
                    stw.WriteLine("CastRightSkill");
                    stw.WriteLine("Sleep 300");
                }
                stw.Close();
                stw = File.CreateText(DataTypes.mainConfig.AOPath + "config\\ini\\" + charBuild + "\\PreWalk.ini");
                stw.Close();
            }
            catch (DirectoryNotFoundException)
            {
                Directory.CreateDirectory(DataTypes.mainConfig.AOPath + "config\\ini\\" + charBuild + "\\");
                SaveJavazon();
            }
        }//Done...Need testing
        private void SaveBowazon()
        {
        }

        private void SaveNecromancerAttack(int p_noRadio)
        {
            switch (p_noRadio)
            {
                case 1: SavePoisonNecro(); break;
                case 2: SaveBoneNecro(); break;
                default: SavePoisonNecro(); break;
            }
        }//Done
        private void SavePoisonNecro()
        {
        }
        private void SaveBoneNecro()
        {
        }

        private void SaveBarbarianAttack(int p_noRadio)
        {
            switch (p_noRadio)
            {
                case 1: SaveWhirlwindBarb(); break;
                default: SaveWhirlwindBarb(); break;
            }
        }//Done
        private void SaveWhirlwindBarb()
        {
        }
    }
}
