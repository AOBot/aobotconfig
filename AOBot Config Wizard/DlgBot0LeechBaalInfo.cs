﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AOBot_Config_Wizard
{
    public partial class DlgBot0LeechBaalInfo : Form
    {
        public DlgBot0LeechBaalInfo()
        {
            InitializeComponent();
        }

        private void SaveLeechTempInfo(object sender, EventArgs e)
        {
            DataTypes.botIni0.RunnerAccount = textBoxRunnerAccount.Text;
            DataTypes.botIni0.RunnerCharName = textBoxRunnerCharName.Text;
            DataTypes.botIni0.RunnerGamePass = textBoxRunnerPass.Text;
            Close();
        }
        private void NoSpacesValidation(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
            if (Convert.ToInt32(e.KeyChar) == 32)
                e.Handled = true;
        }
    }
}
